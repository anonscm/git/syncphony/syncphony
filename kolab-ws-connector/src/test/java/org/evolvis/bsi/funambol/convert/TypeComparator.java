/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.CollaborationItem;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Phone;

public class TypeComparator {


	private static void equals(CollaborationItem i1, CollaborationItem i2, boolean testAll) {
		assertEquals(i1.getUid(), i2.getUid());
		assertEquals(i1.getBody(), i2.getBody());
		assertEquals(i1.getCategories(), i2.getCategories());
		assertEquals(i1.getCreationDate(), i2.getCreationDate());
		assertEquals(i1.getLastModificationDate(), i2.getLastModificationDate());
		if (testAll)
			assertEquals(i1.getSensitivity(), i2.getSensitivity());
		assertEquals(i1.getInlineAttachment(), i2.getInlineAttachment());
		assertEquals(i1.getProductId(), i2.getProductId());
	}
	
	public static void equals(Contact c1, Contact c2, boolean testAll) {
		if (c1 == null && c2 == null)
			return;
		if (c1 == null || c2 == null)
			throw new AssertionError("one contact is null");
		equals((CollaborationItem) c1, (CollaborationItem) c2, testAll);
		assertEquals(c1.getGivenName(), c2.getGivenName());
		assertEquals(c1.getMiddleNames(), c2.getMiddleNames());
		assertEquals(c1.getLastName(), c2.getLastName());
		if (c1.getFullName() == null && c2.getFullName() == null)
			assertEquals(c1.getFullName(), c2.getFullName());
		assertEquals(c1.getInitials(), c2.getInitials());
		assertEquals(c1.getPrefix(), c2.getPrefix());
		assertEquals(c1.getSuffix(), c2.getSuffix());
		assertEquals(c1.getFreeBusyUrl(), c2.getFreeBusyUrl());
		assertEquals(c1.getOrganization(), c2.getOrganization());
		assertEquals(c1.getWebPage(), c2.getWebPage());
		assertEquals(c1.getImAddress(), c2.getImAddress());
		assertEquals(c1.getDepartment(), c2.getDepartment());
		assertEquals(c1.getOfficeLocation(), c2.getOfficeLocation());
		assertEquals(c1.getProfession(), c2.getProfession());
		assertEquals(c1.getJobTitle(), c2.getJobTitle());
		assertEquals(c1.getManagerName(), c2.getManagerName());
		assertEquals(c1.getAssistant(), c2.getAssistant());
		assertEquals(c1.getNickName(), c2.getNickName());
		assertEquals(c1.getSpouseName(), c2.getSpouseName());
		assertEquals(c1.getBirthday(), c2.getBirthday());
		assertEquals(c1.getAnniversary(), c2.getAnniversary());
		assertEquals(c1.getPictureAttachmentId(), c2.getPictureAttachmentId());
		assertEquals(c1.getChildren(), c2.getChildren());
		assertEquals(c1.getGender(), c2.getGender());
		assertEquals(c1.getLanguage(), c2.getLanguage());
		List<Phone> pl1 = c1.getPhone();
		List<Phone> pl2 = c2.getPhone();
		assertEquals(pl1.size(), pl2.size());
		for (int i = 0; i < pl1.size(); i ++) {
			Phone p1 = pl1.get(i);
			Phone p2 = pl2.get(i);
			assertEquals(p1.getType(), p2.getType());
			assertEquals(p1.getNumber(), p2.getNumber());
		}
		List<Email> el1 = c1.getEmail();
		List<Email> el2 = c2.getEmail();
		assertEquals(el1.size(), el2.size());
		for (int i = 0; i < el1.size(); i ++) {
			Email e1 = el1.get(i);
			Email e2 = el2.get(i);
			if (testAll)
				assertEquals(e1.getDisplayName(), e2.getDisplayName());
			assertEquals(e1.getSmtpAddress(), e2.getSmtpAddress());
		}
		List<Address> al1 = c1.getAddress();
		List<Address> al2 = c2.getAddress();
		assertEquals(al1.size(), al2.size());
		for (int i = 0; i < al1.size(); i ++) {
			Address a1 = al1.get(i);
			Address a2 = al2.get(i);
			assertEquals(a1.getType(), a2.getType());
			assertEquals(a1.getStreet(), a2.getStreet());
			assertEquals(a1.getLocality(), a2.getLocality());
			assertEquals(a1.getRegion(), a2.getRegion());
			assertEquals(a1.getPostalCode(), a2.getPostalCode());
			assertEquals(a1.getCountry(), a2.getCountry());
		}
		assertEquals(c1.getPreferredAddress(), c2.getPreferredAddress());
		assertEquals(c1.getLatitude(), c2.getLatitude());
		assertEquals(c1.getLongitude(), c2.getLongitude());
	}
	
	
}
