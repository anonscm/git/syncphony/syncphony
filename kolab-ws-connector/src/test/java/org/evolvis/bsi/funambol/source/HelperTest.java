/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.evolvis.bsi.funambol.source.Helper.*;

import org.evolvis.bsi.funambol.source.Helper.KolabId;

import junit.framework.TestCase;

public class HelperTest extends TestCase {

	public void testConvertKolabAndFunambolId() {
		String folderName = "user/p.mue\\\\ller/Con.tact";
		String kolabId = "12\\\\3dsfj-s.d3";
		KolabType type = KolabType.CONTACT;
		String funambolId = convertToFunambolId(folderName, kolabId, type);
		KolabId id = convertToKolabId(funambolId);
		assertEquals(folderName, id.getFolder().getName());
		assertEquals(kolabId, id.getGuid());
		assertEquals(type, id.getType());
	}

}
