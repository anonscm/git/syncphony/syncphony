/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.funambol.common.pim.calendar.RecurrencePattern;
import com.funambol.common.pim.common.ConversionException;

public class ConverterTest {
	
	private com.funambol.common.pim.contact.Contact funambolContact1 = null;
	private com.funambol.common.pim.contact.Contact funambolContact2 = null;
	private org.evolvis.bsi.kolab.service.Contact kolabContact1 = null;
	private org.evolvis.bsi.kolab.service.Contact kolabContact2 = null;
	private org.evolvis.bsi.funambol.convert.Converter convertContact = null;
	
	private com.funambol.common.pim.contact.PersonalDetail funambolPersonalDetail = null;
	private com.funambol.common.pim.contact.BusinessDetail funambolBusinessDetail = null;
	
	@Before 
	public void testUp(){
	this.kolabContact1 = new org.evolvis.bsi.kolab.service.Contact();
	this.kolabContact2 = new org.evolvis.bsi.kolab.service.Contact();
	this.funambolContact1 = new com.funambol.common.pim.contact.Contact();
	this.funambolContact2 = new com.funambol.common.pim.contact.Contact();
	this.convertContact = new org.evolvis.bsi.funambol.convert.Converter();
	}
	
	@After
	public void tearDown(){
		
	}
	
	public void bugfix(){
		com.funambol.common.pim.contact.Photo funambolPhoto = new com.funambol.common.pim.contact.Photo();
		this.funambolPersonalDetail.setPhotoObject(funambolPhoto);
	}
	
	@Test
	@Ignore
	public void testObjectEventConvert() throws ConversionException {
		
		org.evolvis.bsi.kolab.service.Event oKolabEvent1 = new org.evolvis.bsi.kolab.service.Event();
		org.evolvis.bsi.kolab.service.Event oKolabEvent2 = new org.evolvis.bsi.kolab.service.Event();
		com.funambol.common.pim.calendar.Calendar oFunambolCalendar1 = new com.funambol.common.pim.calendar.Calendar();
		org.evolvis.bsi.funambol.convert.Converter oConverter = new org.evolvis.bsi.funambol.convert.Converter();
		
		oFunambolCalendar1 = oConverter.convertEvent(oKolabEvent1); 
		oKolabEvent2 = oConverter.convertToEvent(oFunambolCalendar1, null, null, null);
			
	}
	
	/**
	 * Email addresses which empty smtp address should not be added because
	 * a NullPointerException will be thrown later in this case:
	 * 
	 * java.lang.NullPointerException
        at com.funambol.common.pim.converter.BaseConverter.escapeSeparator(BaseConverter.java:516)
        at com.funambol.common.pim.converter.ContactToVcard.composeFieldEmail(ContactToVcard.java:529)
        at com.funambol.common.pim.converter.ContactToVcard.convert(ContactToVcard.java:89)
        at org.evolvis.bsi.funambol.source.Helper.createContent(Helper.java:496)
        at org.evolvis.bsi.funambol.source.KolabContactSyncSource.getItem(KolabContactSyncSource.java:135)
        at org.evolvis.bsi.funambol.source.AbstractKolabSyncSource.getSyncItemFromId(AbstractKolabSyncSource.java:499)
	 * @throws ConversionException
	 */
	@Test
	public void testEmailEmptySmtpAddress() throws ConversionException {
		Contact contact = new Contact();
		
		Email email = new Email();
		email.setDisplayName("name");
		email.setSmtpAddress("em@il.de");
		
		contact.getEmail().add(email);
		
		com.funambol.common.pim.contact.Contact c2 = Converter.convertContact(contact);
		
		assertEquals(1, c2.getPersonalDetail().getEmails().size());
	}
	

	public void testObjectValuesEventConvert() throws ConversionException {
		
		org.evolvis.bsi.kolab.service.Event oKolabEvent1 = new org.evolvis.bsi.kolab.service.Event();
		org.evolvis.bsi.kolab.service.Event oKolabEvent2 = new org.evolvis.bsi.kolab.service.Event();
		com.funambol.common.pim.calendar.Calendar oFunambolCalendar1 = new com.funambol.common.pim.calendar.Calendar();
		org.evolvis.bsi.funambol.convert.Converter oConverter = new org.evolvis.bsi.funambol.convert.Converter();
		
		oFunambolCalendar1 = oConverter.convertEvent(oKolabEvent1); 
		oKolabEvent2 = oConverter.convertToEvent(oFunambolCalendar1, null, null, null);
			
	}

	@Test
	@Ignore
	public void testObjectContactConvert(){	
		// junit.framework.Assert
		try {
			this.funambolContact1 = convertContact.convertContact(this.kolabContact1);
			this.kolabContact2 = convertContact.convertToContact(this.funambolContact1);
		} catch (ConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void testSingleFieldsKolab2Funambol2Kolab(){

		SimpleDateFormat fmt = new SimpleDateFormat();
		fmt.applyPattern("yyyy-MM-dd");
		Date dtTest = new Date();
		String strTest = fmt.format(dtTest);

		Date temp_ = null;
		
		try {
			temp_ = fmt.parse(strTest);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String strDate = "20091101";
		this.kolabContact1.setPictureAttachmentId("http://ugs.tarent.de");
		
		this.kolabContact1.setAnniversary(temp_);
		this.kolabContact1.setBirthday(temp_);
		
		String strMonth = strDate.substring(4,6);
		String strDay = strDate.substring(6,8);
		String strYear = strDate.substring(0,4);

		String strDate_ = strYear + "-" + strMonth + "-" + strDay;
		
		this.kolabContact1.setAssistant("Max Mustermann");
		this.kolabContact1.setCategories("Kategorie 1");
		this.kolabContact1.setBody("Bemerkung");
		this.kolabContact1.setChildren("Johanna, Felix");
		this.kolabContact1.setDepartment("Softwareentwicklung");
		this.kolabContact1.setFreeBusyUrl("Free busy irgendwas URL");
		this.kolabContact1.setFullName("Patrick Apel");
		this.kolabContact1.setGivenName("Patrick");
		this.kolabContact1.setJobTitle("Höchstwahrscheinlich Position");
		this.kolabContact1.setLanguage("Deutsch");
		this.kolabContact1.setLastName("Apel");
		this.kolabContact1.setManagerName("B=C3=A4r");
		this.kolabContact1.setMiddleNames("Günther Hugo");
		this.kolabContact1.setNickName("Pit");
		this.kolabContact1.setOrganization("Tarent GmbH");
		this.kolabContact1.setPrefix("Herr");
		this.kolabContact1.setProfession("Softwareentwickler");
		this.kolabContact1.setSpouseName("Partner");
		this.kolabContact1.setSuffix("Dr. med.");
		this.kolabContact1.setWebPage("http://www.patrick-apel.de");
		//this.kolabContact1.setSensitivity("5");
		this.kolabContact1.setUid("1337");
		
		try {
			this.funambolContact1 = convertContact.convertContact(this.kolabContact1);
			this.kolabContact2 = convertContact.convertToContact(this.funambolContact1);
			this.funambolContact2 = convertContact.convertContact(this.kolabContact2);

//org.junit.Assert.assertEquals(this.kolabContact1.getPictureAttachmentId(),this.kolabContact2.getPictureAttachmentId());

//org.junit.Assert.assertEquals(this.kolabContact1.getAnniversary(),this.kolabContact2.getAnniversary());

//org.junit.Assert.assertEquals(this.kolabContact1.getAnniversary(), strDate_);
	
org.junit.Assert.assertEquals(this.kolabContact1.getBirthday(),this.kolabContact2.getBirthday());
org.junit.Assert.assertEquals(this.kolabContact1.getAssistant(),this.kolabContact2.getAssistant());
org.junit.Assert.assertEquals(this.kolabContact1.getCategories(),this.kolabContact2.getCategories());
org.junit.Assert.assertEquals(this.kolabContact1.getBody(),this.kolabContact2.getBody());
org.junit.Assert.assertEquals(this.kolabContact1.getChildren(),this.kolabContact2.getChildren());
org.junit.Assert.assertEquals(this.kolabContact1.getDepartment(),this.kolabContact2.getDepartment());
org.junit.Assert.assertEquals(this.kolabContact1.getFreeBusyUrl(),this.kolabContact2.getFreeBusyUrl());
org.junit.Assert.assertEquals(this.kolabContact1.getFullName(),this.kolabContact2.getFullName());
org.junit.Assert.assertEquals(this.kolabContact1.getGivenName(),this.kolabContact2.getGivenName());
org.junit.Assert.assertEquals(this.kolabContact1.getJobTitle(),this.kolabContact2.getJobTitle());
org.junit.Assert.assertEquals(this.kolabContact1.getLanguage(),this.kolabContact2.getLanguage());
org.junit.Assert.assertEquals(this.kolabContact1.getLastName(),this.kolabContact2.getLastName());

//org.junit.Assert.assertEquals(this.kolabContact1.getManagerName(), this.kolabContact2.getManagerName());

org.junit.Assert.assertEquals(this.funambolContact1.getBusinessDetail().getManager(), this.funambolContact2.getBusinessDetail().getManager());

org.junit.Assert.assertEquals(this.kolabContact1.getMiddleNames(),this.kolabContact2.getMiddleNames());
org.junit.Assert.assertEquals(this.kolabContact1.getNickName(),this.kolabContact2.getNickName());
org.junit.Assert.assertEquals(this.kolabContact1.getOrganization(),this.kolabContact2.getOrganization());
org.junit.Assert.assertEquals(this.kolabContact1.getPrefix(),this.kolabContact2.getPrefix());
org.junit.Assert.assertEquals(this.kolabContact1.getProfession(),this.kolabContact2.getProfession());
org.junit.Assert.assertEquals(this.kolabContact1.getSpouseName(),this.kolabContact2.getSpouseName());
org.junit.Assert.assertEquals(this.kolabContact1.getSuffix(),this.kolabContact2.getSuffix());
org.junit.Assert.assertEquals(this.kolabContact1.getWebPage(),this.kolabContact2.getWebPage());

//org.junit.Assert.assertEquals(this.kolabContact1.getSensitivity(),this.kolabContact2.getSensitivity());

org.junit.Assert.assertEquals(this.kolabContact1.getUid(),this.kolabContact2.getUid());
			
		} catch (ConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//org.junit.Assert.assertEquals(kolabContact1.getAddress().get(0).getLocality().toString(), funambolContact1.getPersonalDetail().getAddress().getCity().getPropertyValueAsString());
	}
	
	
	@Test
	public void testNonSingleFieldsKolab2Funambol2Kolab(){
		
		com.funambol.common.pim.contact.Contact funambolContact1_ = new com.funambol.common.pim.contact.Contact();
		com.funambol.common.pim.contact.Contact funambolContact2_ = new com.funambol.common.pim.contact.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact1_ = new org.evolvis.bsi.kolab.service.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact2_ = new org.evolvis.bsi.kolab.service.Contact();
		
	///Addresses
	//Address 
	org.evolvis.bsi.kolab.service.Address kolabAddress1 = new org.evolvis.bsi.kolab.service.Address();
	kolabAddress1.setCountry("Germany");
	kolabAddress1.setLocality("Euskirchen");
	kolabAddress1.setPostalCode("53879");
	kolabAddress1.setRegion("NRW");
	kolabAddress1.setStreet("Winkelpfad 5");
	kolabAddress1.setType(AddressType.HOME);

	org.evolvis.bsi.kolab.service.Address kolabAddress3 = new org.evolvis.bsi.kolab.service.Address();
	kolabAddress3.setCountry("Germany");
	kolabAddress3.setLocality("Köln");
	kolabAddress3.setPostalCode("50733");
	kolabAddress3.setRegion("NRW");
	kolabAddress3.setStreet("Nordstraße 61");
	kolabAddress3.setType(AddressType.BUSINESS);

	kolabContact1_.getAddress().add(kolabAddress1);
	kolabContact1_.getAddress().add(kolabAddress3);

	///Phones
	//Phone 1
	org.evolvis.bsi.kolab.service.Phone kolabPhone1 = new org.evolvis.bsi.kolab.service.Phone();
	kolabPhone1.setType(PhoneType.HOME_1);
	kolabPhone1.setNumber("0202665603");
	//Phone 2
	org.evolvis.bsi.kolab.service.Phone kolabPhone2 = new org.evolvis.bsi.kolab.service.Phone();
	kolabPhone2.setType(PhoneType.HOME_2);;
	kolabPhone2.setNumber("020212345");
	//Phone 3
	org.evolvis.bsi.kolab.service.Phone kolabPhone3 = new org.evolvis.bsi.kolab.service.Phone();
	kolabPhone3.setType(PhoneType.BUSINESS_1);;
	kolabPhone3.setNumber("020256789");
	//Phone 4
	org.evolvis.bsi.kolab.service.Phone kolabPhone4 = new org.evolvis.bsi.kolab.service.Phone();
	kolabPhone4.setType(PhoneType.BUSINESS_2);;
	kolabPhone4.setNumber("020256789");
	
	kolabContact1_.getPhone().add(kolabPhone1);
	kolabContact1_.getPhone().add(kolabPhone2);
	kolabContact1_.getPhone().add(kolabPhone3);
	kolabContact1_.getPhone().add(kolabPhone4);
	
	////////////////////////// Emails //////////////////////////
	//Email 1
	org.evolvis.bsi.kolab.service.Email kolabEmail1 = new org.evolvis.bsi.kolab.service.Email();
	kolabEmail1.setDisplayName("Patrick Apel");
	kolabEmail1.setSmtpAddress("p.apel@tarent.de");
	//Email 2
	org.evolvis.bsi.kolab.service.Email kolabEmail2 = new org.evolvis.bsi.kolab.service.Email();
	kolabEmail2.setDisplayName("Max Mustermann");
	kolabEmail2.setSmtpAddress("m.mustermann@tarent.de");
	//Email 3
	org.evolvis.bsi.kolab.service.Email kolabEmail3 = new org.evolvis.bsi.kolab.service.Email();
	kolabEmail3.setDisplayName("Felix Hubertus");
	kolabEmail3.setSmtpAddress("f.hubertus@tarent.de");
	//Email 4
	org.evolvis.bsi.kolab.service.Email kolabEmail4 = new org.evolvis.bsi.kolab.service.Email();
	kolabEmail4.setDisplayName("Sonja Krumm");
	kolabEmail4.setSmtpAddress("s.krumm@tarent.de");
	
	kolabContact1_.getEmail().add(kolabEmail1);
	kolabContact1_.getEmail().add(kolabEmail2);
	kolabContact1_.getEmail().add(kolabEmail3);
	kolabContact1_.getEmail().add(kolabEmail4);
	
		try {
			funambolContact1_ = convertContact.convertContact(kolabContact1_);
			kolabContact2_ = convertContact.convertToContact(funambolContact1_);
			
			///Emails
			//Email 1
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(0).getDisplayName(),kolabContact2_.getEmail().get(0).getDisplayName());
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(0).getSmtpAddress(),kolabContact2_.getEmail().get(0).getSmtpAddress());
			
			//Email 2
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(1).getDisplayName(),kolabContact2_.getEmail().get(1).getDisplayName());
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(1).getSmtpAddress(),kolabContact2_.getEmail().get(1).getSmtpAddress());
			
			//Email 3
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(2).getDisplayName(),kolabContact2_.getEmail().get(2).getDisplayName());
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(2).getSmtpAddress(),kolabContact2_.getEmail().get(2).getSmtpAddress());
			
			//Email 4
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(3).getDisplayName(),kolabContact2_.getEmail().get(3).getDisplayName());
		//	org.junit.Assert.assertEquals(kolabContact1_.getEmail().get(3).getSmtpAddress(),kolabContact2_.getEmail().get(3).getSmtpAddress());

			///Phones
			//Phone 1
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(0).getType(),kolabContact2_.getPhone().get(0).getType());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(0).getNumber(),kolabContact2_.getPhone().get(0).getNumber());
			
			//Phone 2
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(1).getType(),kolabContact2_.getPhone().get(1).getType());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(1).getNumber(),kolabContact2_.getPhone().get(1).getNumber());
		
			//Phone 3
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(2).getType(),kolabContact2_.getPhone().get(2).getType());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(2).getNumber(),kolabContact2_.getPhone().get(2).getNumber());

			//Phone 4
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(3).getType(),kolabContact2_.getPhone().get(3).getType());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(3).getNumber(),kolabContact2_.getPhone().get(3).getNumber());

			///Addresses
			//Address 1
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getCountry(),kolabContact2_.getAddress().get(0).getCountry());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getLocality(),kolabContact2_.getAddress().get(0).getLocality());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getPostalCode(),kolabContact2_.getAddress().get(0).getPostalCode());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getRegion(),kolabContact2_.getAddress().get(0).getRegion());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getStreet(),kolabContact2_.getAddress().get(0).getStreet());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(0).getType(),kolabContact2_.getAddress().get(0).getType());
			
			//Address 2
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getCountry(),kolabContact2_.getAddress().get(1).getCountry());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getLocality(),kolabContact2_.getAddress().get(1).getLocality());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getPostalCode(),kolabContact2_.getAddress().get(1).getPostalCode());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getRegion(),kolabContact2_.getAddress().get(1).getRegion());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getStreet(),kolabContact2_.getAddress().get(1).getStreet());
			org.junit.Assert.assertEquals(kolabContact1_.getAddress().get(1).getType(),kolabContact2_.getAddress().get(1).getType());

			///Phones
			//Phone 1
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(0).getNumber(),kolabContact2_.getPhone().get(0).getNumber());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(0).getType(),kolabContact2_.getPhone().get(0).getType());
			//Phone 2
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(1).getNumber(),kolabContact2_.getPhone().get(1).getNumber());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(1).getType(),kolabContact2_.getPhone().get(1).getType());
			
			//Phone 3
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(2).getNumber(),kolabContact2_.getPhone().get(2).getNumber());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(2).getType(),kolabContact2_.getPhone().get(2).getType());
			
			//Phone 4
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(3).getNumber(),kolabContact2_.getPhone().get(3).getNumber());
			org.junit.Assert.assertEquals(kolabContact1_.getPhone().get(3).getType(),kolabContact2_.getPhone().get(3).getType());
			
		} catch (ConversionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testPattern(){
		
		com.funambol.common.pim.contact.Contact funambolContact1_ = new com.funambol.common.pim.contact.Contact();
		com.funambol.common.pim.contact.Contact funambolContact2_ = new com.funambol.common.pim.contact.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact1_ = new org.evolvis.bsi.kolab.service.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact2_ = new org.evolvis.bsi.kolab.service.Contact();

	}

	@Test
	public void testSingleFieldsFunambol2Kolab2Funambol(){
		
		com.funambol.common.pim.contact.Contact funambolContact1_ = new com.funambol.common.pim.contact.Contact();
		com.funambol.common.pim.contact.Contact funambolContact2_ = new com.funambol.common.pim.contact.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact1_ = new org.evolvis.bsi.kolab.service.Contact();
		org.evolvis.bsi.kolab.service.Contact kolabContact2_ = new org.evolvis.bsi.kolab.service.Contact();	
	
	}

	@Test
	public void testConvertDayOfWeekMask() {
		// check bijective
		for (short i = 0; i <= 127; i++) {
			assertEquals(i, Converter.convertDayOfWeekMaskFromKolab(
					Converter.convertDayOfWeekMaskToKolab(i)));
			assertEquals(i, Converter.convertDayOfWeekMaskToKolab(
					Converter.convertDayOfWeekMaskFromKolab(i)));
		}
	}

	@Test //(expected=IOException.class)
	public void testFunambolWeekMask() {
		assertEquals(1<<1, RecurrencePattern.DAY_OF_WEEK_MONDAY);
		assertEquals(1<<2, RecurrencePattern.DAY_OF_WEEK_TUESDAY);
		assertEquals(1<<3, RecurrencePattern.DAY_OF_WEEK_WEDNESDAY);
		assertEquals(1<<4, RecurrencePattern.DAY_OF_WEEK_THURSDAY);
		assertEquals(1<<5, RecurrencePattern.DAY_OF_WEEK_FRIDAY);
		assertEquals(1<<6, RecurrencePattern.DAY_OF_WEEK_SATURDAY);
		assertEquals(1<<0, RecurrencePattern.DAY_OF_WEEK_SUNDAY);
	}
	
	/* private static void asserEquals(Contact c1, Contact c2) {
		assertEquals(c1.getAnniversary(), c2.getAnniversary());
		assertEquals(c1.getAddress().size(), c2.getAddress().size());
		for (Address a : c1.getAddress())
			;
	} */

}
