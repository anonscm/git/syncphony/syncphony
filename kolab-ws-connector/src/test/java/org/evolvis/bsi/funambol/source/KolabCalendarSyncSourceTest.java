/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.junit.Assert.*;

import org.evolvis.bsi.kolab.service.Event;
import org.junit.Test;


public class KolabCalendarSyncSourceTest {

	@Test
	public void testAddFolderInfo() {
		Event event = new Event();
		
		KolabCalendarSyncSource.addFolderInfo("/bla/test/parent", event);
		assertEquals("[parent]", event.getSummary());
		
		event.setSummary("");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/parent", event);
		assertEquals("[parent]", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/parent/", event);
		assertEquals("[parent] summary", event.getSummary());

		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/parent", event);
		assertEquals("[parent] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("parent", event);
		assertEquals("[parent] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/", event);
		assertEquals("summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("", event);
		assertEquals("summary", event.getSummary());
		
		event.setSummary("s[ummary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/parent", event);
		assertEquals("[parent] s[ummary", event.getSummary());
		
		event.setSummary("s]ummary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/parent", event);
		assertEquals("[parent] s]ummary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/par[ent", event);
		assertEquals("[par[ent] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/par]ent", event);
		assertEquals("[par ent] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/par]e]nt", event);
		assertEquals("[par e nt] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test/ ", event);
		assertEquals("[ ] summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.addFolderInfo("/bla/test//", event);
		assertEquals("[test] summary", event.getSummary());
		
	}
	
	@Test
	public void testRemoveFolderInfo() {
		Event event = new Event();
		
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertNull(event.getSummary());
		
		event.setSummary("[parent] summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("summary", event.getSummary());
		
		event.setSummary("summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("summary", event.getSummary());
		
		event.setSummary("[] summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("[] summary", event.getSummary());
		
		event.setSummary("[par]ent] summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("ent] summary", event.getSummary());
		
		event.setSummary("[par[ent] summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("summary", event.getSummary());
		
		event.setSummary(" [parent] summary");
		KolabCalendarSyncSource.removeFolderInfo(event);
		assertEquals("summary", event.getSummary());
	}
}
