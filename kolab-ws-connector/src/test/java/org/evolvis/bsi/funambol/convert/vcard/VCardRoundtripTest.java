/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcard;

import static org.evolvis.bsi.funambol.source.Helper.*;

import java.util.Date;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.convert.TypeComparator;
import org.evolvis.bsi.kolab.service.Contact;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.converter.ConverterException;

public class VCardRoundtripTest {
	
	@Test
	public void testEmpty() throws ConverterException, ConversionException {
	}
	
	@Test
	public void testEmail() throws ConverterException, ConversionException {
		contact.setAnniversary(new Date(333 *1000L*60*60*24));
		org.evolvis.bsi.kolab.service.Email email = new org.evolvis.bsi.kolab.service.Email();
		email.setDisplayName("dn1");
		email.setSmtpAddress("smtp1");
		contact.getEmail().add(email);
		contact.setImAddress("im");
	}
	
	private Contact contact;
	
	@Before
	public void init() {
		contact = new Contact();
	}
	
	@After
	public void testContact() throws ConversionException, ConverterException {
		// convert to vcard
		byte[] pic = null;
		TimeZone tz = null;
		String charset = "UTF-8";
		com.funambol.common.pim.contact.Contact fcontact = Converter
				.convertContact(contact, pic);
		String content = createContent(fcontact, CONTENT_TYPE_VCARD, tz,
				charset);
		// convert back
		com.funambol.common.pim.contact.Contact fcontact2 = createContact(content, CONTENT_TYPE_VCARD, tz,
				charset);
		Contact contact2 = Converter.convertToContact(fcontact2);
		
		// compare
		TypeComparator.equals(contact, contact2, false);
	}
}
