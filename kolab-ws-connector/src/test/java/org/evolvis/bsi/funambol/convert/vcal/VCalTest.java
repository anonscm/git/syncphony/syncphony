/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcal;

import static junit.framework.Assert.*;
import static org.evolvis.bsi.funambol.source.Helper.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.convert.vcard.VcardTest;
import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.ShowTimeAs;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.funambol.common.pim.calendar.Calendar;
import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.converter.ConverterException;
import com.funambol.common.pim.xvcalendar.ParseException;

/**
 * Test VCal conversion
 * 
 * @author Hendrik Helwich
 *
 */
public class VCalTest {
	
	@Test
	public void testVCardFullConversion() throws IOException, ParseException, ConverterException, com.funambol.common.pim.icalendar.ParseException, SAXException, ConversionException {
		InputStream in = getClass().getResourceAsStream("htc-windows-vcal-simple.txt");
		String content = VcardTest.convertStreamToString(in);
		
		Calendar fcalendar = createCalendar(content, Helper.CONTENT_TYPE_VCAL2,
				TimeZone.getDefault(),
				"UTF-8");
		Event event = Converter.convertToEvent(fcalendar, null, null, "UTF-8");
		assertEquals(ShowTimeAs.TENTATIVE, event.getShowTimeAs());
	}
		
}
