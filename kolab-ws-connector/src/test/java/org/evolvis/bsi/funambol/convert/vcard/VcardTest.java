/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcard;


import static org.evolvis.bsi.funambol.source.Helper.*;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.contact.Email;
import com.funambol.common.pim.converter.ConverterException;

/**
 * Test Vcard conversion
 * 
 * @author Hendrik Helwich
 *
 */
public class VcardTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testVCardFullConversion() throws ConverterException, ConversionException, IOException {
		InputStream in = getClass().getResourceAsStream("windows-mobile-vcard-full.txt");
		String content = convertStreamToString(in);
		com.funambol.common.pim.contact.Contact fcontact = createContact(content, Helper.CONTENT_TYPE_VCARD,
				TimeZone.getDefault(),
				"UTF-8");
		
		// ------------------------------------------------- Test Funambol Data

		List<Email> pmails = fcontact.getPersonalDetail().getEmails();
		assertEquals(3, pmails.size());
		assertEquals("em@il.de", pmails.get(0).getPropertyValueAsString());
		assertEquals("Email1Address", pmails.get(0).getPropertyType());
		assertEquals("em@il2.de", pmails.get(1).getPropertyValueAsString());
		assertEquals("Email2Address", pmails.get(1).getPropertyType());
		// im and im2 will be overwritten 
		assertEquals("im", pmails.get(2).getPropertyValueAsString()); //There can be just one im address because the kolab can't save more than one
		assertEquals("IMAddress", pmails.get(2).getPropertyType());

		List<Email> bmails = fcontact.getBusinessDetail().getEmails();
		assertEquals(1, bmails.size());
		assertEquals("em@il3.de", bmails.get(0).getPropertyValueAsString());
		assertEquals("Email3Address", bmails.get(0).getPropertyType());
		
		
		Contact contact = Converter.convertToContact(fcontact);

		// ------------------------------------------------- Test Kolab Data
		
//		N:Nachname;Vorname;Vorname2;Anrede;Titel
		assertEquals("Nachname", contact.getLastName());
		assertEquals("Vorname", contact.getGivenName());
		assertEquals("Vorname2", contact.getMiddleNames());
		assertEquals("Anrede", contact.getPrefix());
		assertEquals("Titel", contact.getSuffix());
		
		//TODO test other fields
		
//		BDAY:20091105
//		NOTE;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Notizen=0D=0A
//		TEL;WORK;FAX;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Fax B=C3=BCro
//		TEL;VOICE;WORK;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Tel B=C3=BCro
//		TEL;VOICE;WORK:Tel2 Buro
//		TEL;CAR;VOICE:Autotelefon
//		CATEGORIES;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Gesch=C3=A4ftlich, Pers=C3=B6nlich
//		TEL;WORK;PREF:Tel Firma
//		FN:Nachname, Vorname Vorbame2
//		EMAIL;INTERNET:em@il.de
//		EMAIL;INTERNET;HOME:em@il2.de
//		EMAIL;INTERNET;WORK:em@il3.de
		assertEquals(3, contact.getEmail().size());
		assertEquals("em@il.de", contact.getEmail().get(0).getSmtpAddress());
		assertEquals("em@il2.de", contact.getEmail().get(1).getSmtpAddress());
		assertEquals("em@il3.de", contact.getEmail().get(2).getSmtpAddress());
//		TITLE:Position
//		TEL;VOICE;HOME:Tel Privat
//		TEL;VOICE;HOME:Tel2 Privat
//		TEL;HOME;FAX:Fax Privat
//		TEL;CELL:Handy
//		NICKNAME:Spitzname
//		TEL;PAGER:Pager
//		URL:webseite.de
//		ORG;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Firma;Abteilung;B=C3=BCro
//		ADR;HOME:;;Pa Strasse;Pa Ort;Pa Region;Pa Plz;Pa Land
//		ADR:;;Wa Strasse;Wa Ort;Wa Region;Wa Plz;Wa land
//		ADR;WORK:;;Ab Strasse;Ab ort;Ab Region;Ab plz;Ab Land
//		PHOTO:
//		X-ANNIVERSARY:20091106
//		X-FUNAMBOL-CHILDREN:Kinder
//		X-FUNAMBOL-CUSTOMERID:Kundennr
//		X-FUNAMBOL-GOVERNMENTID:Regid
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im2
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im3
		assertEquals("im", contact.getImAddress()); // im and im2 are overwritten (see above)
		//There can be just one im address because the kolab can't save more than one
//		X-MANAGER:Vorgesetzter
//		TEL;X-FUNAMBOL-RADIO:Funktelefon
//		X-SPOUSE:Partner
//		X-FUNAMBOL-YOMICOMPANYNAME:
//		X-FUNAMBOL-YOMIFIRSTNAME:
//		X-FUNAMBOL-YOMILASTNAME:
	}
	
	
	

	@Test
	public void testVCardFullHtcConversion() throws ConverterException, ConversionException, IOException {
		/* Tel Büro: 11111
		 * Fax Büro: 22222
		 * Handy: 33333
		 * Klingeltion: Alarm-Classic
		 * Tel privat: 444444
		 * Pager: 55555
		 * Autotelefon: 66666
		 * Fax privat: 77777
		 * Tel Firma: 88888
		 * Tel2 Büro: 999999
		 * Tel2 privat: 123123
		 * Funktelefon: 234234
		 * Tel Sekretariat: 345345
		 * Geburtstag: 16.11.2009
		 * Jahrestag: 17.11.2009
		 */
		InputStream in = getClass().getResourceAsStream("htc-windows-vcard-full.txt");
		String content = convertStreamToString(in);
		com.funambol.common.pim.contact.Contact fcontact = createContact(content, Helper.CONTENT_TYPE_VCARD,
				TimeZone.getDefault(),
				"UTF-8");
		
		// ------------------------------------------------- Test Funambol Data

		List<Email> pmails = fcontact.getPersonalDetail().getEmails();
		assertEquals(3, pmails.size());
		assertEquals("em@il.de", pmails.get(0).getPropertyValueAsString());
		assertEquals("Email1Address", pmails.get(0).getPropertyType());
		assertEquals("em@il2.de", pmails.get(1).getPropertyValueAsString());
		assertEquals("Email2Address", pmails.get(1).getPropertyType());
		// im and im2 will be overwritten 
		assertEquals("im", pmails.get(2).getPropertyValueAsString());
		assertEquals("IMAddress", pmails.get(2).getPropertyType());

		List<Email> bmails = fcontact.getBusinessDetail().getEmails();
		assertEquals(1, bmails.size());
		assertEquals("em@il3.de", bmails.get(0).getPropertyValueAsString());
		assertEquals("Email3Address", bmails.get(0).getPropertyType());
		
		
		Contact contact = Converter.convertToContact(fcontact);

		// ------------------------------------------------- Test Kolab Data
		
//		N:Nachname;Vorname;Vorname2;Anrede;Titel
		assertEquals("Nachname", contact.getLastName());
		assertEquals("Vorname", contact.getGivenName());
		assertEquals("Vorname2", contact.getMiddleNames());
		assertEquals("Anrede", contact.getPrefix());
		assertEquals("Titel", contact.getSuffix());
		
		//TODO test other fields
		
//		BDAY:20091105
//		NOTE;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Notizen=0D=0A
//		TEL;WORK;FAX;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Fax B=C3=BCro
//		TEL;VOICE;WORK;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Tel B=C3=BCro
//		TEL;VOICE;WORK:Tel2 Buro
//		TEL;CAR;VOICE:Autotelefon
//		CATEGORIES;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Gesch=C3=A4ftlich, Pers=C3=B6nlich
//		TEL;WORK;PREF:Tel Firma
//		FN:Nachname, Vorname Vorbame2
//		EMAIL;INTERNET:em@il.de
//		EMAIL;INTERNET;HOME:em@il2.de
//		EMAIL;INTERNET;WORK:em@il3.de
		assertEquals(3, contact.getEmail().size());
		assertEquals("em@il.de", contact.getEmail().get(0).getSmtpAddress());
		assertEquals("em@il2.de", contact.getEmail().get(1).getSmtpAddress());
		assertEquals("em@il3.de", contact.getEmail().get(2).getSmtpAddress());
//		TITLE:Position
//		TEL;VOICE;HOME:Tel Privat
//		TEL;VOICE;HOME:Tel2 Privat
//		TEL;HOME;FAX:Fax Privat
//		TEL;CELL:Handy
//		NICKNAME:Spitzname
//		TEL;PAGER:Pager
//		URL:webseite.de
//		ORG;ENCODING=QUOTED-PRINTABLE;CHARSET=UTF-8:Firma;Abteilung;B=C3=BCro
//		ADR;HOME:;;Pa Strasse;Pa Ort;Pa Region;Pa Plz;Pa Land
//		ADR:;;Wa Strasse;Wa Ort;Wa Region;Wa Plz;Wa land
//		ADR;WORK:;;Ab Strasse;Ab ort;Ab Region;Ab plz;Ab Land
//		PHOTO:
//		X-ANNIVERSARY:20091106
//		X-FUNAMBOL-CHILDREN:Kinder
//		X-FUNAMBOL-CUSTOMERID:Kundennr
//		X-FUNAMBOL-GOVERNMENTID:Regid
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im2
//		EMAIL;INTERNET;HOME;X-FUNAMBOL-INSTANTMESSENGER:im3
		assertEquals("im", contact.getImAddress()); //There can be just one im address because the kolab can't save more than one
//		X-MANAGER:Vorgesetzter
//		TEL;X-FUNAMBOL-RADIO:Funktelefon
//		X-SPOUSE:Partner
//		X-FUNAMBOL-YOMICOMPANYNAME:
//		X-FUNAMBOL-YOMIFIRSTNAME:
//		X-FUNAMBOL-YOMILASTNAME:
	}
	
	


	@Test
	public void testVCardS60SimpleConversion() throws ConverterException, ConversionException, IOException {
		/*
N:Nachname;Vorname;;;
FN:voller Name
NICKNAME:Spitzname
TEL;VOICE;HOME:123123123
UID:f7bd3d6d-7d00-4bef-b2b7-abbf844e7841
		 */
		
		InputStream in = getClass().getResourceAsStream("sw-s60-vcard-simple.txt");
		String content = convertStreamToString(in);
		com.funambol.common.pim.contact.Contact fcontact = createContact(content, Helper.CONTENT_TYPE_VCARD,
				TimeZone.getDefault(),
				"UTF-8");
		
		// ------------------------------------------------- Test Funambol Data
		
		Contact contact = Converter.convertToContact(fcontact);

		// ------------------------------------------------- Test Kolab Data
		
		assertEquals("Nachname", contact.getLastName());
		assertEquals("Vorname", contact.getGivenName());
		assertEquals("voller Name", contact.getFullName());
		assertEquals("Spitzname", contact.getNickName());

		assertEquals(1, contact.getPhone().size());
		assertEquals(PhoneType.HOME_1, contact.getPhone().get(0).getType());
		assertEquals("123123123", contact.getPhone().get(0).getNumber());
		assertEquals("f7bd3d6d-7d00-4bef-b2b7-abbf844e7841", contact.getUid());
		
		// other fields must be empty

		assertEquals(0, contact.getAddress().size());
		assertEquals(0, contact.getEmail().size());
		assertEquals(0, contact.getInlineAttachment().size());
		assertEquals(0, contact.getLinkAttachment().size());

		assertNull(contact.getAnniversary());
		assertNull(contact.getAssistant());
		assertNull(contact.getBirthday());
		assertNull(contact.getBody());
		assertNull(contact.getCategories());
		assertNull(contact.getChildren());
		assertNull(contact.getCreationDate());
		assertNull(contact.getDepartment());
		assertNull(contact.getFreeBusyUrl());
		assertNull(contact.getGender());
		assertNull(contact.getImAddress());
		assertNull(contact.getInitials());
		assertNull(contact.getJobTitle());
		assertNull(contact.getLanguage());
		assertNull(contact.getLastModificationDate());
		assertNull(contact.getLatitude());
		assertNull(contact.getLongitude());
		assertNull(contact.getManagerName());
		assertNull(contact.getMiddleNames());
		assertNull(contact.getOfficeLocation());
		assertNull(contact.getPictureAttachmentId());
		assertNull(contact.getPreferredAddress());
		assertNull(contact.getPrefix());
		assertNull(contact.getProductId());
		assertNull(contact.getProfession());
		assertEquals(Sensitivity.PUBLIC, contact.getSensitivity());
		assertNull(contact.getSpouseName());
		assertNull(contact.getSuffix());
		assertNull(contact.getWebPage());
	}
	
    public static String convertStreamToString(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null)
				sb.append(line + "\n");
		} finally {
			is.close();
		}
		return sb.toString();
	}

}
