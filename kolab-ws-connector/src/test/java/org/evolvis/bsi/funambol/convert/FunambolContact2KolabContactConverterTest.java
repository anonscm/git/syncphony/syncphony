/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

// JUnit
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.common.Property;
import com.funambol.common.pim.contact.Contact;
import java.lang.reflect.*;

// Kolab Datatypes
//import org.evolvis.bsi.kolab.service.Contact;

public class FunambolContact2KolabContactConverterTest {
	
	Contact fContact;
	
	
	@Before
	public void setUp() throws Exception {
		 fContact = new com.funambol.common.pim.contact.Contact();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void testConvertVorname2Kolab()
	{
		
		final Property fName = fContact.getName().getFirstName();
		fName.setPropertyValue("Vorname");
		org.evolvis.bsi.kolab.service.Contact kContact = null; 
		try {
			kContact = Converter.convertToContact(fContact);
			assertEquals("Vorname", kContact.getGivenName());
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(0, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}
	
	@Test
	public void testConvertLastname2Kolab()
	{
		
		final Property fName = fContact.getName().getLastName();
		fName.setPropertyValue("Lastname");
		org.evolvis.bsi.kolab.service.Contact kContact = null; 
		try {
			kContact = Converter.convertToContact(fContact);
			assertEquals("Lastname", kContact.getLastName());
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(0, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}

	@Test
	public void testConvertFullName2Kolab() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException
	{
	    Class params[] = {};
		final Property fName = fContact.getName().getDisplayName();
		String value = "getFullName";
		fName.setPropertyValue(value);
		org.evolvis.bsi.kolab.service.Contact kContact = null; 
		try {
			kContact = Converter.convertToContact(fContact);		
			Method callee = org.evolvis.bsi.kolab.service.Contact.class.getDeclaredMethod(value, params);
			assertEquals(value, callee.invoke(kContact));
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(0, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}

	@Test
	public void testConvertInitials2Kolab() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException
	{
	    Class params[] = {};
		final Property fName = fContact.getName().getInitials();
		String value = "getInitials";
		fName.setPropertyValue(value);
		org.evolvis.bsi.kolab.service.Contact kContact = null;
		
		try {
			kContact = Converter.convertToContact(fContact);		
			Method callee = org.evolvis.bsi.kolab.service.Contact.class.getDeclaredMethod(value, params);
			assertEquals(value, callee.invoke(kContact));
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(0, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}
	
	@Test
	public void testConvertgetMiddleNames2Kolab() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException
	{
	    Class params[] = {};
		final Property fName = fContact.getName().getMiddleName();
		String value = "getMiddleNames";
		fName.setPropertyValue(value);
		org.evolvis.bsi.kolab.service.Contact kContact = null;
		
		try {
			kContact = Converter.convertToContact(fContact);		
			Method callee = org.evolvis.bsi.kolab.service.Contact.class.getDeclaredMethod(value, params);
			assertEquals(value, callee.invoke(kContact));
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(0, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}
	
	@Test
	@Ignore
	public void testConvertgetNickname2Kolab() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException
	{
	    Class params[] = {};
		final Property fName = fContact.getName().getNickname();
		String value = "getNickName";
		fName.setPropertyValue(value);
		org.evolvis.bsi.kolab.service.Contact kContact = null;
		
		try {
			kContact = Converter.convertToContact(fContact);		
			Method callee = org.evolvis.bsi.kolab.service.Contact.class.getDeclaredMethod(value, params);
			assertEquals(value, callee.invoke(kContact));
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(3, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}

	// Is this missing in Kolab Data Model
	@Ignore
	@Test
	public void testConvertgetSalutatio2Kolab() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException
	{
	    Class params[] = {};
		final Property fName = fContact.getName().getSalutation();
		String value = "getSaluation";
		fName.setPropertyValue(value);
		org.evolvis.bsi.kolab.service.Contact kContact = null;
		
		try {
			kContact = Converter.convertToContact(fContact);		
			Method callee = org.evolvis.bsi.kolab.service.Contact.class.getDeclaredMethod(value, params);
			assertEquals(value, callee.invoke(kContact));
			List<org.evolvis.bsi.kolab.service.Address> add = kContact.getAddress();
			// Business, Home, ...
			assertEquals(3, kContact.getAddress().size());
			assertEquals(null, kContact.getAnniversary());
		} catch (ConversionException e) {
			fail("Conversion failed due ConversionException");
		}
	}
	

}
