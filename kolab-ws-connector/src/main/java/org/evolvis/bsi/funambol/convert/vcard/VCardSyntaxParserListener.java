/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcard;

import java.util.TimeZone;

import com.funambol.common.pim.ParamList;
import com.funambol.common.pim.contact.Contact;
//import com.funambol.common.pim.vcard.VCardSyntaxParserListenerImpl;

public class VCardSyntaxParserListener {
	
	private final VCardSyntaxParserListenerImplExtended delegate;
	
	private final Contact contact;

	public VCardSyntaxParserListener(Contact contact, TimeZone object,
			String defaultCharset) {
		delegate = new VCardSyntaxParserListenerImplExtended(contact, object, defaultCharset);
		this.contact = contact;
	}

	private static com.funambol.common.pim.vcard.Token convertToken(Token group) {
		if (group == null)
			return null;
		com.funambol.common.pim.vcard.Token token = new com.funambol.common.pim.vcard.Token();
		token.image = group.image;
		return token;
	}

    // delegating operations
    
	public void start() {
		delegate.start();
	}

	public void end() {
		delegate.end();
	}

	public void setCategories(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setCategories(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void addExtension(String image, String content, ParamList plist,
			Token group) throws ParseException {
		try {
			delegate.addExtension(image, content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setVersion(String image, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setVersion(image, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setTitle(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setTitle(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setMail(String content, ParamList plist, Token group) throws ParseException {
		try {
			content = content.trim();
			if (content.length() > 0)
				delegate.setMail(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setUrl(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setUrl(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setTelephone(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setTelephone(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setFName(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setFName(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setRole(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setRole(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setRevision(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setRevision(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setNickname(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setNickname(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}
	
	public void setOrganization(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setOrganization(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}
	
	public void setAssistant(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setAssistant(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setAddress(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setAddress(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setBirthday(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setBirthday(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setLabel(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setLabel(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setTimezone(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setTimezone(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setLogo(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setLogo(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setNote(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setNote(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setUid(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setUid(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setPhoto(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setPhoto(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setName(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setName(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setFolder(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setFolder(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setFreebusy(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setFreebusy(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setAnniversary(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setAnniversary(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setChildren(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setChildren(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setCompanies(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setCompanies(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setLanguages(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setLanguages(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setManager(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setManager(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setMileage(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setMileage(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setSpouse(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setSpouse(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public void setSubject(String content, ParamList plist, Token group) throws ParseException {
		try {
			delegate.setSubject(content, plist, convertToken(group));
		} catch (com.funambol.common.pim.vcard.ParseException e) {
			throw new ParseException(e.getMessage());
		}
	}

}
