/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

//import static org.evolvis.bsi.funambol.source.Helper.long2utc;
//import static org.evolvis.bsi.funambol.source.Helper.utc2long;
//import org.evolvis.bsi.funambol.source.Helper;
//import org.evolvis.bsi.funambol.source.Helper;
//import com.funambol.common.pim.calendar.RecurrencePattern;
//import com.funambol.common.pim.common.PropertyWithTimeZone;
//import com.funambol.framework.core.Status;
//import com.funambol.framework.core.Property;
//import com.funambol.framework.core.Status;

import static org.evolvis.bsi.funambol.source.Helper.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.kolab.service.Address;
import org.evolvis.bsi.kolab.service.AddressType;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Email;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Gender;
import org.evolvis.bsi.kolab.service.PhoneType;
import org.evolvis.bsi.kolab.service.Note;
import org.evolvis.bsi.kolab.service.Role;
import org.evolvis.bsi.kolab.service.Sensitivity;
import org.evolvis.bsi.kolab.service.ShowTimeAs;
import org.evolvis.bsi.kolab.service.Task;
import com.funambol.common.pim.calendar.Calendar;
import com.funambol.common.pim.calendar.Reminder;
import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.common.Property;
import com.funambol.common.pim.contact.BusinessDetail;
import com.funambol.common.pim.contact.Name;
import com.funambol.common.pim.contact.PersonalDetail;
import com.funambol.common.pim.contact.Photo;

/**
 * @author Hendrik Helwich, Patrick Apel tarent GmbH
 *
 */
public class Converter {
	
	/* If you want to synchronize a country from a windows mobile device to kolab, you can't write
	 * Afghanistan for example in the input field of the device, instead you have to write 
	 * AF (for Afghanistan) in capital letters.
	 * See other capital letters which match countries below.
	 * 
	 <option value="AF">Afghanistan</option>
	 <option value="AL">Albanien</option>
	 <option value="DZ">Algerien</option>
	 <option value="AS">Amerikanisch-Samoa</option>
	 <option value="AD">Andorra</option>
	 <option value="AO">Angola</option>
	 <option value="AI">Anguilla</option>
	 <option value="AQ">Antarktis</option>
	 <option value="AG">Antigua und Barbuda</option>
	 <option value="AR">Argentinien</option>
	 <option value="AM">Armenien</option>
	 <option value="AW">Aruba</option>
	 <option value="AU">Australien</option>
	 <option value="AT">Österreich</option>
	 <option value="AZ">Aserbaidschan</option>
	 <option value="BS">Bahamas</option>
	 <option value="BH">Bahrein</option>
	 <option value="BD">Bangladesch</option>
	 <option value="BB">Barbados</option>
	 <option value="BY">Weißruland</option>
	 <option value="BE">Belgien</option>
	 <option value="BZ">Belize</option>
	 <option value="BJ">Benin</option>
	 <option value="BM">Bermudas</option>
	 <option value="BT">Bhutan</option>
	 <option value="BO">Bolivien</option>
	 <option value="BA">Bosnien-Herzegowina</option>
	 <option value="BW">Botswana</option>
	 <option value="BV">Bouvetinsel</option>
	 <option value="BR">Brasilien</option>
	 <option value="IO">Britisches Territorium im Indischen Ozean</option>
	 <option value="BN">Brunei Darussalam</option>
	 <option value="BG">Bulgarien</option>
	 <option value="BF">Burkina Faso</option>
	 <option value="BI">Burundi</option>
	 <option value="KH">Kambodscha</option>
	 <option value="CM">Kamerun</option>
	 <option value="CA">Kanada</option>
	 <option value="CV">Kap Verde</option>
	 <option value="KY">Cayman-Inseln</option>
	 <option value="CF">Zentralafrikanische Republik</option>
	 <option value="TD">Tschad</option>
	 <option value="CL">Chile</option>
	 <option value="CN">China</option>
	 <option value="CX">Weihnachtsinsel</option>
	 <option value="CC">Kokosinseln</option>
	 <option value="CO">Kolumbien</option>
	 <option value="KM">Komoren</option>
	 <option value="CG">Kongo</option>
	 <option value="CD">Demokratische Republic Kongo</option>
	 <option value="CK">Cook-Inseln</option>
	 <option value="CR">Costa Rica</option>
	 <option value="CI">Elfenbeinküste</option>
	 <option value="HR">Kroatien</option>
	 <option value="CU">Kuba</option>
	 <option value="CY">Zypern</option>
	 <option value="CZ">Tschechische Republik</option>
	 <option value="DK">Dänemark</option>
	 <option value="DJ">Dschibuti</option>
	 <option value="DM">Dominica</option>
	 <option value="DO">Dominikanische Republik</option>
	 <option value="EC">Ecuador</option>
	 <option value="EG">Ägypten</option>
	 <option value="SV">El Salvador</option>
	 <option value="GQ">Äquatorial-Guinea</option>
	 <option value="ER">Eritrea</option>
	 <option value="EE">Estland</option>
	 <option value="ET">Äthiopien</option>
	 <option value="FK">Falklandinseln</option>
	 <option value="FO">Faröer</option>
	 <option value="FJ">Fidschi</option>
	 <option value="FI">Finnland</option>
	 <option value="FR">Frankreich</option>
	 <option value="GF">Französisch-Guayana</option>
	 <option value="PF">Französisch-Polynesien</option>
	 <option value="TF">Französische Gebiete im südlichen Indischen Ozean</option>
	 <option value="GA">Gabun</option>
	 <option value="GM">Gambia</option>
	 <option value="GE">Georgien</option>
	 <option value="DE" selected="selected">Deutschland</option>
	 <option value="GH">Ghana</option>
	 <option value="GI">Gibraltar</option>
	 <option value="GR">Griechenland</option>
	 <option value="GL">Grönland</option>
	 <option value="GD">Grenada</option>
	 <option value="GP">Guadeloupe</option>
	 <option value="GU">Guam</option>
	 <option value="GT">Guatemala</option>
	 <option value="GN">Guinea</option>
	 <option value="GW">Guinea-Bissau</option>
	 <option value="GY">Guyana</option>
	 <option value="HT">Haiti</option>
	 <option value="HM">Heard- und McDonaldinseln</option>
	 <option value="VA">Vatikanstadt</option>
	 <option value="HN">Honduras</option>
	 <option value="HK">Hong Kong</option>
	 <option value="HU">Ungarn</option>
	 <option value="IS">Island</option>
	 <option value="IN">Indien</option>
	 <option value="ID">Indonesien</option>
	 <option value="IR">Islamische Republik Iran</option>
	 <option value="IQ">Irak</option>
	 <option value="IE">Irland</option>
	 <option value="IL">Israel</option>
	 <option value="IT">Italien</option>
	 <option value="JM">Jamaika</option>
	 <option value="JP">Japan</option>
	 <option value="JO">Jordanien</option>
	 <option value="KZ">Kasachstan</option>
	 <option value="KE">Kenia</option>
	 <option value="KI">Kiribati</option>
	 <option value="KP">Demokratische Volksrepublic Korea</option>
	 <option value="KR">Republik Korea</option>
	 <option value="KW">Kuwait</option>
	 <option value="KG">Kirgisistan</option>
	 <option value="LA">Laos</option>
	 <option value="LV">Lettland</option>
	 <option value="LB">Libanon</option>
	 <option value="LS">Lesotho</option>
	 <option value="LR">Liberia</option>
	 <option value="LY">Libyen</option>
	 <option value="LI">Liechtenstein</option>
	 <option value="LT">Litauen</option>
	 <option value="LU">Luxemburg</option>
	 <option value="MO">Macao</option>
	 <option value="MK">Die ehemalige Jugoslawische Republik Mazedonien</option>
	 <option value="MG">Madagaskar</option>
	 <option value="MW">Malawi</option>
	 <option value="MY">Malaysia</option>
	 <option value="MV">Malidiven</option>
	 <option value="ML">Mali</option>
	 <option value="MT">Malta</option>
	 <option value="MH">Marshallinseln</option>
	 <option value="MQ">Martinique</option>
	 <option value="MR">Mauretanien</option>
	 <option value="MU">Mauritius</option>
	 <option value="YT">Mayotte Island</option>
	 <option value="MX">Mexiko</option>
	 <option value="FM">Föderierte Staaten von Mikronesien</option>
	 <option value="MD">Republik Moldau</option>
	 <option value="MC">Monaco</option>
	 <option value="MN">Mongolei</option>
	 <option value="MS">Montserrat</option>
	 <option value="MA">Marokko</option>
	 <option value="MZ">Mosambik</option>
	 <option value="MM">Burma</option>
	 <option value="NA">Namibia</option>
	 <option value="NR">Nauru</option>
	 <option value="NP">Nepal</option>
	 <option value="NL">Niederlande</option>
	 <option value="AN">Niederländische Antillen</option>
	 <option value="NC">Neukaledonien</option>
	 <option value="NZ">Neuseeland</option>
	 <option value="NI">Nicaragua</option>
	 <option value="NE">Niger</option>
	 <option value="NG">Nigeria</option>
	 <option value="NU">Niue</option>
	 <option value="NF">Norfolkinsel</option>
	 <option value="MP">Nördliche Marianen</option>
	 <option value="NO">Norwegen</option>
	 <option value="OM">Oman</option>
	 <option value="PK">Pakistan</option>
	 <option value="PW">Palau</option>
	 <option value="PS">Besetzte Palästinensische Gebiete</option>
	 <option value="PA">Panama</option>
	 <option value="PG">Papua-Neuguinea</option>
	 <option value="PY">Paraguay</option>
	 <option value="PE">Peru</option>
	 <option value="PH">Philippinen</option>
	 <option value="PN">Pitcairn</option>
	 <option value="PL">Polen</option>
	 <option value="PT">Portugal</option>
	 <option value="PR">Puerto Rico</option>
	 <option value="QA">Katar</option>
	 <option value="RE">Réunion</option>
	 <option value="RO">Rumänien</option>
	 <option value="RU">Russische Förderation</option>
	 <option value="RW">Ruanda</option>
	 <option value="SH">Saint Helena</option>
	 <option value="KN">Saint Kitts und Nevis</option>
	 <option value="LC">Saint Lucia</option>
	 <option value="PM">Saint Pierre und Miquelon</option>
	 <option value="VC">Saint Vincent und die Grenadinen</option>
	 <option value="WS">Samoa</option>
	 <option value="SM">San Marino</option>
	 <option value="ST">Sao Tome und Principe</option>
	 <option value="SA">Saudi-Arabien</option>
	 <option value="SN">Senegal</option>
	 <option value="CS">Serbien und Montenegro</option>
	 <option value="SC">Seychellen</option>
	 <option value="SL">Sierra Leone</option>
	 <option value="SG">Singapur</option>
	 <option value="SK">Slowakei</option>
	 <option value="SI">Slowenien</option>
	 <option value="SB">Solomoninseln</option>
	 <option value="SO">Somalien</option>
	 <option value="ZA">Südafrika</option>
	 <option value="GS">Südgeorgien und die Südlichen Sandwichinseln</option>
	 <option value="ES">Spanien</option>
	 <option value="LK">Sri Lanka</option>
	 <option value="SD">Sudan</option>
	 <option value="SR">Surinam</option>
	 <option value="SJ">Svalbard und Jan Mayen</option>
	 <option value="SZ">Swasiland</option>
	 <option value="SE">Schweden</option>
	 <option value="CH">Schweiz</option>
	 <option value="SY">Arabische Republik Syrien</option>
	 <option value="TW">Taiwan, chinesische Provinz</option>
	 <option value="TJ">Tadschikistan</option>
	 <option value="TZ">Vereinigte Republik Tansania</option>
	 <option value="TH">Thailand</option>
	 <option value="TL">Timor-Leste</option>
	 <option value="TG">Togo</option>
	 <option value="TK">Tokelau</option>
	 <option value="TO">Tonga</option>
	 <option value="TT">Trinidad und Tobago</option>
	 <option value="TN">Tunesien</option>
	 <option value="TR">Türkei</option>
	 <option value="TM">Turkmenistan</option>
	 <option value="TC">Turks- und Caicosinseln</option>
	 <option value="TV">Tuvalu</option>
	 <option value="UG">Uganda</option>
	 <option value="UA">Ukraine</option>
	 <option value="AE">Vereinigte Arabische Emirate</option>
	 <option value="GB">Vereinigtes Königreich</option>
	 <option value="US">Vereinigte Staaten</option>
	 <option value="UM">Kleinere Amerikanische Überseeinseln</option>
	 <option value="UY">Uruguay</option>
	 <option value="UZ">Usbekistan</option>
	 <option value="VU">Vanuatu</option>
	 <option value="VE">Venezuela</option>
	 <option value="VN">Vietnam</option>
	 <option value="VG">Britische Jungferninseln</option>
	 <option value="VI">Amerikanische Jungferninseln</option>
	 <option value="WF">Wallis und Futuna</option>
	 <option value="EH">Westsahara</option>
	 <option value="YE">Jemen</option>
	 <option value="ZM">Sambia</option>
	 <option value="ZW">Simbabwe</option> */
	
	private static String[] strUml = new String[] {"=C3=9F","=C3=84","=C3=A4","=C3=9C","=C3=BC","=C3=96","=C3=B6"};
	private static String[] strErsUml = new String[] {"ß","Ä","ä","Ü","ü","Ö","ö"};
	private static String[] strArFunambolEmailType = new String[] {"Email1Address","Email2Address","Email3Address","IMAddress"};
	private static String[] strArFunambolPhoneType = new String[] {"HomeTelephoneNumber","Home2TelephoneNumber","MobileTelephoneNumber","HomeFaxNumber","CarTelephoneNumber","RadioTelephoneNumber"};
	private static String[] strArFunambolPhoneType_ = new String[]{"BusinessTelephoneNumber","Business2TelephoneNumber","PagerNumber","BusinessFaxNumber","CompanyMainTelephoneNumber"};
	public static final String CONTENT_TYPE_VCAL2 = "text/x-vcalendar"; 
	public static final String CONTENT_TYPE_SIF_TASK = "text/x-s4j-sift";
	
	public static final com.funambol.common.pim.contact.Contact convertContact(Contact kolabContact, byte[] pic) throws ConversionException {
		com.funambol.common.pim.contact.Contact contact = convertContact(kolabContact);
		if  (pic != null)
		{
			Photo photo = new Photo();
			photo.setImage(pic);
			contact.getPersonalDetail().setPhotoObject(photo);
		}
		return contact;
	}
	//Convert "Kolab contact" to "Funambol contact"
	@SuppressWarnings({ "unchecked" })
	public static final com.funambol.common.pim.contact.Contact convertContact(Contact kolabContact) throws ConversionException {
		
		//Funambol contact object
		com.funambol.common.pim.contact.Contact funambolContact = new com.funambol.common.pim.contact.Contact();
	    
		//Funambol objects
		BusinessDetail funambolBusinessDetail = new BusinessDetail();
		PersonalDetail funambolPersonalDetail = new PersonalDetail();
		
		///Names
		Name funambolName = new Name();
		
		//Given name
		String givenName = "";
		if(kolabContact.getGivenName() != null){
		givenName = kolabContact.getGivenName();
		funambolName.getFirstName().setPropertyValue(givenName);
		}
		
		//Last name
		String lastName = "";
		if(kolabContact.getLastName() != null){
			lastName = kolabContact.getLastName();
			funambolName.getLastName().setPropertyValue(lastName);
		}
		
		//Full name
		if(kolabContact.getFullName() != null)
		{
			if(givenName != null && lastName != null && givenName != "" && lastName != "") {
			funambolName.getDisplayName().setPropertyValue(givenName + " " + lastName);
			} else {
				funambolName.getDisplayName().setPropertyValue(kolabContact.getFullName());
			}
		}
		else
		{
			String separator = givenName.equals("") ? "" : ", ";
			if (!lastName.equals(""))
				funambolName.getDisplayName().setPropertyValue(lastName + separator + givenName);
		}
		
		//Nickname
		if(kolabContact.getNickName() != null){
		funambolName.getNickname().setPropertyValue(kolabContact.getNickName());
		}
		
		//Middle name
		if(kolabContact.getMiddleNames() != null){
		funambolName.getMiddleName().setPropertyValue(kolabContact.getMiddleNames());
		}
		
		//Prefix
		if(kolabContact.getPrefix() != null){
		funambolName.getSalutation().setPropertyValue(kolabContact.getPrefix());
		}
		
		//Suffix
		if(kolabContact.getSuffix() != null){
		funambolName.getSuffix().setPropertyValue(kolabContact.getSuffix());
		}
		
		//Initialen
		if(kolabContact.getInitials() != null){
		funambolName.getInitials().setPropertyValue(kolabContact.getInitials());
		}
		
		funambolContact.setName(funambolName);
		///////////////// Manager name ////////////
		//Manager name
		//Workaround begin: because of bug in funambol "(Contact) parser.vCard();"
		// ß => "=C3=9F" 
		// Ä => "=C3=84" 
		// ä => "=C3=A4" 
		// Ü => "=C3=9C" 
		// ü => "=C3=BC" 
		// Ö => "=C3=96" 
		// ö => "=C3=B6"
		String strManager = null;
		strManager = kolabContact.getManagerName();
		if(strManager != null){
		for(int i = 0; i < strUml.length; i++){ 
		if(strManager.indexOf(strUml[i]) != -1){
		strManager = strManager.replaceAll(strUml[i], strErsUml[i]);
		}
		}
		funambolBusinessDetail.setManager(strManager);	
		}
		//Workaround end
		
		
		//EMail
		int countEmail = kolabContact.getEmail().size();
		if(countEmail != 0){
		String strEmailAddress = null;
		com.funambol.common.pim.contact.Email[] funambolEmail = new com.funambol.common.pim.contact.Email[countEmail];
		
		for(int i=0; i < countEmail; i++){ // Kolab does not decide between business and private emails.
			
			if(i == 3){break;} //Remove later?!
			
			strEmailAddress = kolabContact.getEmail().get(i).getSmtpAddress();
			if (strEmailAddress != null) {
				funambolEmail[i] = new com.funambol.common.pim.contact.Email(strEmailAddress);

				funambolEmail[i].setPropertyType(strArFunambolEmailType[i]);
				funambolPersonalDetail.getEmails().add(funambolEmail[i]);
			}
		}		
		}
		
		//Instant Messenger
		if(kolabContact.getImAddress() != null){
			com.funambol.common.pim.contact.Email funambolEmail_ = new com.funambol.common.pim.contact.Email();
			funambolEmail_ = new com.funambol.common.pim.contact.Email(kolabContact.getImAddress());
			funambolEmail_.setPropertyType(strArFunambolEmailType[3]);	
			funambolPersonalDetail.getEmails().add(funambolEmail_);
		}
		//Phone
		int countPhone = kolabContact.getPhone().size();
		if(countPhone != 0){
		String strPhoneNumber = null;
		com.funambol.common.pim.contact.Phone[] funambolPhone = new com.funambol.common.pim.contact.Phone[countPhone]; 

		for(int i=0; i < countPhone; i++){ // The phone-list has to be separated in business and private call numbers in funambol
			strPhoneNumber = kolabContact.getPhone().get(i).getNumber().toString();
			funambolPhone[i] = new com.funambol.common.pim.contact.Phone(strPhoneNumber);
			
			if(kolabContact.getPhone().get(i).getType()!=null){
			switch (kolabContact.getPhone().get(i).getType()) {
			case HOME_1: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[0]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break;
			case HOME_2: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[1]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break; 
			case MOBILE: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[2]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break; 
			case HOMEFAX: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[3]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break;
			case CAR: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[4]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break;
			case RADIO: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType[5]);
				funambolPersonalDetail.getPhones().add(funambolPhone[i]);
				break;
			case BUSINESS_1: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType_[0]);
				funambolBusinessDetail.getPhones().add(funambolPhone[i]);
				break;
			case BUSINESS_2: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType_[1]);
				funambolBusinessDetail.getPhones().add(funambolPhone[i]);
				break; 
			case PAGER: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType_[2]);
				funambolBusinessDetail.getPhones().add(funambolPhone[i]);
				break; 
			case BUSINESSFAX: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType_[3]);
				funambolBusinessDetail.getPhones().add(funambolPhone[i]);
				break;
			case COMPANY: 
				funambolPhone[i].setPropertyType(strArFunambolPhoneType_[4]);
				funambolBusinessDetail.getPhones().add(funambolPhone[i]);
				break;
			}
			}
		}
		}
		
		//Address-lists
	    int countAddresses = kolabContact.getAddress().size();
	    if(countAddresses != 0) {
		com.funambol.common.pim.contact.Address[] funambolAddress = new com.funambol.common.pim.contact.Address[countAddresses];
	
			for(int i=0; i < countAddresses; i++){
				
				funambolAddress[i] = new com.funambol.common.pim.contact.Address();
				funambolAddress[i].getCity().setPropertyValue(kolabContact.getAddress().get(i).getLocality());
				funambolAddress[i].getPostalCode().setPropertyValue(kolabContact.getAddress().get(i).getPostalCode());
				funambolAddress[i].getCountry().setPropertyValue(kolabContact.getAddress().get(i).getCountry());	
				funambolAddress[i].getStreet().setPropertyValue(kolabContact.getAddress().get(i).getStreet());	
				funambolAddress[i].getState().setPropertyValue(kolabContact.getAddress().get(i).getRegion());
				
				switch(kolabContact.getAddress().get(i).getType()) {
				case HOME: funambolPersonalDetail.setAddress(funambolAddress[i]);
					break;
				case BUSINESS: funambolBusinessDetail.setAddress(funambolAddress[i]);	
					break;
				case OTHER: funambolPersonalDetail.setAddress(funambolAddress[i]);
					break;	
				}
			}
	    }
			
		/// Birthday
		if(kolabContact.getBirthday() != null){ 
		SimpleDateFormat fmt = new SimpleDateFormat();
		fmt.applyPattern("yyyy-MM-dd");
		Date dtTest_ = kolabContact.getBirthday();
		String strBirthday = fmt.format(dtTest_);
		funambolPersonalDetail.setBirthday(strBirthday);
		}

		///Anniversary
		if(kolabContact.getAnniversary() != null){
		SimpleDateFormat fmt = new SimpleDateFormat();
		fmt.applyPattern("yyyy-MM-dd");
		Date dtTest_ = kolabContact.getAnniversary();
		String strAnniversary = fmt.format(dtTest_);
		funambolPersonalDetail.setAnniversary(strAnniversary);
		}
			
		//Children
		if(kolabContact.getChildren() != null){
		funambolPersonalDetail.setChildren(kolabContact.getChildren());
		}

		//Kategorien
		if(kolabContact.getCategories() != null){
		funambolContact.getCategories().setPropertyValue(kolabContact.getCategories());
		}
		
		//Gender
		if(kolabContact.getGender() != null){
		Gender gender = kolabContact.getGender();
		if (gender == Gender.MALE)
			funambolPersonalDetail.setGender("male");
		else if (gender == Gender.FEMALE)
			funambolPersonalDetail.setGender("female");
		}
		
		//Website
		if(kolabContact.getWebPage() != null){
		String strWebsite = kolabContact.getWebPage();
		com.funambol.common.pim.contact.WebPage oFunambolWebPage = new com.funambol.common.pim.contact.WebPage(strWebsite);
		oFunambolWebPage.setPropertyType("WebPage");
		funambolPersonalDetail.addWebPage(oFunambolWebPage);
		}
		
		//Sprache
		if(kolabContact.getLanguage() != null){
		funambolContact.setLanguages(kolabContact.getLanguage());
		}
		
		//Free-Busy-Url
		if(kolabContact.getFreeBusyUrl() != null){
		funambolContact.setFreeBusy(kolabContact.getFreeBusyUrl());
		}
		
		//Department
		if(kolabContact.getDepartment() != null){
		funambolBusinessDetail.getDepartment().setPropertyValue(kolabContact.getDepartment());
		}
		
		//kolab profession == funambol role
		if(kolabContact.getProfession() != null){
		funambolBusinessDetail.getRole().setPropertyValue(kolabContact.getProfession());
		}
		
		//Company = Organisation
		if(kolabContact.getOrganization() != null){
		funambolBusinessDetail.getCompany().setPropertyValue(kolabContact.getOrganization());
		}
		
		//Office
		if(kolabContact.getOfficeLocation() != null){
		funambolBusinessDetail.setOfficeLocation(kolabContact.getOfficeLocation());
		}
		
		//Job Title
		if(kolabContact.getJobTitle() != null){
		com.funambol.common.pim.contact.Title oFunambolTitle = new com.funambol.common.pim.contact.Title();
		oFunambolTitle.setPropertyType("JobTitle");
		oFunambolTitle.setPropertyValue(kolabContact.getJobTitle());
		funambolBusinessDetail.addTitle(oFunambolTitle);
		}

		//Spouse name
		if(kolabContact.getSpouseName() != null){ 
		funambolPersonalDetail.setSpouse(kolabContact.getSpouseName());
		}

		//Assistant
		if(kolabContact.getAssistant() != null){
		funambolBusinessDetail.setAssistant(kolabContact.getAssistant());
		}
		
		//Bemerkung
		if(kolabContact.getBody() != null){
		com.funambol.common.pim.contact.Note oFunambolNote = new com.funambol.common.pim.contact.Note();
		oFunambolNote.setPropertyType("Body");
		oFunambolNote.setPropertyValue(kolabContact.getBody());
		funambolContact.addNote(oFunambolNote);
		}

		//Sensitivity
		if(kolabContact.getSensitivity() != null){
		Short shShort = null;
		switch(kolabContact.getSensitivity()){

		case PUBLIC:  shShort = 0; //Public -> normal 
			break;
		case CONFIDENTIAL: shShort = 3;
			break;
		case PRIVATE: shShort = 2;
			break;
		}
		funambolContact.setSensitivity(shShort);
		}
		
		//Uid
		if(kolabContact.getUid() != null){
		funambolContact.setUid(kolabContact.getUid());
		}
		
		//Personal und business details
		funambolContact.setPersonalDetail(funambolPersonalDetail);
		funambolContact.setBusinessDetail(funambolBusinessDetail);
		
		return funambolContact;
	}
	
	
	//Convert "Funambol contact" to "Kolab contact"
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static Contact convertToContact(com.funambol.common.pim.contact.Contact funambolContact) throws ConversionException {
		org.evolvis.bsi.kolab.service.Contact kolabContact = new org.evolvis.bsi.kolab.service.Contact();

		if (!isEmpty(funambolContact.getName().getFirstName().getPropertyValueAsString()))
			kolabContact.setGivenName(funambolContact.getName().getFirstName().getPropertyValueAsString());	

		if (!isEmpty(funambolContact.getName().getLastName().getPropertyValueAsString()))
			kolabContact.setLastName(funambolContact.getName().getLastName().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getDisplayName().getPropertyValueAsString()))
			kolabContact.setFullName(funambolContact.getName().getDisplayName().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getNickname().getPropertyValueAsString()))
			kolabContact.setNickName(funambolContact.getName().getNickname().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getMiddleName().getPropertyValueAsString()))
			kolabContact.setMiddleNames(funambolContact.getName().getMiddleName().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getSalutation().getPropertyValueAsString()))
			kolabContact.setPrefix(funambolContact.getName().getSalutation().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getSuffix().getPropertyValueAsString()))
			kolabContact.setSuffix(funambolContact.getName().getSuffix().getPropertyValueAsString());

		if (!isEmpty(funambolContact.getName().getInitials().getPropertyValueAsString()))
			kolabContact.setInitials(funambolContact.getName().getInitials().getPropertyValueAsString());

		//Manager name
		//Workaround begin: because of bug in funambol "(Contact) parser.vCard();"
		// ß => "=C3=9F" 
		// Ä => "=C3=84" 
		// ä => "=C3=A4" 
		// Ü => "=C3=9C" 
		// ü => "=C3=BC" 
		// Ö => "=C3=96" 
		// ö => "=C3=B6"
		String strManager = null;
		strManager = funambolContact.getBusinessDetail().getManager();
		if(strManager != null){
		for(int i = 0; i < strUml.length; i++){ 
		if(strManager.indexOf(strUml[i]) != -1){
		strManager = strManager.replaceAll(strUml[i], strErsUml[i]);
		}
		}
		kolabContact.setManagerName(strManager);
		}
		//Workaround end
	
		//////////////////// Emails /////////////////////
		//Private emails
		String strEmailAddress = null;
		int countEmailPrivate = funambolContact.getPersonalDetail().getEmails().size();
		java.util.List<com.funambol.common.pim.contact.Email> emailList;
		emailList = funambolContact.getPersonalDetail().getEmails();
		
		if(countEmailPrivate != 0) {
		for(int i=0; i < countEmailPrivate; i++){ // Kolab does not decide between business and private emails.
			
			strEmailAddress = emailList.get(i).getPropertyValueAsString();
			if ("IMAddress".equals(emailList.get(i).getPropertyType())) {
				kolabContact.setImAddress(strEmailAddress);
			} else {
				Email kolabEmail = new org.evolvis.bsi.kolab.service.Email();
				kolabEmail.setDisplayName(funambolContact.getName().getDisplayName().getPropertyValueAsString());
				kolabEmail.setSmtpAddress(strEmailAddress);
				kolabContact.getEmail().add(kolabEmail);
			}

		}		
		}

		//Business emails
		String strEmailAddress_ = null;
		int countEmailBusiness = funambolContact.getBusinessDetail().getEmails().size();
		java.util.List<com.funambol.common.pim.contact.Email> emailList_;
		emailList_ = funambolContact.getBusinessDetail().getEmails();
		
		if(countEmailBusiness != 0) {
		org.evolvis.bsi.kolab.service.Email kolabEmail_[] = new org.evolvis.bsi.kolab.service.Email[countEmailBusiness]; 
		for(int i=0; i < countEmailBusiness; i++){ // Kolab does not decide between business und privat emails.
			strEmailAddress_ = emailList_.get(i).getPropertyValueAsString();
			kolabEmail_[i] = new org.evolvis.bsi.kolab.service.Email();
			kolabEmail_[i].setDisplayName(funambolContact.getName().getDisplayName().getPropertyValueAsString());
			kolabEmail_[i].setSmtpAddress(strEmailAddress_);
			kolabContact.getEmail().add(kolabEmail_[i]);
		}	
		}
		
		//////////////////// Phone ////////////////////
		//Private phone
		int countPhonePrivate = funambolContact.getPersonalDetail().getPhones().size();
		if(countPhonePrivate != 0) {

		org.evolvis.bsi.kolab.service.Phone oKolabPhone[] = new org.evolvis.bsi.kolab.service.Phone[countPhonePrivate];
		String strPhoneType = null;
		
		java.util.List<com.funambol.common.pim.contact.Phone> phoneList;
		phoneList = funambolContact.getPersonalDetail().getPhones();
		
		for(int i=0; i < countPhonePrivate; i++){
			
			oKolabPhone[i] = new org.evolvis.bsi.kolab.service.Phone();
			oKolabPhone[i].setNumber(phoneList.get(i).getPropertyValueAsString());
			strPhoneType = phoneList.get(i).getPhoneType();
			strPhoneType = strPhoneType.trim();

			if(strPhoneType != null){
			if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[0])){
				oKolabPhone[i].setType(PhoneType.HOME_1);
			}else{
				if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[1])){
					oKolabPhone[i].setType(PhoneType.HOME_2);
				}else{
					if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[2])){
						oKolabPhone[i].setType(PhoneType.MOBILE);
					}else{
						if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[3])){
							oKolabPhone[i].setType(PhoneType.HOMEFAX);
						}else{
							if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[4])){
								oKolabPhone[i].setType(PhoneType.CAR);
							}else{
								if(strPhoneType.equalsIgnoreCase(strArFunambolPhoneType[5])){
									oKolabPhone[i].setType(PhoneType.RADIO);
								}else {
									oKolabPhone[i].setType(PhoneType.OTHER);
								}
							}
						}
				    }
			    }
			}	
			if (oKolabPhone[i].getNumber().trim().length() > 0)
				kolabContact.getPhone().add(oKolabPhone[i]);
		}
		}
		}
		
		//Business phone
		int countPhoneBusiness = funambolContact.getBusinessDetail().getPhones().size();
		if(countPhoneBusiness != 0) {

		org.evolvis.bsi.kolab.service.Phone oKolabPhone_[] = new org.evolvis.bsi.kolab.service.Phone[countPhoneBusiness];
		String strPhoneType_ = null;
		
		java.util.List<com.funambol.common.pim.contact.Phone> phoneList_;
		phoneList_ = funambolContact.getBusinessDetail().getPhones();
		
		for(int i=0; i < countPhoneBusiness; i++){

			oKolabPhone_[i] = new org.evolvis.bsi.kolab.service.Phone();
			oKolabPhone_[i].setNumber(phoneList_.get(i).getPropertyValueAsString());
			strPhoneType_ = phoneList_.get(i).getPhoneType();
			strPhoneType_ = strPhoneType_.trim();
	
			if(strPhoneType_ != null){
			if(strPhoneType_.equalsIgnoreCase(strArFunambolPhoneType_[0])){
				oKolabPhone_[i].setType(PhoneType.BUSINESS_1);
			}else{
				if(strPhoneType_.equalsIgnoreCase(strArFunambolPhoneType_[1])){
					oKolabPhone_[i].setType(PhoneType.BUSINESS_2);
				}else{
					if(strPhoneType_.equalsIgnoreCase(strArFunambolPhoneType_[2])){
						oKolabPhone_[i].setType(PhoneType.PAGER);
					}else{
						if(strPhoneType_.equalsIgnoreCase(strArFunambolPhoneType_[3])){
							oKolabPhone_[i].setType(PhoneType.BUSINESSFAX);
						}else{
							if(strPhoneType_.equalsIgnoreCase(strArFunambolPhoneType_[4])){
								oKolabPhone_[i].setType(PhoneType.COMPANY);					
							}else{
								oKolabPhone_[i].setType(PhoneType.OTHER);
							}
						}
				    }
			    }
			}
			if (oKolabPhone_[i].getNumber().trim().length() > 0)
				kolabContact.getPhone().add(oKolabPhone_[i]);
		}
		}
		}
		
		//////// Address-lists ////////
		//Private address
		if(funambolContact.getPersonalDetail().getAddress() != null){
		org.evolvis.bsi.kolab.service.Address kolabAddress = new org.evolvis.bsi.kolab.service.Address();
		//you can set more address in kolab than in funambol
		kolabAddress.setLocality(funambolContact.getPersonalDetail().getAddress().getCity().getPropertyValueAsString());
		kolabAddress.setPostalCode((String) funambolContact.getPersonalDetail().getAddress().getPostalCode().getPropertyValueAsString());
		kolabAddress.setCountry((String) funambolContact.getPersonalDetail().getAddress().getCountry().getPropertyValueAsString());
		kolabAddress.setStreet((String) funambolContact.getPersonalDetail().getAddress().getStreet().getPropertyValueAsString());
		kolabAddress.setRegion((String) funambolContact.getPersonalDetail().getAddress().getState().getPropertyValueAsString());
		kolabAddress.setType(AddressType.HOME);
		if (!isEmpty(kolabAddress))
			kolabContact.getAddress().add(kolabAddress); 
		}
		
		//Business address
		if(funambolContact.getBusinessDetail().getAddress() != null){
		org.evolvis.bsi.kolab.service.Address kolabAddress_ = new org.evolvis.bsi.kolab.service.Address();
		//you can set more address in kolab than in funambol
		kolabAddress_.setLocality((String) funambolContact.getBusinessDetail().getAddress().getCity().getPropertyValueAsString());
		kolabAddress_.setPostalCode((String) funambolContact.getBusinessDetail().getAddress().getPostalCode().getPropertyValueAsString());
		kolabAddress_.setCountry((String) funambolContact.getBusinessDetail().getAddress().getCountry().getPropertyValueAsString());
		kolabAddress_.setStreet((String) funambolContact.getBusinessDetail().getAddress().getStreet().getPropertyValueAsString());
		kolabAddress_.setRegion((String) funambolContact.getBusinessDetail().getAddress().getState().getPropertyValueAsString());
		kolabAddress_.setType(AddressType.BUSINESS);
		if (!isEmpty(kolabAddress_))
			kolabContact.getAddress().add(kolabAddress_);
		}
		
		//Other Address
		if(funambolContact.getPersonalDetail().getOtherAddress() != null){
		org.evolvis.bsi.kolab.service.Address kolabAddress_o = new org.evolvis.bsi.kolab.service.Address();
		//you can set more address in kolab than in funambol
		kolabAddress_o.setLocality((String) funambolContact.getPersonalDetail().getOtherAddress().getCity().getPropertyValueAsString());
		kolabAddress_o.setPostalCode((String) funambolContact.getPersonalDetail().getOtherAddress().getPostalCode().getPropertyValueAsString());
		kolabAddress_o.setCountry((String) funambolContact.getPersonalDetail().getOtherAddress().getCountry().getPropertyValueAsString());
		kolabAddress_o.setStreet((String) funambolContact.getPersonalDetail().getOtherAddress().getStreet().getPropertyValueAsString());
		kolabAddress_o.setRegion((String) funambolContact.getPersonalDetail().getOtherAddress().getState().getPropertyValueAsString());
		kolabAddress_o.setType(AddressType.OTHER);
		if (!isEmpty(kolabAddress_o))
			kolabContact.getAddress().add(kolabAddress_o);
		}
		
		// for Bug [#12] Falsche Datumübernahme bei Kontakten (Geburtstag, Jahrestag)
		// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=12&group_id=7&atid=105
		TimeZone tz_ = TimeZone.getDefault();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));	
		
		//Birthday
		if(funambolContact.getPersonalDetail().getBirthday() != null){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setLenient(true);
		String strDate = funambolContact.getPersonalDetail().getBirthday();
		
		Date temp = null;
		
		try {
			temp = sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		kolabContact.setBirthday(temp);
		}

		//Anniversary
		if(funambolContact.getPersonalDetail().getAnniversary() != null) {
		SimpleDateFormat sdf_ = new SimpleDateFormat("yyyy-MM-dd");
		sdf_.setTimeZone(TimeZone.getTimeZone("UTC"));
		sdf_.setLenient(true);
		String strDate_ = funambolContact.getPersonalDetail().getAnniversary();
		
		//Workaround begin: because of bug in funambol "(Contact) parser.vCard();"
		if(funambolContact.getPersonalDetail().getAnniversary().length() == 8){
		String strMonth = strDate_.substring(4,6);
		String strDay = strDate_.substring(6,8);
		String strYear = strDate_.substring(0,4);
		strDate_ = strYear + "-" + strMonth + "-" + strDay;
		}		
		//Workaround end
		
		Date temp = null;
		
		try{
			temp = sdf_.parse(strDate_);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		kolabContact.setAnniversary(temp);
		}
		
		TimeZone.setDefault(tz_);
		
		//Children
		if(funambolContact.getPersonalDetail().getChildren() != null){
		kolabContact.setChildren(funambolContact.getPersonalDetail().getChildren());
		}
		
		//Kategorien
		String strCategories = funambolContact.getCategories().getPropertyValueAsString();
		if(strCategories != null){
		kolabContact.setCategories(strCategories);
		}
		
		//Gender
		if(funambolContact.getPersonalDetail().getGender() != null){
		String gender = funambolContact.getPersonalDetail().getGender();
		if ("male".equals(gender))
			kolabContact.setGender(Gender.MALE);
		else if ("female".equals(gender))
			kolabContact.setGender(Gender.FEMALE);
		}
		
		//Website
		java.util.List<com.funambol.common.pim.contact.WebPage> webPageList;
		webPageList = funambolContact.getPersonalDetail().getWebPages();
		
		if (webPageList != null && webPageList.size() > 0 && webPageList.get(0).getPropertyValueAsString().length() > 0)
			kolabContact.setWebPage(webPageList.get(0).getPropertyValueAsString());
		//.get(0) because in the list can be just one website
		
		//Language
		if(funambolContact.getLanguages() != null){
		kolabContact.setLanguage(funambolContact.getLanguages());
		}
		
		//Free-Busy-Url
		if(funambolContact.getFreeBusy() != null){
		kolabContact.setFreeBusyUrl(funambolContact.getFreeBusy());
		}
		
		//Department
		String strDepartment = funambolContact.getBusinessDetail().getDepartment().getPropertyValueAsString();
		if(!isEmpty(strDepartment))
			kolabContact.setDepartment(strDepartment);
		
		//Office
		String strOfficeLocation = funambolContact.getBusinessDetail().getOfficeLocation();
		if(strOfficeLocation != null){
		kolabContact.setOfficeLocation(strOfficeLocation);
		}
		
		//Kolab profession == funambol role
		String strRole = funambolContact.getBusinessDetail().getRole().getPropertyValueAsString();
		if(!isEmpty(strRole))
			kolabContact.setProfession(strRole);
		
		//Company = Organisation
		String strCompany = funambolContact.getBusinessDetail().getCompany().getPropertyValueAsString();
		if(strCompany != null){
		kolabContact.setOrganization(strCompany);
		}
		
		//Title
		java.util.List<com.funambol.common.pim.contact.Title> titleList;
		titleList = funambolContact.getBusinessDetail().getTitles();
		
		if (titleList != null && titleList.size() > 0 && titleList.get(0).getPropertyValueAsString().length() > 0)
			kolabContact.setJobTitle(titleList.get(0).getPropertyValueAsString());
		//.get(0) because there can be just one title in Kolab

		//Spouse name
		kolabContact.setSpouseName(funambolContact.getPersonalDetail().getSpouse());

		//Assistant
		kolabContact.setAssistant(funambolContact.getBusinessDetail().getAssistant());
		
		//Bemerkung
		java.util.List<com.funambol.common.pim.contact.Note> getNotes_;
		getNotes_ = funambolContact.getNotes();
		
		if (getNotes_ != null && getNotes_.size() > 0 && getNotes_.get(0).getPropertyValueAsString().length() > 0)
			kolabContact.setBody(getNotes_.get(0).getPropertyValueAsString());
		//.get(0) because there can only be one Note
		
		//Sensitivity
		if(funambolContact.getSensitivity() != null){

			switch(funambolContact.getSensitivity()){
			case 0:  kolabContact.setSensitivity(Sensitivity.PUBLIC);
				break;
			case 3: kolabContact.setSensitivity(Sensitivity.CONFIDENTIAL);
				break;
			case 2: kolabContact.setSensitivity(Sensitivity.PRIVATE);
				break;
			case 1: kolabContact.setSensitivity(Sensitivity.PUBLIC); // Sensitivity "Personal" not available 
			}	
		}	
		
		if (kolabContact.getSensitivity() == null) // set default value
			kolabContact.setSensitivity(Sensitivity.PUBLIC);
	
		//Uid
		if(funambolContact.getUid() != null){
		kolabContact.setUid(funambolContact.getUid());
		}
		
		// set picture reference in xml
		final Photo photo = funambolContact.getPersonalDetail().getPhotoObject();
		if (photo != null && photo.getImage() != null)
		{
			kolabContact.setPictureAttachmentId("photo.attachment");
		}
		return kolabContact;
	}
	
	private static boolean isEmpty(Address address) {
		return 
			isEmpty(address.getStreet()) &&
			isEmpty(address.getLocality()) &&
			isEmpty(address.getRegion()) &&
			isEmpty(address.getPostalCode()) &&
			isEmpty(address.getCountry());
	}	

	private static boolean isEmpty(String string) {
		return string == null || string.trim().length() == 0;
	}
	
	//Convert funambol note to kolab note
	public static Note convertToNote(com.funambol.common.pim.note.Note fnote) throws ConversionException {
    	org.evolvis.bsi.kolab.service.Note kolabNote = new org.evolvis.bsi.kolab.service.Note();
    	
    	if(fnote.getUid() != null && 
    			fnote.getUid().getPropertyValue() != null && 
    				fnote.getUid().getPropertyValueAsString().trim() != ""){
    	kolabNote.setUid(fnote.getUid().getPropertyValueAsString());
    	}

    		kolabNote.setBackgroundColor("#000000");
    		kolabNote.setForegroundColor("#ffff00");
    	
    	kolabNote.setSensitivity(Sensitivity.PUBLIC); //Sensitivity is not available in funambol note
    	
    	if(fnote.getTextDescription() != null){
    		kolabNote.setBody(fnote.getTextDescription().getPropertyValueAsString());
		kolabNote.setSummary(fnote.getTextDescription().getPropertyValueAsString());
    	}
    	
		return kolabNote;
	} 

	//Convert kolab note to funambol note
	public static final com.funambol.common.pim.note.Note convertNote(Note kolabNote) throws ConversionException {
    	com.funambol.common.pim.note.Note fnote = new com.funambol.common.pim.note.Note();
		
    	if(kolabNote.getUid() != null 
    			&& kolabNote.getUid() != ""
    				&& kolabNote.getUid().trim() != ""){
    	fnote.getUid().setPropertyValue(kolabNote.getUid());
    	}
    	
    	if(kolabNote.getBackgroundColor() != null) {
    	fnote.getColor().setPropertyValue(kolabNote.getBackgroundColor());
    	}
    	
    	//Sensitivity is not available in funambol note
    	
    	if(kolabNote.getSummary() != null){
    	fnote.getTextDescription().setPropertyValue(kolabNote.getBody());
    	fnote.getSubject().setPropertyValue(kolabNote.getSummary());
    	}
    	
    	return fnote;
	}
	
	//Convert funambol task to kolab task
	public static Task convertToTask(com.funambol.common.pim.calendar.Task ftask, TimeZone deviceZone, String strDeviceZone, String contentType) throws ConversionException {
		org.evolvis.bsi.kolab.service.Task kolabTask = new org.evolvis.bsi.kolab.service.Task();
		
		kolabTask.setDueDateAllDay(false);
		
		TimeZone tz_ = TimeZone.getDefault();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		if(ftask.getUid() != null){
		kolabTask.setUid(ftask.getUid().getPropertyValueAsString());
		}

		TimeZone tz = TimeZone.getTimeZone("UTC");
    	// for Bug [#37] Aufgabe im Horde als "erledigt" markieren: Änderung wird nicht auf dem Endgerät übernommen
    	// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=37&group_id=7&atid=105
		//Completed task or not
		String taskCompleted = ftask.getComplete().getPropertyValueAsString();
		if(taskCompleted != null && taskCompleted != "")
		{
			if(taskCompleted.equalsIgnoreCase("1")) { //Completed
				kolabTask.setCompleted(100);
			} else if (ftask.getPercentComplete().getPropertyValue() != null 
					&& ftask.getPercentComplete().getPropertyValueAsString().trim() != ""){ //Started but not already done
				kolabTask.setCompleted(Integer.parseInt(ftask.getPercentComplete().getPropertyValueAsString()));
			} else {
				kolabTask.setCompleted(0); //Not started
			}
		} else {
			kolabTask.setCompleted(0); //Not started
		}		
		
		// Alarm bzw. Reminder
		int intMinutes = 0;

		Reminder reminder = null;
		
		
		//Begin to set startdate and the startdate for the reminder
		String strDate1 = "";
		
		if(ftask.getDtStart().getPropertyValue() != null && 
				ftask.getDtStart() != null &&
					ftask.getDtStart().getPropertyValueAsString().trim() != "" &&
						ftask.getDtStart().getPropertyValueAsString() != "" &&
							ftask.getDtStart().getPropertyValue() != "" && 
								ftask.getDtStart().getPropertyValueAsString() != null){
		
		strDate1 = ftask.getDtStart().getPropertyValueAsString();	
		
		} else if(ftask.getDueDate().getPropertyValue() != null && 
				ftask.getDueDate() != null &&
					ftask.getDueDate().getPropertyValueAsString().trim() != "" &&
						ftask.getDueDate().getPropertyValueAsString() != "" &&
							ftask.getDueDate().getPropertyValue() != "" &&
								ftask.getDueDate().getPropertyValue() != null) {
			
		strDate1 = ftask.getDueDate().getPropertyValueAsString();	
		
		} else if(ftask.getDtEnd().getPropertyValue() != null && 
				ftask.getDtEnd() != null &&
					ftask.getDtEnd().getPropertyValueAsString().trim() != "" &&
						ftask.getDtEnd().getPropertyValueAsString() != "" &&
							ftask.getDtEnd().getPropertyValue() != "" &&
								ftask.getDtEnd().getPropertyValue() != null) {
			
		strDate1 = ftask.getDtEnd().getPropertyValueAsString();
		
		} else if(ftask.getLastModified().getPropertyValue() != null && 
					ftask.getLastModified() != null &&
						ftask.getLastModified().getPropertyValueAsString().trim() != "" &&
							ftask.getLastModified().getPropertyValueAsString() != "" &&
								ftask.getLastModified().getPropertyValue() != "" &&
									ftask.getLastModified().getPropertyValue() != null) {	
				
		strDate1 = ftask.getLastModified().getPropertyValueAsString();
			
		} else if(ftask.getCreated().getPropertyValue() != null && 
					ftask.getCreated() != null &&
						ftask.getCreated().getPropertyValueAsString().trim() != "" &&
							ftask.getCreated().getPropertyValueAsString() != "" &&
								ftask.getCreated().getPropertyValue() != "" &&
									ftask.getCreated().getPropertyValue() != null){
		
		strDate1 = ftask.getCreated().getPropertyValueAsString();
		} else if(ftask.getReminder() != null ) {
			
		strDate1 = ftask.getReminder().getTime();	
		}
		
		//Reminder begin
		if(ftask.getReminder() != null 
				&& ftask.getReminder().getTime() != null
					&& ftask.getReminder().getTime().trim() != "") {
		reminder = ftask.getReminder();

		
		Date dtDate1 = null;
		Date dtDate2 = null;
		

		dtDate1 = Helper.utc2long(strDate1);
		
		if(dtDate1 != null 
				&& reminder.getTime() != null
					&& reminder.getTime().trim() != "") {
			
		dtDate2 = Helper.utc2long(reminder.getTime());
		
		if(reminder.getTime().toString().length() == 16) { //Nokia: 20081212T000000Z == 16 (reminder.getTime())
		/* On a mobile device with the nokia funambol plugin you will get a format with a lenght of 16. The funambol plugin
		 * for windows mobile will send you a non UTC date string with lenght 15. */
		int intMinutes_ = Helper.getDateFromDeviceZone(Helper.getServerTimeZoneDisplayName());
			intMinutes = Helper.getDateDifferencesInMinutes(dtDate1, dtDate2);
			
		if(intMinutes < 0){
		intMinutes += intMinutes_; 
		} else {
		intMinutes -= intMinutes_;	
		}
	
		} else { //WinMo: 20081212T000000 == 15 (reminder.getTime()		
			intMinutes = Helper.getDateDifferencesInMinutes(dtDate1, dtDate2);	
		}
		
		}
		
		}	
		//Reminder end

		// for Bug [#46] Nokia S60: Änderung an einem Jahrestag; Alarm/ Erinnerung wird nicht richtig gesynct
		// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=46&group_id=7&atid=105
		if(intMinutes < 0) { intMinutes *= -1;}
		if(Helper.getBoolDeleteMinutes() == true || ftask.getDueDate() == null) { intMinutes = 0; } 
		Helper.setBoolDeleteMinutes(false);
		/* Maybe someone set an alarm but no due date, so the minutes of the alarm before the appointment can
		 * not calculated.*/
		
		if (reminder != null){  // && reminder.getMinutes() != 0) You will never get minutes from the s60
			kolabTask.setAlarm(intMinutes); 
		}

		// TODO windows mobile sets all day *every* time
		/* You can manipulate the parser that the parser will not set the all day event every time. 
		 * If the start and end-date from the device is 2009-12-01 for example you can set the 
		 * isAllDay attribute = true and otherwise = false */
		
		if(strDate1 != null && strDate1.trim() != "") {
			kolabTask.setAllDay(ftask.isAllDay());
		} else {
			kolabTask.setAllDay(null);
		}
	
		Property organizer = ftask.getOrganizer();
		if (organizer != null)
		{
			kolabTask.setOrganizerDisplayName(organizer.getPropertyValueAsString());
		}

		// for Bug [#47] s60: Aufgabe wird nicht im Horde angezeigt
    	// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=47&group_id=7&atid=105
		// description	
		if(ftask.getDescription() != null) {
		if(ftask.getDescription().getPropertyValueAsString() != "" 
			&& ftask.getDescription().getPropertyValueAsString() != null 
				&& ftask.getDescription().getPropertyValue() != null) {
		kolabTask.setBody(ftask.getDescription().getPropertyValueAsString()); // Does not match with anything else in funambol
		}
		}
		
		if (ftask.getCategories() != null
				&& ftask.getCategories().getPropertyValue() != null
					&& ftask.getCategories().getPropertyValueAsString().trim() != "") {
			kolabTask.setCategories(ftask.getCategories().getPropertyValueAsString());
		}
		
		//startDate => begin
		Date dtDate = null;
		Date dtDate_ = null;
		String strDate = null;
		if(strDate1 != null && strDate1.trim() != ""){

		try {
		dtDate_ = Helper.eliminateTime(strDate1, tz); //20091211T000000Z
		/* kolab Horde does not save the time, so we should not submit time */
		
		strDate = Helper.long2utc(dtDate_).substring(0, 8);
		dtDate = Helper.utc2long(strDate);

		kolabTask.setStartDate(dtDate);
		} catch (Exception e) {
			e.printStackTrace();
			kolabTask.setAllDay(null);
		} 			
		} else {
			kolabTask.setStartDate(null);
		}
		
		/* kolabTask.setDueDate() == ftask.getDtEnd() on s60 
		 * There is a ftask.getDueDate() but it is always null on the known mobile devices*/
		dtDate = null;
		dtDate_ = null;
		strDate = null;
		if(ftask.getDtEnd() != null 
				&& ftask.getDtEnd().getPropertyValue() != null 
					&& ftask.getDtEnd().getPropertyValueAsString().trim() != ""){

		try {
			
		dtDate_ = Helper.eliminateTime(ftask.getDtEnd().getPropertyValueAsString(), tz); //20091211T000000Z
		/* kolab Horde does not save the time, so we should not submit time */
		
		strDate = Helper.long2utc(dtDate_).substring(0, 8);
		dtDate = Helper.utc2long(strDate);	

		kolabTask.setDueDateAllDay(true); // do not serialize time later
		kolabTask.setDueDate(dtDate); //There is not Enddate in kolab. EndDate == DueDate
		} catch (Exception e) {
			e.printStackTrace();
			kolabTask.setAllDay(null);
		}
		}
		
		/* Just for the reason a not known mobile device or a newer funambol-plugin does it the right way 
		 * kolabTask.setDueDate() will be overwritten with the right data */
		dtDate = null;
		if(ftask.getDueDate() != null 
				&& ftask.getDueDate().getPropertyValue() != null 
					&& ftask.getDueDate().getPropertyValueAsString().trim() != ""){

		try {
			
		dtDate_ = Helper.eliminateTime(ftask.getDueDate().getPropertyValueAsString(), tz); //20091211T000000Z
		/* kolab Horde does not save the time, so we should not submit time */
		
		strDate = Helper.long2utc(dtDate_).substring(0, 8);
		dtDate = Helper.utc2long(strDate);

		kolabTask.setDueDate(dtDate); //There is not Enddate in kolab. EndDate == DueDate
		} catch (Exception e) {
			e.printStackTrace();
			kolabTask.setAllDay(null);
		}
		}
		
		if(ftask.getLastModified().getPropertyValue() != null && 
				ftask.getLastModified().getPropertyValueAsString().trim() != "") {
		kolabTask.setLastModificationDate(Helper.utc2long(ftask.getLastModified().getPropertyValueAsString()));
		}
		
		if(ftask.getCreated().getPropertyValue() != null &&
				ftask.getCreated().getPropertyValueAsString().trim() != ""){
		kolabTask.setCreationDate(Helper.utc2long(ftask.getCreated().getPropertyValueAsString()));	
		}

		if(ftask.getLocation() != null 
				&& ftask.getLocation().getPropertyValue() != null
					&& ftask.getLocation().getPropertyValueAsString().trim() != ""){
		kolabTask.setLocation(ftask.getLocation().getPropertyValueAsString());
		}
		
		String priorityy = ftask.getPriority().getPropertyValueAsString();
		if (priorityy != null && ftask.getPriority().getPropertyValueAsString().trim() != "")
		{
			switch(Integer.parseInt(priorityy))
			{
				// niedrig
				case 9: //Priority 4 and 5 in kolab
					kolabTask.setPriority(5); 
					break;
				// standard
				case 5: //Priority 3 in kolab
					kolabTask.setPriority(3);
					break;
				// niedrig
				case 1: //Priority 1 and 2 in kolab
					kolabTask.setPriority(1);
					break;
				default:
					kolabTask.setPriority(5);
			}
		}
		
		if(ftask.getSensitivity() != null 
				&& ftask.getSensitivity().getPropertyValue() != null 
					&& ftask.getSensitivity().getPropertyValueAsString().trim() != ""){

			switch(Integer.parseInt(ftask.getSensitivity().getPropertyValueAsString())){
			case 0:  kolabTask.setSensitivity(Sensitivity.PUBLIC);
				break;
			case 3: kolabTask.setSensitivity(Sensitivity.CONFIDENTIAL);
				break;
			case 2: kolabTask.setSensitivity(Sensitivity.PRIVATE);
				break;
			case 1: kolabTask.setSensitivity(Sensitivity.PRIVATE); /* Sensitivity "personal" not available,
			but if something is personal it is more private than public */
			}	
		}

		if (ftask.getSummary() != null 
				&& ftask.getSummary().getPropertyValue() != null
					&& ftask.getSummary().getPropertyValueAsString().trim() != "") {
			kolabTask.setSummary(ftask.getSummary().getPropertyValueAsString());
		}
		
		kolabTask.setRecurrence(RecurrenceConverter.convertRecurrencePattern(ftask.getRecurrencePattern(), deviceZone));
		
		//Attendee Begin
		int j = ftask.getAttendees().size();
		org.evolvis.bsi.kolab.service.Attendee[] kolabAttendee = new org.evolvis.bsi.kolab.service.Attendee[j];
		
		for(int i = 0; i<j; i++) {
			
			kolabAttendee[i] = new org.evolvis.bsi.kolab.service.Attendee();
			kolabAttendee[i].setDisplayName(ftask.getAttendees().get(i).getName());
			kolabAttendee[i].setSmtpAddress(ftask.getAttendees().get(i).getEmail());
			
			switch(ftask.getAttendees().get(i).getExpected()){
				case 1: kolabAttendee[i].setRole(Role.OPTIONAL);
					break;
				case 2: kolabAttendee[i].setRole(Role.REQUIRED);
					break;
				case 5: kolabAttendee[i].setRole(Role.RESOURCE);
					break;
				default: kolabAttendee[i].setRole(Role.RESOURCE);
					break;
			}
			
			switch(ftask.getAttendees().get(i).getStatus()){
				case 5: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.ACCEPTED);  
					break;
				case 0: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.DECLINED);
					break;
				case 8: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.NONE);
					break;
				case 4: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.TENTATIVE);
					default: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.NONE);;
			}
			
			kolabAttendee[i].setDisplayName(ftask.getAttendees().get(i).getName());
			kolabAttendee[i].setSmtpAddress(ftask.getAttendees().get(i).getEmail());

			kolabTask.getAttendee().add(kolabAttendee[i]);	
		}
		//Attendee End
		
		//Organizer
		if(ftask.getOrganizer() != null && 
				ftask.getOrganizer().getPropertyValue() != null && 
					ftask.getOrganizer().getPropertyValueAsString().trim() != ""){
		kolabTask.setOrganizerDisplayName(ftask.getOrganizer().getPropertyValueAsString());
		}
		
		//Location
		if(ftask.getLocation() != null && 
				ftask.getLocation().getPropertyValue() != null && 
					ftask.getLocation().getPropertyValueAsString().trim() != "") {
		kolabTask.setLocation(ftask.getLocation().getPropertyValueAsString());
		}
		
		TimeZone.setDefault(tz_);	
		
		return kolabTask;
	}
	
	//Convert kolab task to funambol task
	@SuppressWarnings({"deprecation"})
	public static final com.funambol.common.pim.calendar.Task convertTask(Task kolabTask) throws ConversionException {	
    	com.funambol.common.pim.calendar.Task ftask = new com.funambol.common.pim.calendar.Task();
    	
    	java.util.Calendar cal_0 = new GregorianCalendar();
    	cal_0.set(1970, java.util.Calendar.JANUARY, 1, 0, 0, 0 );  
    	
    	if(kolabTask.getStartDate() == null) {
    		kolabTask.setStartDate(cal_0.getTime());
    	}
    	if(kolabTask.getDueDate() == null) {
    		kolabTask.setDueDate(cal_0.getTime());
    	}
    	
    	/* Kolab Horde can have a DueDate with date and time, but not the devices Windows Mobile
    	 * and the Symbian. The time would get lost anyway. So we have to cut it off */ 
    	
    	java.util.Calendar cal_1 = new GregorianCalendar();
    	cal_1.set(1970, java.util.Calendar.JANUARY, 2, 0, 0, 0 );  
    	
    	if(kolabTask.getUid() != "" && kolabTask.getUid() != null) {
    	ftask.getUid().setPropertyValue(kolabTask.getUid());
    	} 	
    	
    	if(kolabTask.getStartDate() == null) { 
    		ftask.setAllDay(null);
    	} else {
    		ftask.setAllDay(kolabTask.isAllDay());
    	}
    	
    	Reminder freminder = new Reminder();
    	freminder.setActive(false);

     	String strDueDate_ = "";
     	Date dtDate = null;

    	
    	/* Important to know: 
    	 * You have to choose a valid combination of due date and start date in kolab. In kolab you have
    	 * the possiblility to choose a later start date than the due date is, but that's wrong. If you synchronisize
    	 * the task there will no task appear on windows mobile. */

    	if(kolabTask.getStartDate()!= null 
    			&& kolabTask.getStartDate().toString().trim() != "" 
    				&& kolabTask.getStartDate().after(cal_1.getTime())){
    		
    		dtDate = kolabTask.getStartDate();
    	
    	} else if (kolabTask.getLastModificationDate() != null 
    				&& kolabTask.getLastModificationDate().toString().trim() != ""){
    		/* Would be more userfriendly. The user set an alarm, but forgot to set a due date 
    		 * The date of the reminder would be in the year 1970 on windows mobile, because the kolab server
    		 * set that date. With these lines it's primarly set on the Last modification date or creation daten */	
    		dtDate = kolabTask.getLastModificationDate();	
    	
    	} else {
    		
    		dtDate = kolabTask.getCreationDate();	
    	
    	}
    	
    	String strDisplayName =  Helper.getServerTimeZoneDisplayName();
    	String strStartDate = Helper.long2localtime_(dtDate, strDisplayName);
    	String strStartDate_ =  strStartDate.substring(0, 8);
    	
    	//Reminder begin
    	if(kolabTask.getAlarm() != null) { 
    	
        	freminder.setActive(true);       	
        	
        dtDate = Helper.localtime2long(strStartDate, TimeZone.getTimeZone(strDisplayName));
       
        java.util.Calendar cal_4 = new GregorianCalendar();
        cal_4.set(2010, java.util.Calendar.JANUARY, 1, 0, 0, 0);  
        	
        Date dtDateForReminder_ = cal_4.getTime();
    		
    	String strDateForReminder_ = null;
    		  			
    	dtDateForReminder_.setYear(dtDate.getYear());
    	dtDateForReminder_.setMonth(dtDate.getMonth());
    	dtDateForReminder_.setDate(dtDate.getDate() - 1);
    	dtDateForReminder_.setHours(23);
    	dtDateForReminder_.setMinutes(60 - kolabTask.getAlarm());
    	dtDateForReminder_.setSeconds(0);
    	
    	strDateForReminder_ = Helper.long2utc(dtDateForReminder_);
        	
    	freminder.setMinutes(kolabTask.getAlarm());
    	freminder.setTime(strDateForReminder_);
    	ftask.setReminder(freminder);
    	}
    	//Reminder end
    	
    	if(kolabTask.getAlarm() != null) {
    	if(kolabTask.getAlarm() == 0 ) {
    		freminder.setActive(false);
    	}
    	}

    	//DueDate
    	if(kolabTask.getDueDate() != null
    			&& kolabTask.getDueDate().toString().trim() != "") {
    	
        strDueDate_ = Helper.long2localtime_(kolabTask.getDueDate(), strDisplayName).substring(0, 8);
    	}
    	
    	// Workaround begin /////////////////////////////////////////////////////////////////////////////////
    	//Bugfix [#87]
    	/* You can create a task at kolab horde whose due date takes place before
    	 * it's start date but that's not logical. If you synchroize the task it will not occure
    	 * on a mobile device (Windows Mobile, Symbian), because it is not possible to create such kind
    	 * of task on that devices. */
    	if(kolabTask.getDueDate() != null &&
    			kolabTask.getDueDate().toString().trim() != "" && 
    				kolabTask.getStartDate() != null &&
    					kolabTask.getStartDate().toString().trim() != "" &&   					
    						kolabTask.getDueDate().before(kolabTask.getStartDate())) { //Due date earlier than start date
    		
    		kolabTask.setDueDate(kolabTask.getStartDate());
    		
    		strDueDate_ = strStartDate_;
    		
    	} else if(kolabTask.getStartDate().before(cal_1.getTime()) &&
    				kolabTask.getDueDate().before(dtDate)){ //No Start date choosen: year 1970 ...
    		
    		strDueDate_ = strStartDate_;
    		
    		/* kolabTask.setDueDate(dtDate); We choose the creation date at least for the 
    		start date, because the s60 needs a date. The due date is the start date on the s60.
    		But if a user choose a due date that's before the creation date (start date) the mobile devices 
    		can not handle it.
    		So we choose the creation date for the due date as well. */
    	}	
    	// Workaround end  /////////////////////////////////////////////////////////////////////////////////
    	
    	if(kolabTask.getCategories() != null){
    	ftask.getCategories().setPropertyValue(kolabTask.getCategories());
    	}
    	
    	if(kolabTask.getLocation() != null){
    	ftask.getLocation().setPropertyValue(kolabTask.getLocation());
    	}

    	if(kolabTask.getSummary() != null){
    	ftask.getSummary().setPropertyValue(kolabTask.getSummary());
    	}
    	
    	if(kolabTask.getCreationDate() != null) {
    	String strCreationDate = Helper.long2utc(kolabTask.getCreationDate());	
    	ftask.getCreated().setPropertyValue(strCreationDate);
    	}
    	
    	if(kolabTask.getLastModificationDate() != null){
    	String strLastModifiedDate = Helper.long2utc(kolabTask.getLastModificationDate());
    	ftask.getLastModified().setPropertyValue(strLastModifiedDate);
    	}
		
    	/* If you set not start-date and due-date in kolab horde:
    	 * the date is available but 1970-01-01
    	 * 
    	<task version="1.0">
    	<uid>eb4a869a-2303-41a5-94b2-760cf21f3a94</uid>
    	<body/>
    	<categories/>
    	<creation-date>2009-12-08T12:44:09Z</creation-date>
    	<last-modification-date>2009-12-08T12:44:09Z</last-modification-date>
    	<sensitivity>public</sensitivity>
    	<product-id>Kolab WebService (tarent GmbH)</product-id>
    	<summary>Aufgabe</summary>
    	<start-date>1970-01-01</start-date>
    	<priority>3</priority>
    	<completed>0</completed>
    	<due-date>1970-01-01T00:00:00Z</due-date>
    	</task> */
    	
    	if(kolabTask.getDueDate() != null 
    			&& kolabTask.getDueDate().after(cal_1.getTime())){
    				ftask.getDueDate().setPropertyValue(strDueDate_);
    						
    				ftask.getDtEnd().setPropertyValue(strDueDate_);
        } else {
        	//ftask.getDueDate().setPropertyValue(null); //XXX: Remove this line?
        	/* There will be always a DueDate with the
        	date 01.01.1970 on the s60 mobile devicethe if no other date is choosen on kolab horde, 
        	because you can not create a task without due date on the s60 device */
        	if(kolabTask.getLastModificationDate() != null && 
        			kolabTask.getLastModificationDate().toString().trim() != ""){
        		
        	ftask.getDueDate().setPropertyValue(strDueDate_);
        		
        	} else {
        		
        	ftask.getDueDate().setPropertyValue(strDueDate_); 
        	ftask.getDtEnd().setPropertyValue(strDueDate_);
        	
        	}
        }
    		
    	// Start date begin //////////////////////////////////////////////////////////////////////
        if(kolabTask.getStartDate() != null 
        		&& kolabTask.getStartDate().after(cal_1.getTime())){

    			ftask.getDtStart().setPropertyValue(strStartDate_); 

        } else if(kolabTask.getLastModificationDate() != null) {
        		ftask.getDtStart().setPropertyValue(Helper.long2localtime_(kolabTask.getLastModificationDate(), strDisplayName).substring(0, 8)); 
        } else {
        		ftask.getDtStart().setPropertyValue(Helper.long2localtime_(kolabTask.getCreationDate(), strDisplayName).substring(0, 8)); 
        	/* This line has the same matter like ftask.getDueDate(null) on the line above */
        }
        // Start date end //////////////////////////////////////////////////////////////////////
      
        if(kolabTask.getBody() != null) {
        	ftask.getDescription().setPropertyValue(kolabTask.getBody());
        }
    	   
    	int priority = kolabTask.getPriority();

    	switch (priority)
    	{
    		case 1:
    			ftask.getPriority().setPropertyValue("1");
    			break;
    		case 2: 
    			ftask.getPriority().setPropertyValue("1");
    			break;
    		case 3:
    			ftask.getPriority().setPropertyValue("5");
    			break;
    		case 4:
    			ftask.getPriority().setPropertyValue("9");
    			break;
    		case 5:
    			ftask.getPriority().setPropertyValue("9");
    			break;
    		default:
    			 ftask.getPriority().setPropertyValue("5"); 
    			break;
    	}
    	
    	ftask.getPriority().setTag("Importance"); 
    	
    	boolean boolIsPrivate = false;
    	
    	if(kolabTask.getSensitivity() != null 
    			&& kolabTask.getSensitivity().toString().trim() != "") {
    		Sensitivity sens = kolabTask.getSensitivity();
    		if (sens.equals(Sensitivity.CONFIDENTIAL))
    		{
    			ftask.getSensitivity().setPropertyValue("3");
    		} 
    		else if (sens.equals(Sensitivity.PRIVATE))
    		{
    			boolIsPrivate = true;
    			ftask.getSensitivity().setPropertyValue("1");
    		}
    		else if (sens.equals(Sensitivity.PUBLIC))
    		{
    			ftask.getSensitivity().setPropertyValue("0");
    		} 
    		else
    		{
    			ftask.getSensitivity().setPropertyValue("0");
    		}
    	}
    	
    	// for Bug [#37] Aufgabe im Horde als "erledigt" markieren: Änderung wird nicht auf dem Endgerät übernommen
    	// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=37&group_id=7&atid=105
		//Completed Task or not
		int taskCompleted = kolabTask.getCompleted();
		if(taskCompleted == 100) { //Completed
			ftask.getComplete().setPropertyValue("1"); //it is 1 and not "true"!
		} else { //started but not completed yet
			ftask.getPercentComplete().setPropertyValue(taskCompleted);
			// ftask.getComplete().setPropertyValue("0"); Not needed
			// ftask.getStatus().setPropertyValue("NEEDS-ACTION"); Not needed  
			ftask.getDateCompleted().setPropertyValue(" "); /* Needs to be an empty string with a blank.
			Otherwise the VCALENDAR-Parser will not add COMPLETED to the VCALENDAR.
				BEGIN:VCALENDAR
			   	VERSION:1.0
				BEGIN:VTODO
				X-FUNAMBOL-ALLDAY:1
				SUMMARY:Aufgabe
				DESCRIPTION:
				COMPLETED: 
				
				This VCALENDAR is send from the funambol plugin.
				If the field for the date of completition is empty, it will reset 
				STATUS:COMPLETED of the task. If you send STATUS:NEEDS-ACTION or INCOMPLETED
				nothing will happen.
				
				DUE:19700101
				DTSTART:19700101
				CATEGORIES:
				PRIORITY:1
				CLASS:PUBLIC
				RRULE:
				AALARM:
				X-FUNAMBOL-AALARMOPTIONS:4
				X-FUNAMBOL-TEAMTASK:0
				END:VTODO
				END:VCALENDAR */
		}
		
    	ftask.setRecurrencePattern(RecurrenceConverter.convertRecurrence(kolabTask.getRecurrence(), kolabTask.getStartDate()));
	
    	//Attendee Begin
		int j = kolabTask.getAttendee().size();
		com.funambol.common.pim.calendar.Attendee[] funambolAttendee = new com.funambol.common.pim.calendar.Attendee[j];
		
		Short shShort = -1;
		for(int i = 0; i<j; i++) {
			
		funambolAttendee[i] = new com.funambol.common.pim.calendar.Attendee();
		funambolAttendee[i].setEmail(kolabTask.getAttendee().get(i).getSmtpAddress());
		funambolAttendee[i].setName(kolabTask.getAttendee().get(i).getDisplayName());
			
			if(kolabTask.getAttendee().get(i).getRole() != null){
				switch (kolabTask.getAttendee().get(i).getRole()){
					case OPTIONAL: shShort = 1;  // ?
						break;
					case REQUIRED: shShort = 2;
						break;
					case RESOURCE: shShort = -1; //RESSOURCE is not available in funambol, so it is unexpected
				/* -1 is unexpected in funambol. Funambol does not know the value resource. 
				 * The funambol value by default "UNKNOWN" will be set.
				 * */
						break;
					default: shShort = -1;
						break;
			}
			funambolAttendee[i].setExpected(shShort);
			}
			
			shShort = -1;
			if(kolabTask.getAttendee().get(i).getStatus()!= null){
				switch (kolabTask.getAttendee().get(i).getStatus()){
					case ACCEPTED: shShort = 5;
						break;
					case DECLINED: shShort = 0; 
						break;
					case NONE: shShort = -1; //UNKNOWN in funambol
						break;
					case TENTATIVE: shShort = 4;
						break;
					default: shShort = -1;
						break;
			}
			funambolAttendee[i].setStatus(shShort);
			}
			
			funambolAttendee[i].setName(kolabTask.getAttendee().get(i).getDisplayName());
			funambolAttendee[i].setEmail(kolabTask.getAttendee().get(i).getSmtpAddress());

			ftask.addAttendee(funambolAttendee[i]);		
		}
		//Attendee End
		
		//Organizer
		if(kolabTask.getOrganizerDisplayName() != null && 
				kolabTask.getOrganizerDisplayName() != "" && 
					kolabTask.getOrganizerDisplayName().trim() != ""){
		ftask.getOrganizer().setPropertyValue(kolabTask.getOrganizerDisplayName());
		}
		
		//Location
		if(kolabTask.getOrganizerDisplayName() != null && 
				kolabTask.getOrganizerDisplayName() != "" && 
					kolabTask.getLocation().trim() != ""){
		ftask.getLocation().setPropertyValue(kolabTask.getLocation());
		}
		
		//for Bug [#34] MDA: Aufgaben werden bei initialer Synchronisation nicht aufs Handy geladen.
		// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=34&group_id=7&atid=105
		if(boolIsPrivate == false) {
			ftask.getAccessClass().setPropertyValue("0"); // maybe 1 ?//Otherwise there would be a parse exception at VCalendarContentConverter.class line 202
		} else {
			ftask.getAccessClass().setPropertyValue("2"); //It's sensitivity = private
		}

		kolabTask.setDueDateAllDay(true); /* Currently there is always a datetime in the due date on a kolab server */
		if(kolabTask.isDueDateAllDay()) { //If (only a date in the due date and no datetime) {...} 
			ftask.setAllDay(true);
		}
		
		return ftask;
	}

	// Convert from Kolab Event to Funambol vCalendar
	@SuppressWarnings({"deprecation", "unused"})
	public static final Calendar convertEvent(Event kolabEvent) throws ConversionException {	
		Calendar cal = new Calendar();
		com.funambol.common.pim.calendar.Event funEvent = new com.funambol.common.pim.calendar.Event();
		
		/* If you get the start date and end date in kolabEvent that has no time (for examle: 
		 * 2009-12-08 00:00:00 UTC and 2009-12-08 00:00:00 UTC, that means that someone has created
		 * an allday appointment. But it is not UTC!. Kolab Horde saves only all appointments
		 * in UTC that has time present in the appointment (Those time is not 00:00:00. 
		 * If there is no time presents, its local time (Its local time also if you will find a UTC mark!
		 * An all day appointment saved in kolab horde:
		 * 
		 * <start-date>2009-12-12</start-date>
			<end-date>2009-12-12</end-date>

			An all day appointment send by the client and understood in the right way from kolab.
			But if you save the appointment again in kolab horde withouth modifying any data, it will
			format the start and end date like the format above:
			
			<start-date>2009-12-11T23:00:00Z</start-date>
			<end-date>2009-12-12T23:00:00Z</end-date> */
		TimeZone tz_ = TimeZone.getDefault();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		funEvent.getSummary().setPropertyValue(kolabEvent.getSummary());
		funEvent.getDescription().setPropertyValue(kolabEvent.getBody());
		funEvent.getCategories().setPropertyValue(kolabEvent.getCategories());
		funEvent.getLocation().setPropertyValue(kolabEvent.getLocation());
		funEvent.setAllDay(kolabEvent.isAllDay());

		/* If you get the same start and endtime (for example 2009-12-08 00:00:00 UTC) and allDay == true, 
		 * that means in kolab horde someone has created a appointment from 12 AM on the 2009-12-08 up to 
		 * 12 AM on the 2009-12-09.  
		 * S60 does not understand this format. You have to add one day to the end date to get it right. 
		 *  
		 * If you get the same start and endtime with allDay == false that means in kolab horde someone
		 * has created a day about 0 hours (No Duration)
		 */
		
		funEvent.getDtStart().setPropertyValue(long2utc(kolabEvent.getStartDate()));
		
		String strDtStart = null;
		String strDtEnd = null;
		Date dtStartDate = null;
		Date dtEndDate = null;
		
		Date dtDateForReminder = new Date();
		
		/* You could get the following data: 			 
		    <start-date>2009-12-11T00:00:00Z</start-date>
			<end-date>2009-12-12T00:00:00Z</end-date> 
			
			It's UTC because start and end date are not the same. Someone created
			an appointsment that last for two days, because if you add at least one day
			you will get 2009-12-11T01:00:00Z to 2009-12-11T01:00:00Z */
		boolean allDayOneDay = false;
		boolean allDayMoreThanOneDay = false;
		
		if(kolabEvent.getStartDate().equals(kolabEvent.getEndDate()) //Just one day but allDay
				&& kolabEvent.isAllDay() == true) {
				/* Fix for bug [#51] */
				if(kolabEvent.getStartDate().getHours() == 0 //This if tree is to get rid of the utc time
						&& kolabEvent.getEndDate().getHours() == 0) { /* Here is no UTC time presented, so we have to get rid off UTC */
					
					//Start date				
					strDtStart = Helper.long2dateStr(kolabEvent.getStartDate());
					funEvent.getDtStart().setPropertyValue(strDtStart);
					
					//End date
					strDtEnd = Helper.long2dateStr(kolabEvent.getEndDate());				
					funEvent.getDtEnd().setPropertyValue(strDtEnd);
					
					allDayOneDay = true;				
					//TODO: Is it ok to add a day for the reminder.setTime() method?
				} else { /* There is time present in the start and end date that's not equal 00:00:00Z, so we have not
						    to eliminate the utc (Z) definition in the string, because the string is realy utc.
						    But we have to add a day, to get the Symbian to show an all day event */
		
					
					//End date		
					strDtEnd = Helper.long2dateStr(kolabEvent.getEndDate());
					funEvent.getDtEnd().setPropertyValue(strDtEnd);
					
				}
					
		} else if(kolabEvent.getStartDate().equals(kolabEvent.getEndDate()) /* Maybe there will be a reason to keep that line */
				&& kolabEvent.isAllDay() == false) {
			
					funEvent.getDtStart().setPropertyValue(long2utc(kolabEvent.getStartDate()));
					funEvent.getDtEnd().setPropertyValue(long2utc(kolabEvent.getEndDate())); 

		} else { //kolabEvent.getStartDate() != kolabEvent.getEndDate();
			
			if(kolabEvent.getStartDate().getHours() == 0 //More days than one.
					&& kolabEvent.getEndDate().getHours() == 0) { // If that happens no UTC time is presented in the date or may not be presented in the date)
				
				// Start date
				strDtStart = Helper.long2dateStr(kolabEvent.getStartDate());
				funEvent.getDtStart().setPropertyValue(strDtStart);
				
				//End date
				strDtEnd = Helper.long2dateStr(kolabEvent.getEndDate());
				funEvent.getDtEnd().setPropertyValue(strDtEnd);

				allDayOneDay = true;
				allDayMoreThanOneDay = true;
			} else {
				funEvent.getDtEnd().setPropertyValue(long2utc(kolabEvent.getEndDate()));
			}
		}
		
		com.funambol.common.pim.calendar.Reminder funReminder = new com.funambol.common.pim.calendar.Reminder();
		funReminder.setActive(false);			
		
		if(kolabEvent.getAlarm() != null && kolabEvent.getAlarm() != 0){
		
		java.util.Calendar cal_3 = new GregorianCalendar();
    	cal_3.set(2010, java.util.Calendar.JANUARY, 1, 0, 0, 0);  
    	
    	Date dtDateForReminder_ = cal_3.getTime();
		
		String strDateForReminder_ = null;
		
		if(allDayOneDay == true) {
			dtDateForReminder = Helper.utc2long(funEvent.getDtStart().getPropertyValueAsString()); //Helper.eliminateUTC(funEvent.getDtStart().getPropertyValueAsString());
			
			dtDateForReminder_.setYear(dtDateForReminder.getYear());
			dtDateForReminder_.setMonth(dtDateForReminder.getMonth());
			dtDateForReminder_.setDate(dtDateForReminder.getDate() - 1);
			dtDateForReminder_.setHours(23);
			dtDateForReminder_.setMinutes(60 - kolabEvent.getAlarm());
			dtDateForReminder_.setSeconds(0);
		} else {
			dtDateForReminder = Helper.utc2long(funEvent.getDtStart().getPropertyValueAsString());
			
			dtDateForReminder_.setYear(dtDateForReminder.getYear());
			dtDateForReminder_.setMonth(dtDateForReminder.getMonth());
			dtDateForReminder_.setDate(dtDateForReminder.getDate());
			dtDateForReminder_.setHours(dtDateForReminder.getHours());
			dtDateForReminder_.setMinutes(dtDateForReminder.getMinutes() - kolabEvent.getAlarm());
			dtDateForReminder_.setSeconds(0);
		}
		
		funReminder.setActive(true);
		funReminder.setMinutes(kolabEvent.getAlarm());
		
		if(allDayOneDay == true) {
		strDateForReminder_ = Helper.long2localtime(dtDateForReminder_); // If the date is from 12 AM to 12 AM on the same day
		} else {
		strDateForReminder_ = Helper.long2utc(dtDateForReminder_);
		}
		
		funReminder.setTime(strDateForReminder_);
		funEvent.setReminder(funReminder);
		}
		
		///NEW Begin
		//Attendee Begin
		int j = kolabEvent.getAttendee().size();
		com.funambol.common.pim.calendar.Attendee[] funambolAttendee = new com.funambol.common.pim.calendar.Attendee[j];
		
		Short shShort = -1;
		for(int i = 0; i<j; i++) {
			
			funambolAttendee[i] = new com.funambol.common.pim.calendar.Attendee();	
			funambolAttendee[i].setEmail(kolabEvent.getAttendee().get(i).getSmtpAddress());
			funambolAttendee[i].setName(kolabEvent.getAttendee().get(i).getDisplayName());
			
			if(kolabEvent.getAttendee().get(i).getRole() != null){
				switch (kolabEvent.getAttendee().get(i).getRole()){
					case OPTIONAL: shShort = 1;  // ?
						break;
					case REQUIRED: shShort = 2;
						break;
					case RESOURCE: shShort = -1; 
				/* -1 is unexpected in funambol. Funambol does not know the value resource. 
				 * The funambol value by default "UNKNOWN" will be set.
				 * */
						break;
					default: shShort = -1;
						break;
			}
			funambolAttendee[i].setExpected(shShort);
			}

			shShort = 8;
			if(kolabEvent.getAttendee().get(i).getStatus()!= null){
				switch (kolabEvent.getAttendee().get(i).getStatus()){
					case ACCEPTED: shShort = 5;
						break;
					case DECLINED: shShort = 0; 
						break;
					case NONE: shShort = 8; //UNKNOWN in funambol
						break;
					case TENTATIVE: shShort = 4;
						break;
					default: shShort = 8;
						break;
			}
			funambolAttendee[i].setStatus(shShort);
			}
			
			funambolAttendee[i].setName(kolabEvent.getAttendee().get(i).getDisplayName());
			funambolAttendee[i].setEmail(kolabEvent.getAttendee().get(i).getSmtpAddress());

			funEvent.addAttendee(funambolAttendee[i]);		
		}
		//Attendee End
		
		//Uid
		funEvent.getUid().setPropertyValue(kolabEvent.getUid());

		//Creation date
		String strCreationDate = Helper.long2utc(kolabEvent.getCreationDate());
		funEvent.getCreated().setPropertyValue(strCreationDate);
		
		//Last modified date
		String strLastModifiedDate = Helper.long2utc(kolabEvent.getLastModificationDate());
		funEvent.getLastModified().setPropertyValue(strLastModifiedDate);
		
		//Status
		if(kolabEvent.getShowTimeAs() != null){
		switch (kolabEvent.getShowTimeAs()) {
			case FREE: funEvent.setBusyStatus(CALENDAR_CONTENT_BUSY_STATUS_OL_FREE); break;
			case TENTATIVE: funEvent.setBusyStatus(CALENDAR_CONTENT_BUSY_STATUS_OL_TENTATIVE); break;
			case BUSY: funEvent.setBusyStatus(CALENDAR_CONTENT_BUSY_STATUS_OL_BUSY); break;
			case OUTOFOFFICE: funEvent.setBusyStatus(CALENDAR_CONTENT_BUSY_STATUS_OL_OUT_OF_OFFICE); break;
			default: throw new RuntimeException("wsdl definition has changed");
		}
		}
		
    	if(kolabEvent.getSensitivity() != null 
    			&& kolabEvent.getSensitivity().toString().trim() != "") {
    		Sensitivity sens = kolabEvent.getSensitivity();
    		if (sens.equals(Sensitivity.CONFIDENTIAL))
    		{
    			funEvent.getAccessClass().setPropertyValue("0");
    		} 
    		else if (sens.equals(Sensitivity.PRIVATE))
    		{
    			funEvent.getAccessClass().setPropertyValue("2");
    		}
    		else if (sens.equals(Sensitivity.PUBLIC))
    		{
    			funEvent.getAccessClass().setPropertyValue("0");
    		} 
    		else
    		{
    			funEvent.getAccessClass().setPropertyValue("0");
    		}
    	}
		
		funEvent.setRecurrencePattern(RecurrenceConverter.convertRecurrence(kolabEvent.getRecurrence(), kolabEvent.getStartDate()));
		
		if(allDayOneDay == false) {
			funEvent.setAllDay(false);
		} 	
		
		if(kolabEvent.getRecurrence() != null){
		if(kolabEvent.getRecurrence().getCycle().equals(org.evolvis.bsi.kolab.service.Cycle.YEARLY) && allDayMoreThanOneDay == true){ /* If you get an all day appointment 
			thats recurrence is all day you will get an anniversary on the s60, so AllDay has to be false. So an all day appointment that is every year one the same date, will
			be an anniversary */
			funEvent.setAllDay(false);
		}
		}
		
		
		
		TimeZone.setDefault(tz_);
		
		cal.setEvent(funEvent);
		return cal;		
	}
	
	private static Object getPropertyValue(Property property) {
		if (property == null)
			return null;
		return property.getPropertyValue();
	}

	// Convert from Funambol vCalendar to Kolab Event
	public static Event convertToEvent(Calendar cal, TimeZone deviceZone, String strDeviceZone, String contentType) throws ConversionException {
		com.funambol.common.pim.calendar.Event funEvent = cal.getEvent(); // Converts vCalendar to Funambol event
		Event kolabEvent = new Event();
		
		kolabEvent.setSummary((String) getPropertyValue(funEvent.getSummary()));
		kolabEvent.setBody((String) getPropertyValue(funEvent.getDescription()));
		kolabEvent.setCategories((String) getPropertyValue(funEvent.getCategories()));
		kolabEvent.setLocation((String) getPropertyValue(funEvent.getLocation()));
		
		kolabEvent.setAllDay(funEvent.isAllDay());

		int intMinutes = 0;
		if(funEvent.getReminder() != null 
				&& funEvent.getReminder().isActive() != false) {
		intMinutes = funEvent.getReminder().getMinutes();
		}
		
		//It's definitely needed for Windows Mobile
		if(CONTENT_TYPE_VCAL2.equalsIgnoreCase(contentType)){ 
				if(kolabEvent.isAllDay() == true) { /* The problem appears if allDay == true on Windows Mobile.
					Maybe the s60 would act an other way. */
					if(funEvent.getReminder() != null && 
							funEvent.getReminder().getMinutes() != 0) {
						intMinutes += Helper.getDateFromDeviceZone(strDeviceZone);
					}
				}
		}
		
		// for Bug [#46] Nokia S60: Änderung an einem Jahrestag; Alarm/ Erinnerung wird nicht richtig gesynct
		// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=46&group_id=7&atid=105
		if(intMinutes < 0) { intMinutes *= -1;}
		
		Reminder reminder = null;
		if(funEvent.getReminder() != null) {
		reminder = funEvent.getReminder();
		}

		if (reminder != null && reminder.getMinutes() != 0){
			kolabEvent.setAlarm(intMinutes); 
		}
		
		Date dtStartDate = utc2long((String) getPropertyValue(funEvent.getDtStart()));
		Date dtEndDate = utc2long((String) getPropertyValue(funEvent.getDtEnd()));

		kolabEvent.setStartDate(dtStartDate);
		kolabEvent.setEndDate(dtEndDate);

		if (funEvent.getBusyStatus() != null)
			switch (funEvent.getBusyStatus()) {
				case CALENDAR_CONTENT_BUSY_STATUS_OL_FREE: kolabEvent.setShowTimeAs(ShowTimeAs.FREE); break;
				case CALENDAR_CONTENT_BUSY_STATUS_OL_TENTATIVE: kolabEvent.setShowTimeAs(ShowTimeAs.TENTATIVE); break;
				case CALENDAR_CONTENT_BUSY_STATUS_OL_BUSY: kolabEvent.setShowTimeAs(ShowTimeAs.BUSY); break;
				case CALENDAR_CONTENT_BUSY_STATUS_OL_OUT_OF_OFFICE: kolabEvent.setShowTimeAs(ShowTimeAs.OUTOFOFFICE); break;
				default: log.warn("unkown event busy status code: "+funEvent.getBusyStatus());
			}
		
		//Attendee Begin
		int j = funEvent.getAttendees().size();
		org.evolvis.bsi.kolab.service.Attendee[] kolabAttendee = new org.evolvis.bsi.kolab.service.Attendee[j];
		
		for(int i = 0; i<j; i++) {
			
			kolabAttendee[i] = new org.evolvis.bsi.kolab.service.Attendee();
			kolabAttendee[i].setDisplayName(funEvent.getAttendees().get(i).getName());
			kolabAttendee[i].setSmtpAddress(funEvent.getAttendees().get(i).getEmail());
			
			switch(funEvent.getAttendees().get(i).getExpected()){
				case 1: kolabAttendee[i].setRole(Role.OPTIONAL);
					break;
				case 2: kolabAttendee[i].setRole(Role.REQUIRED);
					break;
				case 8: kolabAttendee[i].setRole(Role.RESOURCE);
					break;
				default: kolabAttendee[i].setRole(Role.RESOURCE);
					break;
			}
			
			switch(funEvent.getAttendees().get(i).getStatus()){
				case 5: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.ACCEPTED);  
					break;
				case 0: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.DECLINED);
					break;
				case 8: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.NONE);
					break;
				case 4: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.TENTATIVE);
					default: kolabAttendee[i].setStatus(org.evolvis.bsi.kolab.service.AttendeeStatus.NONE);;
			}
			
			kolabAttendee[i].setDisplayName(funEvent.getAttendees().get(i).getName());
			kolabAttendee[i].setSmtpAddress(funEvent.getAttendees().get(i).getEmail());

			kolabEvent.getAttendee().add(kolabAttendee[i]);	
		}
		//Attendee End
		
		if (funEvent.getUid() != null)
			kolabEvent.setUid(funEvent.getUid().getPropertyValueAsString());
		///NEW End

		org.evolvis.bsi.kolab.service.Recurrence kolabRecPa;
		kolabRecPa = RecurrenceConverter.convertRecurrencePattern(funEvent.getRecurrencePattern(), deviceZone);
		
		kolabEvent.setRecurrence(kolabRecPa);	
		
			if(funEvent.getAccessClass().getPropertyValue() != null 
					&& funEvent.getAccessClass().getPropertyValueAsString() != null 
						&& funEvent.getAccessClass().getPropertyValueAsString().trim() != ""){

				switch(Integer.parseInt(funEvent.getAccessClass().getPropertyValueAsString())){
				case 0: kolabEvent.setSensitivity(Sensitivity.PUBLIC);
					break;
				case 3: kolabEvent.setSensitivity(Sensitivity.CONFIDENTIAL);
					break;
				case 2: kolabEvent.setSensitivity(Sensitivity.PRIVATE);
					break;
				case 1: kolabEvent.setSensitivity(Sensitivity.PRIVATE); /* Sensitivity "personal" not available,
				but if something is personal it is rather private than public */
				}	
			}	
		
		return kolabEvent;
	}
	
	/*
	 * in funambol: so = 1; mo = 2; sa = 64
	 * in kolab: mo = 1; so = 64
	 */
	public static final int convertDayOfWeekMaskToKolab(short mask) {
		if (mask < 0 || mask > 127)
			throw new RuntimeException("invalid day of week mask: "+mask);
		return ((mask & 1) << 6)
				| mask >> 1;
	}
	
	public static final short convertDayOfWeekMaskFromKolab(int mask) {
		if (mask < 0 || mask > 127)
			throw new RuntimeException("invalid day of week mask: "+mask);
		return (short) ((mask >> 6)
				| (mask & 63) << 1);
	}

	// see inline doc in: com.funambol.common.pim.calendar.CalendarContent
	private static final short CALENDAR_CONTENT_BUSY_STATUS_OL_FREE = 0;
	private static final short CALENDAR_CONTENT_BUSY_STATUS_OL_TENTATIVE = 1;
	private static final short CALENDAR_CONTENT_BUSY_STATUS_OL_BUSY = 2;
	private static final short CALENDAR_CONTENT_BUSY_STATUS_OL_OUT_OF_OFFICE = 3;
	
	
}
