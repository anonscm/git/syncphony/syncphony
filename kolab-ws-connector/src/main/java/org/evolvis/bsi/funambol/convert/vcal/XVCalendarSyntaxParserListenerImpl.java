/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcal;

import java.util.ArrayList;
import java.util.Date;

import org.evolvis.bsi.funambol.source.Helper;

import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.model.VEvent;
import com.funambol.common.pim.model.VTodo;
import com.funambol.common.pim.model.VAlarm;
import com.funambol.common.pim.model.VTimezone;
import com.funambol.common.pim.model.TzStandardComponent;
import com.funambol.common.pim.model.TzDaylightComponent;
import com.funambol.common.pim.model.Property;
import com.funambol.common.pim.model.Parameter;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.ParserParam;
import com.funambol.common.pim.ParserProperty;
import com.funambol.common.pim.model.VCalendarContent;
//import com.funambol.common.pim.xvcalendar.XVCalendarSyntaxParserListener;

/* There is no previous file because of pim funambol prim-framework 8.0.0
 * Therefore the class is not called XVCalendarSyntaxParserListenerImplExtended
 * In Version 8.2.2 there is a XVCalendarSyntaxParserListenerImplExtended */

public class XVCalendarSyntaxParserListenerImpl {

    private VCalendar           calendar  = null;
    private VEvent              event     = null;
    private VTodo               todo      = null;
    private VTimezone           timezone  = null;
    private VAlarm              alarm     = null;
    private TzStandardComponent standardc = null;
    private TzDaylightComponent daylightc = null;
    private VCalendarContent    current   = null;
    

    public XVCalendarSyntaxParserListenerImpl(VCalendar calendar) {
        this.calendar = calendar;
    }

    public void start() {
    }

    public void end() {
    }

    public void addProperty(ParserProperty property) {
        calendar.addProperty(buildProperty(property));
    }

    public void startEvent() {
        event   = new VEvent();
        current = event;
    }

    public void endEvent() {
        calendar.addEvent(event);
        current = null;
    }

    public void addEventProperty(ParserProperty property) {
        event.addProperty(buildProperty(property));
    }

    public void startToDo() {
        todo = new VTodo();
        current = todo;
    }

    public void endToDo() {
        calendar.addTodo(todo);
        current = null;
    }

    public void addToDoProperty(ParserProperty property) {	
        todo.addProperty(buildProperty(property));
    } 

    @SuppressWarnings("unchecked")
	private Property buildProperty(ParserProperty pproperty) {

        ArrayList params = pproperty.getParameters();
        ArrayList newParams = new ArrayList();
        if (params != null) {
            // Each param is a ParserParameter and we need to tranform it into a
            // Parameter
            for(int i=0;i<params.size();++i) {
                ParserParam p = (ParserParam)params.get(i);                
                Parameter newP  = new Parameter(p.getName(), p.getValue());     
                newParams.add(newP);
            }
        }
        
		// for Bug [#45] BEGIN: Nokia S60: Bei einem ganztägigen Termin wird die Uhrzeit nicht richtig gesynct
		// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=45&group_id=7&atid=105	       
        
        Property p = null;
       
        /* Property p_ = null;
        ArrayList newParams_ = new ArrayList();
        if(Helper.getBoolAddedAllDayOnce() == false) {
        		 p_ = new Property("X-FUNAMBOL-ALLDAY", false, newParams_, "0");
        }
        
        if(pproperty.getName().equalsIgnoreCase("DTSTART")) {
        	Helper.setStrXVCalendarStDate(pproperty.getValue());
        	Integer.valueOf(Helper.getStrXVCalendarStDate().substring(9,11)); 
        }
        
        if(pproperty.getName().equalsIgnoreCase("DTEND") 
        		&& Helper.getStrXVCalendarStDate().substring(9, 11) != "12"
        			&& pproperty.getValue().substring(9, 11) != "12") { 
        try {
        	
        	Date dtDate = Helper.utc2long(Helper.getStrXVCalendarStDate());
        	Date dtDate_ = Helper.utc2long(pproperty.getValue());
        	
			if(Helper.getDateDifferencesInHours(dtDate, dtDate_) == 24){
					p = new Property("DTEND",pproperty.getCustom(),newParams,Helper.long2localtimeAddADay(Helper.utc2long(pproperty.getValue()), Helper.getStrXVCalendarStDate().substring(9, 11)));	 
			     } else {
			    	p = new Property(pproperty.getName(), pproperty.getCustom(), newParams, pproperty.getValue());
			     }
		} catch (ConversionException e) {
			e.printStackTrace();
		}
        }
 
        if(!pproperty.getName().equalsIgnoreCase("DTEND")) {
        p = new Property(pproperty.getName(), pproperty.getCustom(), newParams, pproperty.getValue());
        } */

        p = new Property(pproperty.getName(), pproperty.getCustom(), newParams, pproperty.getValue());

        return p;
    }
}

