/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.evolvis.bsi.funambol.source.Helper.*;

import java.util.Date;
import java.util.List;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.source.Helper.KolabType;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.Note;




/**
 * @author Hendrik Helwich
 *
 */
public class KolabNoteSyncSource extends KolabSingleTypeSource {

	public KolabNoteSyncSource() {
		super(KolabType.NOTE);
	}

	@Override
	protected List<Folder> getItemFolders() throws Exception {
		return getClient().getNoteFolders();
	}

	@Override
	protected List<String> getAllItemIDs(Folder folder, KolabType type, Date updateTime) throws Exception {
		return getClient().getNoteIDs(folder, updateTime);
	}

	@Override
	protected List<String> getUpdatedItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		return getClient().getUpdatedNoteIDs(folder, lastUpdateTime, updateTime);
	}

	@Override
	protected List<String> getDeletedItemIDs(Folder folder, KolabType type,
			Date lastUpdateTime, Date updateTime) throws Exception {
		return getClient().getRemovedNoteIDs(folder, lastUpdateTime, updateTime);
	}

	@Override
	protected List<String> getNewItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		return getClient().getNewNoteIDs(folder, lastUpdateTime, updateTime);
	}

	@Override
	protected String addItem(Folder folder, KolabType type, String content, String contentType)
			throws Exception {
		com.funambol.common.pim.note.Note fnote = createNote(content, contentType, getTimezone(),
				getCharset());
		Note note = Converter.convertToNote(fnote);
		return getClient().addNote(folder, note);
	}

	@Override
	protected boolean updateItem(Folder folder, KolabType type, String id, Date lastUpdateTime,
			String content, String contentType) throws Exception {
		com.funambol.common.pim.note.Note fcontact = createNote(content, contentType, getTimezone(),
				getCharset());
		Note note = Converter.convertToNote(fcontact);
		note.setUid(id);
		return getClient().updateNote(folder, note, lastUpdateTime);
	}

	@Override
	protected boolean removeItem(Folder folder, KolabType type, String id, Date lastUpdateTime)
			throws Exception {
		return getClient().removeNote(folder, id, lastUpdateTime);
	}

	@Override
	protected String getItem(Folder folder, KolabType type, String id, String contentType)
			throws Exception {
		Note note = getClient().getNote(folder, id);
		if (note == null)
			return null;
		com.funambol.common.pim.note.Note fnote = Converter
				.convertNote(note);
		String content = createContent(fnote, contentType, getTimezone(),
				getCharset());
		return content;
	}

}
