/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;


import com.funambol.framework.core.SyncML;
import com.funambol.framework.core.Sync4jException;
import com.funambol.framework.engine.pipeline.InputMessageProcessor;
import com.funambol.framework.engine.pipeline.OutputMessageProcessor;
import com.funambol.framework.engine.pipeline.MessageProcessingContext;
import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;
import com.funambol.framework.tools.SyncMLUtil;

/**
 * This class just logs input and output messages. This synclet can then be
 * inserted in any position in a pipeline in order to trace how a message is
 * changed during the pipeline processing. It is at the same time an input and
 * an output pipeline component.<br>
 * Logging is done at INFO level
 */
public class KolabWsSynclet implements InputMessageProcessor, OutputMessageProcessor {
    
    // --------------------------------------------------------------- Constants

    public static final String LOG_NAME = "kolabconnector";

    // ------------------------------------------------------------ Private data

    private static final FunambolLogger log = FunambolLoggerFactory.getLogger(LOG_NAME);

    // ---------------------------------------------------------- Public methods

    /**
     * Logs the input message and context
     *
     * @param processingContext the message processing context
     * @param message the message to be processed
     *
     * @throws Sync4jException
     */
    public void preProcessMessage(MessageProcessingContext processingContext,
                                  SyncML                   message)
    throws Sync4jException {
        if (log.isInfoEnabled()) {
            log.info("--------------------------------------------------------------------------------");
            log.info("Input message processing context"                                                );
            log.info("                                                                                ");
            log.info(processingContext.toString()                                                      );
            log.info("--------------------------------------------------------------------------------");
            log.info("Input message"                                                                   );
            log.info("                                                                                ");
            log.info(SyncMLUtil.toXML(message)                                                               );
            log.info("--------------------------------------------------------------------------------");
        }
    }

    /**
     * Logs the output message and context
     *
     * @param processingContext the message processing context
     * @param message the message to be processed
     *
     * @throws Sync4jException
     */
    public void postProcessMessage(MessageProcessingContext processingContext,
                                   SyncML                   message)
    throws Sync4jException {
        if (log.isInfoEnabled()) {
            log.info("--------------------------------------------------------------------------------");
            log.info("Output message processing context"                                               );
            log.info("                                                                                ");
            log.info(processingContext.toString()                                                      );
            log.info("--------------------------------------------------------------------------------");
            log.info("Output message"                                                                  );
            log.info("                                                                                ");
            log.info(SyncMLUtil.toXML(message)                                                               );
            log.info("--------------------------------------------------------------------------------");
        }
    }


}
