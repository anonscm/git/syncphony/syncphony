/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.ws.BindingProvider;

import org.evolvis.bsi.funambol.convert.ConvertException;
import org.evolvis.bsi.funambol.convert.vcard.VCardSyntaxParser;
import org.evolvis.bsi.funambol.convert.vcard.VCardSyntaxParserListener;

import org.evolvis.bsi.funambol.convert.vcal.XVCalendarSyntaxParser; //The new one
import org.evolvis.bsi.funambol.convert.vcal.XVCalendarSyntaxParserListener;

import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.KolabService;
import org.evolvis.bsi.kolab.service.KolabServicePortType;
import org.evolvis.bsi.kolab.service.KolabWebService;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.AttributedString;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.ObjectFactory;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.PasswordString;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.SecurityHeaderType;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.UsernameTokenType;
import org.xml.sax.SAXException;

import com.funambol.common.pim.calendar.Calendar;
import com.funambol.common.pim.calendar.Task;
import com.funambol.common.pim.common.ConversionException;
import com.funambol.common.pim.common.Property;
import com.funambol.common.pim.common.PropertyWithTimeZone;
import com.funambol.common.pim.contact.Contact;
import com.funambol.common.pim.contact.SIFC;
import com.funambol.common.pim.converter.BaseConverter;
import com.funambol.common.pim.converter.ContactToSIFC;
//import com.funambol.common.pim.converter.ContactToVcard;
import org.evolvis.bsi.funambol.convert.vcard.ContactToVcard;
import com.funambol.common.pim.converter.ConverterException;
import com.funambol.common.pim.converter.NoteToSIFN;
import com.funambol.common.pim.converter.TaskToSIFT;
//import com.funambol.common.pim.converter.VCalendarConverter;
import org.evolvis.bsi.funambol.convert.vcal.VCalendarConverter;
import com.funambol.common.pim.converter.VComponentWriter;
import com.funambol.common.pim.icalendar.ICalendarParser;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.note.Note;
import com.funambol.common.pim.sif.SIFCParser;
import com.funambol.common.pim.sif.SIFCalendarParser;
import com.funambol.common.pim.sif.SIFNParser;
import com.funambol.common.pim.xvcalendar.ParseException;
//import com.funambol.common.pim.xvcalendar.XVCalendarParser; //The old one
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;
import com.funambol.framework.tools.Base64;

/**
 * @author Hendrik Helwich, Patrick Apel, tarent GmbH
 *
 */
public class Helper {

	private static final char ID_SEPARATOR_CHAR = '.'; // can be any char
	private static final char ID_ESCAPE_CHAR = '\\'; // can be any char but must be different to ID_SEPARATOR_CHAR 

    private static final String LOG_NAME = "kolab-ws-connector";
    
    public static final FunambolLogger log = FunambolLoggerFactory.getLogger(LOG_NAME);
    
    private static boolean boolDeleteMinutes = false;
    
    public static TimeZone deviceTimeZone = null;
    
    public static void setBoolDeleteMinutes(boolean deleteMinutes) {
    	boolDeleteMinutes = deleteMinutes;
    }
    
    public static boolean getBoolDeleteMinutes() {
    	return boolDeleteMinutes;
    }
    
    public enum KolabType {
    	
    	EVENT('E'), TASK('T'), NOTE('N'), CONTACT('C');
    	
    	private char idChar;
    	
    	private KolabType(char c) {
    		idChar = c;
    	}

		public char getChar() {
			return idChar;
		}

		public static KolabType getType(char c) {
			for (KolabType type : values())
				if (type.idChar == c)
					return type;
			throw new IllegalArgumentException("invalid type char: "+c);
		}
		
    }

    
	/**
	 * Merge the given strings to a global funambol id.
	 * 
	 * @param  folderName
	 * @param  kolabId
	 * @return
	 */
	public static final String convertToFunambolId(String folderName, String kolabId, KolabType type) {
		if (type == null)
			throw new NullPointerException();
		// escape all special chars in folderName string
		folderName = escapeChars(folderName, ID_ESCAPE_CHAR);
		folderName = escapeChars(folderName, ID_SEPARATOR_CHAR);
//		throw new RuntimeException();

		return type.getChar() + folderName + ID_SEPARATOR_CHAR + kolabId;
	}

	/**
	 * Escape all chars in the given string which are equal to the given char
	 * by prefixing with the char {@link #ID_ESCAPE_CHAR}.
	 * 
	 * @param  str
	 * @param  chr
	 * @return
	 */
	private static final String escapeChars(String str, char chr) {
		// escape all chars in the string
		int idx2 = str.indexOf(chr);
		if (idx2 == -1)
			return str;
		StringBuilder sb = new StringBuilder();
		int idx = 0;
		do {
			sb.append(str.substring(idx, idx2));
			sb.append(ID_ESCAPE_CHAR);
			idx = idx2;
			idx2 = str.indexOf(chr, idx2+1);
		} while (idx2 != -1);
		sb.append(str.substring(idx));
		return sb.toString();
	}
	
	/**
	 * Split the global funambol id in the two components imap folder name and
	 * kolab id.
	 * 
	 * @param  funambolId
	 * @return An array with two elements. First: folder name, Second: kolab id.
	 */
	public static final KolabId convertToKolabId(String funambolId) {
		KolabType type = KolabType.getType(funambolId.charAt(0));
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < funambolId.length(); i ++) {
			char chr = funambolId.charAt(i);
			if (chr == ID_ESCAPE_CHAR) {
				if (++ i == funambolId.length())
					break;
				sb.append(funambolId.charAt(i));
				continue;
			} else if (chr == ID_SEPARATOR_CHAR) {
				String folderName = sb.toString();
				String kolabId = funambolId.substring(i+1);
				return new KolabId(folderName, kolabId, type);
			} else
				sb.append(chr);
		}
		throw new RuntimeException("invalid funambol id '"+funambolId+"'");
	}
	
	static class KolabId {
		
		private final Folder folder;
		private final String guid;
		private final KolabType type;
		
		public KolabId(String folderName, String guid, KolabType type) {
			folder = new Folder();
			folder.setName(folderName);
			this.guid = guid;
			this.type = type;
		}

		public Folder getFolder() {
			return folder;
		}

		public String getGuid() {
			return guid;
		}

		public KolabType getType() {
			return type;
		}

	}
	
	public static final KolabServicePortType getServiceCient(String url) {
		// TODO optimize later
		KolabService ks = new KolabWebService();
		KolabServicePortType client = ks.getKolabServicePort();
		((BindingProvider)client).getRequestContext().put(
				BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
		return client;
	}
	
    /**
     * return the date in the "yyyyMMdd'T'HHmmss'Z'" format
     * source timezone is UTC.
     * 
     * @param l time in msec
     * @return the date in the "yyyyMMdd'T'HHmmss'Z'" format
     */
    public static String long2utc(Date date) {
        return long2localtime(date) + 'Z';
    }
    
    @SuppressWarnings("deprecation")
	public static String long2localtimeRemoveSomeDay(Date date, int intDays_) {
    	int intDay = date.getDate() - intDays_; 
    	date.setDate(intDay);
        SimpleDateFormat LOCAL_DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        LOCAL_DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC")); 
        String strDate = LOCAL_DATE_FORMATTER.format(date);
        return strDate;
    } 
    
    public static String long2dateStr(Date date) {
        SimpleDateFormat LOCAL_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd"); //yyyMMdd
        LOCAL_DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC")); 
        String strDate = LOCAL_DATE_FORMATTER.format(date);
        return strDate;
    }

    /**
     * return the date in the "yyyyMMdd'T'HHmmss" format
     * source timezone is UTC
     * @param l time in msec
     * @return the date in the "yyyyMMdd'T'HHmmss" format
     */
    public static String long2localtime(Date date) {
        SimpleDateFormat LOCAL_DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        LOCAL_DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC")); 
        String strDate = LOCAL_DATE_FORMATTER.format(date);
        return strDate;
    }
    
    public static String long2localtime_(Date date, String strShortTimeZone) {
        SimpleDateFormat LOCAL_DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        LOCAL_DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone(strShortTimeZone)); 
        String strDate = LOCAL_DATE_FORMATTER.format(date);
        return strDate;
    } 
    
    public static String dateToString(Date date) {
        SimpleDateFormat LOCAL_DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        LOCAL_DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC")); 
        String strDate = LOCAL_DATE_FORMATTER.format(date);
        return strDate;
    }
    
    /**
     * return the date in the long value (msec)
     * 
     * @param utc String
     * @return the date in the long value
     */
    public static Date utc2long(String utc) throws ConversionException {
        try {
            String format = null;
            if (utc.length() == 16) {
                format = "yyyyMMdd'T'HHmmss'Z'";
            } else if (utc.length() == 15) {
            	format = "yyyyMMdd'T'HHmmss";
            } else if (utc.length() <= 10 && utc.length() >= 6) {
                utc = utc.replaceAll("-", "");
                format = "yyyyMMdd";
            } else if (utc.length() == 0) {
            	Helper.setBoolDeleteMinutes(true);
            	Date date = new Date();
            	return date; 
            } else {
                throw new Exception ("Error: no available format for the date");
            }
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            formatter.setLenient(true);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = formatter.parse(utc);
            return date;
        } catch (Exception e) {
            throw new ConversionException("Error converting utc 2 long.", e);
        }
    }   

     public static Date getDatetimeInDeviceTZ(String datetime_) throws ConversionException {
    	
    	  String format = "yyyyMMdd'T'HHmmss";
    	  SimpleDateFormat formatter = new SimpleDateFormat(format);
    	  TimeZone tz = Helper.getDeviceTimeZone();
    	  formatter.setTimeZone(tz);
          formatter.setLenient(true);
          Date dtDate = null;
		try {
			dtDate = formatter.parse(datetime_);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

    	  return dtDate;
    } 
    
    public static TimeZone getDeviceTimeZone(){
    	return deviceTimeZone;
    }
    
    public static Date eliminateTime(String strDateTime, TimeZone tz) throws ConversionException {
        try {
        	/* Method to avoid time (datatype datetime) in the data type date
        	 * Possible formats: 
        	 *  yyyyMMddXXXXXXXX
        		20091112T080000Z
        		20091112T080000 
        		etc.
        		    			*/
        	String suffix ="";
        	String format;
            if (strDateTime.length() == 16) {
                format = "yyyyMMdd'T'HHmmss'Z'";
                suffix = "T000000Z";
            } else if (strDateTime.length() == 15) {
            	format = "yyyyMMdd'T'HHmmss";
            	suffix = "T000000";
            } else if (strDateTime.length() <= 10 || strDateTime.length() >= 7 ) {
            	strDateTime = strDateTime.replaceAll("-", "");
                format = "yyyyMMdd";
            } else {
                throw new Exception ("Error: no available format for the date");
            }
        	
        	String strDate;
                  strDate = strDateTime.substring(0, 8);
            strDate += suffix;      

            //format = "yyyyMMdd";
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            formatter.setLenient(true);
            if(tz != null) {
            	formatter.setTimeZone(tz);
            } else {
            	formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            } 
            Date date = formatter.parse(strDate);
            
            return date;
        } catch (Exception e) {
            throw new ConversionException("Error eliminating time.", e);
        }
    }
    
    public static String getServerTimeZoneDisplayName()
    {
        final TimeZone timeZone = TimeZone.getDefault();
        final boolean daylight = timeZone.inDaylightTime(new Date());
       // final Locale locale = servletRequest.getLocale();
        return timeZone.getDisplayName(daylight, TimeZone.SHORT, Locale.getDefault());
    }
	
    public static int getDateDifferencesInMinutes(Date dtDueDate, Date dtReminderDate) {

		long diffInMilli = dtDueDate.getTime() - dtReminderDate.getTime();
		int diffInMinutes = (int) (diffInMilli / (1000 * 60));
    	
    	return diffInMinutes; 
    }
    
    public static int getDateFromDeviceZone(String currentTimeZone){
		
    	int minutes = 0;

		SimpleDateFormat formatter = new SimpleDateFormat();
		SimpleDateFormat formatter_ = new SimpleDateFormat();
		
		String  format = "yyyyMMdd'T'HHmmss";

		java.util.Calendar cal_1 = new GregorianCalendar();
    	java.util.Calendar cal_2 = new GregorianCalendar();
    	
    	cal_1.set( 2000, java.util.Calendar.JANUARY, 1, 12, 0, 0 );                   
    	cal_2.set( 2000, java.util.Calendar.JANUARY, 1, 12, 0, 0 );    					
    	
    	String strDate1 = dateToString(cal_1.getTime());
    	String strDate2 = dateToString(cal_2.getTime());
    	
    	formatter.applyPattern(format);

    	Date dtDate1 = null; 
    	Date dtDate2 = null;
    	
    	formatter_.applyPattern(format);

    	try {
        if(currentTimeZone == null) {
        	currentTimeZone = "UTC"; //TODO: Try to get the real time zone of the device. Maybe it's not UTC.
        } 
        	TimeZone.setDefault(TimeZone.getTimeZone(currentTimeZone));
    		formatter.setTimeZone(TimeZone.getTimeZone(currentTimeZone));
			dtDate1 = formatter.parse(strDate1);
			
		} catch (java.text.ParseException e) {
			ConvertException.getExceptionInformation(null, e);
		}

		try {
		if(currentTimeZone == null) {
			currentTimeZone = "UTC";  //TODO: Try to get the real time zone of the device. Maybe it's not UTC.
		}
			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
			formatter_.setTimeZone(TimeZone.getTimeZone("UTC"));
			dtDate2 = formatter_.parse(strDate2);
			
		} catch (java.text.ParseException e) {
			ConvertException.getExceptionInformation(null, e);
		}
		
    	long time = dtDate2.getTime() - dtDate1.getTime(); //Differenz in ms
    	time = (time / 1000) / 60; //Differenz in minutes
    	
    	minutes = (int) time; 

		return minutes;
    }
    
    /**
     * return the date in the long value (msec)
     * 
     * 
     * @param localtime
     * @param tz
     * @return  the date in the long value
     * @throws com.funambol.common.pim.common.ConversionException
     */
    public static Date localtime2long(String localtime, TimeZone tz) throws ConversionException {
        try {
            String format;
            if (localtime.length() == 15) {
                format = "yyyyMMdd'T'HHmmss";
            } else if (localtime.length() >= 8 && localtime.length() <= 10) {
                localtime = localtime.replaceAll("-", "");
                format = "yyyyMMdd";
            } else {
            if(localtime.length() == 16){
            	format = "yyyyMMdd'T'HHmmss'Z'";
            	tz = TimeZone.getTimeZone("UTC");
            }else{
                throw new Exception ("Error: no available format for the date");
            }
            }
            
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            formatter.setLenient(true);  
            if(tz != null) {
            	formatter.setTimeZone(tz);
            }
            
            Date date = formatter.parse(localtime);
            return date;
        } catch (Exception e) {
            throw new ConversionException("Error converting utc 2 long.", e);
        }
    }
    
    public static SyncItemKey[] toSynItemKeys(List<String> ids) {
        SyncItemKey[] sItemKeys = new SyncItemKey[ids.size()];
        for (int i = 0; i < ids.size(); i ++)
            sItemKeys[i] = new SyncItemKey(ids.get(i));
        return sItemKeys;
    }
    
//    public static final String CONTENT_TYPE_VCAL = "vCalendar"; // VCal
    public static final String CONTENT_TYPE_VCAL2 = "text/x-vcalendar"; // VCal text/x-vcalendar
    public static final String CONTENT_TYPE_ICAL = "text/calendar"; // ICal
    public static final String CONTENT_TYPE_SIF_EVENT = "text/x-s4j-sife"; // SIF-Event
    public static final String CONTENT_TYPE_VCARD = "text/x-vcard"; // VCard
    public static final String CONTENT_TYPE_SIF_CONTACT = "text/x-s4j-sifc"; // SIF-Contact
    public static final String CONTENT_TYPE_SIF_TASK = "text/x-s4j-sift";
    public static final String CONTENT_TYPE_PLAIN_TEXT = "text/plain";
    public static final String CONTENT_TYPE_SIFN = "text/x-s4j-sifn";
    
    public static final String VERSION_SIFE  = "1.0";
    public static final String VERSION_SIFT  = "1.0";
    public static final String VERSION_VCAL  = "1.0";
    public static final String VERSION_ICAL  = "2.0";
    public static final String VERSION_SIFN = "1.0";
    public static final String VERSION_PLAIN_TEXT = "1.0";
    public static final String VERSION_SIFC  = "1.0";
    public static final String VERSION_VCARD = "2.1";
    
    public static String createContent(Calendar calendar, String contentType, TimeZone timezone, String charset) throws ConverterException {
    	boolean isVcal = // CONTENT_TYPE_VCAL.equals(contentType)  || 
    		CONTENT_TYPE_VCAL2.equals(contentType);
    	if (!isVcal && !CONTENT_TYPE_ICAL.equals(contentType))
    		throw new ConverterException("cannot convert calendar to content type "+contentType);
		boolean forceClientLocalTime = false;
		VCalendarConverter vcf = new VCalendarConverter(timezone, charset, forceClientLocalTime);
		VCalendar vcalendar = vcf.calendar2vcalendar(calendar, isVcal);
		VComponentWriter writer = new VComponentWriter(VComponentWriter.NO_FOLDING);
        String vcal = writer.toString(vcalendar);
        return vcal;
    }
    
    public static String createContent(Note note, String contentType, TimeZone timezone, String charset) throws ConverterException {

    	if (CONTENT_TYPE_SIFN.equals(contentType))
    	{
    		NoteToSIFN converter = new NoteToSIFN(timezone, charset);
    		return converter.convert(note);
    	} 
    	else if (CONTENT_TYPE_PLAIN_TEXT.equals(contentType))
    	{
    		return note.getTextDescription().getPropertyValueAsString();
    	}
    	throw new ConverterException("can't convert note to content type: " + contentType);
    }
    
    public static String createContent(Task task, String contentType, TimeZone timezone, String charset) throws ConverterException {
    	// windows mobile
    	if (CONTENT_TYPE_VCAL2.equals(contentType))
    	{
    		Calendar cal = new Calendar();
    		cal.setTask(task);
    		boolean isVcal = // CONTENT_TYPE_VCAL.equals(contentType)  || 
        		CONTENT_TYPE_VCAL2.equals(contentType);
        	if (!isVcal && !CONTENT_TYPE_ICAL.equals(contentType))
        		throw new ConverterException("cannot convert calendar to content type "+contentType);
    		boolean forceClientLocalTime = false;
    		VCalendarConverter vcf = new VCalendarConverter(timezone, charset, forceClientLocalTime);
    		VCalendar vcalendar = vcf.calendar2vcalendar(cal, isVcal);
    		VComponentWriter writer = new VComponentWriter(VComponentWriter.NO_FOLDING);
            String vcal = writer.toString(vcalendar);
            return vcal;
    	} else if (CONTENT_TYPE_SIF_TASK.equals(contentType)) {
    		String xml = null;
			BaseConverter c2xml;
			try {
				c2xml = new TaskToSIFT(timezone, charset);
				xml = c2xml.convert(task);
			} catch (Exception e) {
				throw new ConverterException("Error converting Calendar", e);
			}
			return xml;
    	}
   	
    	throw new ConverterException("can't convert task to content type: " + contentType);
    }

    //Creates a vcard for funambol
    public static String createContent(Contact contact, String contentType, TimeZone timezone, String charset) throws ConverterException {
    	if (CONTENT_TYPE_VCARD.equals(contentType)) {
            ContactToVcard c2vc = new ContactToVcard(timezone, charset);
            return c2vc.convert(contact);
    	} else if (CONTENT_TYPE_SIF_CONTACT.equals(contentType)) {
            ContactToSIFC c2xml = new ContactToSIFC(null,null);
            String xml = c2xml.convert(contact);
            xml = swapSIFMappings(xml, SIFC.EMAIL1_ADDRESS, SIFC.EMAIL3_ADDRESS);            
            return xml;
    	}
		throw new ConverterException("cannot convert contact to content type "+contentType);
    }
    
    /**
     * swaps the tag1 and tag2
     * 
     * @param xml
     * @param tag1
     * @param tag2
     * @return
     */
    protected static String swapSIFMappings(String xml, String tag1, String tag2) {
        
                
        if (xml.indexOf(tagSingle(tag1)) == -1 && 
            xml.indexOf(tagSingle(tag2)) == -1 ){            
            
            final String SWAPPER = "___SWAPPER___";
            // i.e. replace <Email1Address> and </Email1Address> with ___SWAPPER___
            xml = xml.replaceAll(tagClose(tag1)   , tagClose(SWAPPER));
            xml = xml.replaceAll(tagOpen(tag1)    , tagOpen(SWAPPER));

            xml = xml.replaceAll(tagClose(tag2)   , tagClose(tag1));
            xml = xml.replaceAll(tagOpen(tag2)    ,  tagOpen(tag1));

            xml = xml.replaceAll(tagClose(SWAPPER), tagClose(tag2));
            xml = xml.replaceAll(tagOpen(SWAPPER) , tagOpen(tag2));
            
        } else {
            
            // search single tag
            
            if (xml.indexOf(tagSingle(tag2)) != -1 ){           
                
                // NOTE the empty value is: <Email3Address/>
                
                // replace <Email3Address/> with <Email1Address/>
                xml = xml.replaceAll(tagSingle(tag2), tagSingle(tag1));            
                // i.e. replace <Email1Address> and </Email1Address> with 
                // <Email3Address> and </Email3Address>
                xml = xml.replaceAll(tagClose(tag1), tagClose(tag2));
                xml = xml.replaceAll(tagOpen(tag1), tagOpen(tag2));
                
            } else {
                
                // NOTE the empty value is: <Email1Address/>
                // replace <Email1Address/> with <Email3Address/>
                xml = xml.replaceAll(tagSingle(tag1), tagSingle(tag2));            
                // replace <Email3Address> and </Email3Address> with 
                // <Email1Address> and </Email1Address>
                xml = xml.replaceAll(tagClose(tag2), tagClose(tag1));
                xml = xml.replaceAll(tagOpen(tag2), tagOpen(tag1));
                
            }
            
        }
        
                
        return xml;
    } 

    /**
     * 
     * @param tag
     * @return
     */
    private static String tagClose(String tag) {
        StringBuilder sb = new StringBuilder('<');
        sb.append(tag).append('>');
        return sb.toString();
    }
    
    /**
     * 
     * @param tag
     * @return
     */
    private static String tagOpen(String tag) {
        StringBuilder sb = new StringBuilder("</");
        sb.append(tag).append('>');
        return sb.toString();
    }
    
    /**
     * 
     * @param tag
     * @return
     */
    private static String tagSingle(String tag) {
        StringBuilder sb = new StringBuilder('<');
        sb.append(tag).append("/>");
        return sb.toString();
    }  


    public static Note createNote(String text, String contentType,
			TimeZone deviceTimeZone, String deviceCharset) throws ConverterException {
    	if (CONTENT_TYPE_SIFN.equals(contentType))
    	{
    		TimeZone.setDefault(deviceTimeZone);
            ByteArrayInputStream buffer = null;
            SIFNParser sifnParser = null;
            Note note = null;
            try
            {
            	byte[] bytes = text.getBytes();
            	buffer = new ByteArrayInputStream(bytes);
            	if (bytes.length > 0) {
            		sifnParser = new SIFNParser(buffer);
                    note =  sifnParser.parse();
                    return note;
                }	
            }
            catch (Exception e)
            {
            	throw new ConverterException("cant convert note to : " + contentType);
            }
            
    	} 
    	else if (CONTENT_TYPE_PLAIN_TEXT.equals(contentType))
    	{
    		Note plainNote = new Note();
    		Property descr = plainNote.getTextDescription();
    		descr.setPropertyValue(text);
    		plainNote.setTextDescription(descr);
    		return plainNote;
    	}
    	throw new ConverterException("cant convert note to : " + contentType);
    }


    public static Task createTask(String text, String contentType,
			TimeZone deviceTimeZone, String deviceCharset) throws ConverterException {
    	//throw new UnsupportedOperationException("unimplemented");
		TimeZone.setDefault(deviceTimeZone);
        ByteArrayInputStream buffer = null;
        SIFCalendarParser sifCParser = null;
        Task realTask;
        Calendar task;
    	if (CONTENT_TYPE_SIF_TASK.equals(contentType))
    	{

            try {
                task = new Calendar();
                byte[] bytes = text.getBytes();
                buffer = new ByteArrayInputStream(bytes);
                if (bytes.length > 0) {
                    sifCParser = new SIFCalendarParser(buffer);
                    task = (Calendar) sifCParser.parse();
                    // Need to get the Task out of the Calendar
                    realTask = task.getTask();
                    String creationDate = task.getCalendarContent().getCreated().getPropertyValueAsString();
                    PropertyWithTimeZone tz = task.getCalendarContent().getCreated();
                    realTask.setCreated(tz);
                    return realTask;
                }
            } catch (Exception e){
                throw new ConverterException("Error converting SIF-T to Calendar/Task. ", e);
            }
    	}
    	else 
    	{ 
    		if (CONTENT_TYPE_VCAL2.equals(contentType)) 
    		{
    			
    		task = new Calendar();

                byte[] bytes = text.getBytes();
                buffer = new ByteArrayInputStream(bytes);
                
                VCalendar vcalendar = new VCalendar();
            	XVCalendarSyntaxParserListener lis = new XVCalendarSyntaxParserListener(vcalendar);
    			XVCalendarSyntaxParser XVParser   = new XVCalendarSyntaxParser(buffer);
                XVParser.setListener(lis);
                
                try {
					XVParser.parse();
				} catch (org.evolvis.bsi.funambol.convert.vcal.ParseException e1) {
					e1.printStackTrace();
				}
                
    			try
    			{
    			task = getCalendarFromVCalendar(vcalendar, "1.0", deviceTimeZone,
    					deviceCharset, "task");
    			realTask = task.getTask();
    			return realTask;
    			}
    			catch (Exception e)
    			{
    				throw new ConverterException("Error converting vCalendar to Calendar/Task. ", e);
    			}
    		}
    	}
    	
    	throw new ConverterException("unknown content type: " + contentType);
    }

    private static final String DEFAULT_CHARSET =
        new OutputStreamWriter(new ByteArrayOutputStream()).getEncoding();
    
    // Vcard in "String text". This method parses the string to a vcard object
    @SuppressWarnings("unused")
    public static Contact createContact(String text, String contentType,
			TimeZone deviceTimeZone, String deviceCharset) throws ConverterException {
    	if (CONTENT_TYPE_VCARD.equals(contentType)) {

            ByteArrayInputStream buffer = null;
            try {
                byte[] bytes = text.getBytes();
                buffer = new ByteArrayInputStream(bytes);
                if (bytes.length > 0) {
                	Contact contact = new Contact();
                    VCardSyntaxParser parser = new VCardSyntaxParser(buffer);
                    VCardSyntaxParserListener lis = new VCardSyntaxParserListener(contact, null, DEFAULT_CHARSET);
                    parser.setListener(lis);
                    parser.parse();
                    return contact;
                }
            } catch (Exception e){
                throw new ConverterException("Error converting VCARD to Contact. ", e);
            }
    	} else if (CONTENT_TYPE_SIF_CONTACT.equals(contentType)) {
            ByteArrayInputStream buffer = null;
            SIFCParser parser = null;
            Contact contact = null;
            try {
                contact = new Contact();
                byte[] bytes = text.getBytes();
                buffer = new ByteArrayInputStream(bytes);
                if (bytes.length > 0) {
                    parser = new SIFCParser(buffer);
                    return (Contact) parser.parse();
                }
            } catch (Exception e){
                throw new ConverterException("Error converting SIF-C to Contact. ", e);
            }
    	}
		throw new ConverterException("unknown content type: " + contentType);
	}
    
    public static Calendar createCalendar(String text, String contentType,
			TimeZone deviceTimeZone, String deviceCharset)
			throws ParseException, ConverterException,
			com.funambol.common.pim.icalendar.ParseException, SAXException,
			IOException {	

		Calendar c = null;
		ByteArrayInputStream buffer = new ByteArrayInputStream(text.getBytes());

		if (CONTENT_TYPE_VCAL2.equals(contentType)) { // VCAL_FORMAT
			//XVCalendarParser parser = new XVCalendarParser(buffer);
			//XVCalendarSyntaxParser parser = new XVCalendarSyntaxParser(buffer);
			//VCalendar vcalendar = (VCalendar) parser.XVCalendar();
			//VCalendar vcalendar = (VCalendar) parser.X;
			//XVCalendarSyntaxParserListener lis = new XVCalendarSyntaxParserListener();
			
			VCalendar vcalendar = new VCalendar();
			c = new Calendar();
			XVCalendarSyntaxParserListener lis = new XVCalendarSyntaxParserListener(vcalendar);
			XVCalendarSyntaxParser XVParser   = new XVCalendarSyntaxParser(buffer);
		    XVParser.setListener(lis);	
				try {
					XVParser.parse();
				} catch (org.evolvis.bsi.funambol.convert.vcal.ParseException e) {
					ConvertException.getExceptionInformation("", e);
					e.printStackTrace();
				}
			
			c = (Calendar) getCalendarFromVCalendar(vcalendar, "1.0", deviceTimeZone,
			deviceCharset, "cal");
			
		} else if (CONTENT_TYPE_ICAL.equals(contentType)) { // ICAL_FORMAT
			ICalendarParser parser = new ICalendarParser(buffer);
			VCalendar vcalendar = (VCalendar) parser.ICalendar();
			c = getCalendarFromVCalendar(vcalendar, "2.0", deviceTimeZone,
					deviceCharset, "cal");
		} else if (CONTENT_TYPE_SIF_EVENT.equals(contentType)) {
			if ((text.getBytes()).length > 0) {
				SIFCalendarParser parser = new SIFCalendarParser(buffer);
				c = parser.parse();
			}
		} else
			throw new ConverterException("unknown content type: " + contentType);

		if (log.isTraceEnabled())
			log.trace("Conversion done.");

		return c;
	}
    
    @SuppressWarnings("deprecation")
	private static final Calendar getCalendarFromVCalendar(VCalendar vcalendar,
    		String version, TimeZone deviceTimeZone, String deviceCharset, String taskOrCal) throws ConverterException {

        String retrievedVersion = null;
        if (vcalendar.getProperty("VERSION") != null) {
            retrievedVersion = vcalendar.getProperty("VERSION").getValue();
        }
        vcalendar.addProperty("VERSION", version);
        if (retrievedVersion == null) {
            if (log.isTraceEnabled()) {
                log.trace("No version property was found in the vCal/iCal "
                        + "data: version set to " + version);
            }
        } else if (!retrievedVersion.equals(version)) {
            if (log.isTraceEnabled()) {
                log.trace("The version in the data was "
                        + retrievedVersion
                        + " but it's been changed to "
                        + version);
            }
        }

        VCalendarConverter vcf = new VCalendarConverter(deviceTimeZone, deviceCharset);
        
        return vcf.vcalendar2calendar(vcalendar, taskOrCal);

    }
    
    public static String getClientTimeZone(String strDate) {
    	return strDate;
    }
  
    
//    public static void addWsSecurityHeader(KolabServicePortType client,
//			String user, final String password) {
//        Client client_ = ClientProxy.getClient(client);
//        Endpoint cxfEndpoint = client_.getEndpoint(); 
//
//        Map<String,Object> outProps = new HashMap<String,Object>();
//        
//        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
//        cxfEndpoint.getOutInterceptors().add(wssOut);
////        cxfEndpoint.getOutInterceptors().add(new SAAJOutInterceptor()); // 2.0.x only; not needed in 2.1+
//
//        outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
//    
//        outProps.put(WSHandlerConstants.USER, user);
//        
//        outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);          
//
//        outProps.put(WSHandlerConstants.PW_CALLBACK_REF, new CallbackHandler() {
//			public void handle(Callback[] callbacks)
//					throws IOException, UnsupportedCallbackException {
//		        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
//		        pc.setPassword(password);
//			}
//        });
//	}

	private static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
	
    public static final SecurityHeaderType createHeader(String user, String password) {
		AttributedString userstr = new AttributedString();
		userstr.setValue(user);

		PasswordString ps = new PasswordString();
		ps.setValue(password);
		
		UsernameTokenType utt = new UsernameTokenType();
		utt.setUsername(userstr);
		utt.getAny().add(OBJECT_FACTORY.createPassword(ps));

		SecurityHeaderType header = new SecurityHeaderType();
		header.getAny().add(OBJECT_FACTORY.createUsernameToken(utt));
		return header;
    }
    
    /**
     * Credentials are stored in the funambol db by using base64 encoding on "user:pass".
     * This operation can be used to extract the password from the encoded
     * credentials.
     * 
     * @param  username
     * @param  encodedCredentials
     * @return
     */
    public static final String getPassword(String username, String encodedCredentials) {
    	byte[] decoded = Base64.decode(encodedCredentials);
    	int prefixLength = username.length()+1;
    	return new String(decoded, prefixLength, decoded.length - prefixLength);
    }
	
	public static String readVersion() throws XMLStreamException {
		InputStream pomIn = Helper.class.getResourceAsStream("/META-INF/maven/org.evolvis.bsi/kolab-ws-connector/pom.xml");
		if (pomIn == null)
			return null;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(pomIn);
		for (int level = 0; parser.hasNext(); parser.next()) {
			int eventType = parser.getEventType();
			if (XMLStreamConstants.START_ELEMENT == eventType) {
				level ++;
				String name = parser.getLocalName();
				if (level == 2 && "version".equals(name)) {
					String version = parser.getElementText();
					return version;
				}
			} else if (XMLStreamConstants.END_ELEMENT== eventType)
				level --;
		}
		return null;
	}

	public static String getKolabWsUrl(String host, String port) {
		StringBuilder sb = new StringBuilder();
		sb.append("http://").append(host).append(':').append(port)
			.append("/kolab-ws/KolabServicePortTypeImpl");
		return sb.toString();
	}
	
	public static String getKolabWsStatusUrl(String host, String port) {
		StringBuilder sb = new StringBuilder();
		sb.append("http://").append(host).append(':').append(port)
			.append("/kolab-ws/KolabServiceStatusPortTypeImpl");
		return sb.toString();
	}
	
}
