/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.evolvis.bsi.funambol.source.Helper.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.source.Helper.KolabId;
import org.evolvis.bsi.funambol.source.Helper.KolabType;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.KolabServicePortType;
import org.evolvis.bsi.kolab.service.SimpleKolabServicePortType;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.SecurityHeaderType;

import com.funambol.framework.engine.SyncItem;
import com.funambol.framework.engine.SyncItemImpl;
import com.funambol.framework.engine.SyncItemKey;
import com.funambol.framework.engine.SyncItemState;
import com.funambol.framework.engine.source.AbstractSyncSource;
import com.funambol.framework.engine.source.SyncContext;
import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.engine.source.SyncSourceException;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.filter.FilterClause;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.server.Sync4jDevice;
import com.funambol.framework.tools.beans.LazyInitBean;

/**
 * @author Hendrik Helwich
 *
 */
public abstract class AbstractKolabSyncSource extends AbstractSyncSource
    implements SyncSource, LazyInitBean {

    // --------------------------------------------------------------- Constants
	
	// Taken from the 
	// externals/jme-sdk/syncml/src/com/funambol/syncml/protocol/SyncMLStatus.java
	// file in the java micro edition funambol development kit
	  public static final int SUCCESS                     = 200 ;
	   public static final int AUTHENTICATION_ACCEPTED     = 212 ;
	   public static final int CHUNKED_ITEM_ACCEPTED       = 213 ;
	   public static final int INVALID_CREDENTIALS         = 401 ;
	   public static final int FORBIDDEN                   = 403 ;
	   public static final int NOT_FOUND                   = 404 ;
	   public static final int GENERIC_ERROR               = 500 ;
	   public static final int SERVER_BUSY                 = 503 ;
	   public static final int PROCESSING_ERROR            = 506 ;
	   public static final int REFRESH_REQUIRED            = 508 ;
	   public static final int BACKEND_AUTH_ERROR          = 511 ;

    protected SyncContext context;
    
    public static final String ATTACHMENT_PHOTO_ID = "photo.attachment";
    
    public static final long MAXIMUM_TIME_DIFFERENCE = 1000 * 10; // ten seconds dif allowed
    
    private SimpleKolabServicePortType client;

    private String deviceId;

    private TimeZone deviceTimezone;

    private String deviceCharset;
    
    private Timestamp syncTs;
    
    // ------------------------------------------------------------ Constructors

    public AbstractKolabSyncSource() {
    }

    // ---------------------------------------------------------- Public Methods

    /**
     * Invoked after class instantiation when a server bean is loaded.
     */
    public void init() {
        //
        // When a bean is created, first of all the empty constructor is called
        // and the the bean properties are set. If a bean requires that its 
        // properties have a proper value at initialization time, it must 
        // implement com.funambol.framework.tools.beans.LazyInitBean, so that 
        // the server beans factory has the opportunity to initialize the bean 
        // after its creation, but before using it.
    }
    
    /**
     * Called before any other synchronization method. To interrupt the sync
     * process, throw a SyncSourceException.
     *
     * @param syncContext the context of the sync.
     *
     * @see SyncContext
     *
     * @throws SyncSourceException to interrupt the process with an error
     */
    public void beginSync(SyncContext syncContext) throws SyncSourceException {
        log.info("Starting synchronization: " + syncContext.getPrincipal());
    	int syncMode = syncContext.getSyncMode();
    	// 200 TWO_WAY
    	// 201 SLOW
    	// 202 ONE_WAY_FROM_CLIENT
    	// 203 REFRESH_FROM_CLIENT
    	// 204 ONE_WAY_FROM_SERVER
    	// 205 REFRESH_FROM_SERVER
    	//
    	Timestamp since = syncContext.getSince(); // null if slow sync mode
    	syncTs = syncContext.getTo();
    	int conflictResolution = syncContext.getConflictResolution();
    	String sourceQuery = syncContext.getSourceQuery();
    	FilterClause filterClause = syncContext.getFilterClause();
    	Sync4jPrincipal principal = syncContext.getPrincipal();
    	
    	deviceId = principal.getDeviceId();
    	String username = principal.getUsername();
    	String credentials = principal.getEncodedCredentials();
    	String password = getPassword(username, credentials);

        Sync4jDevice device = principal.getDevice();
        String _timezone  = device.getTimeZone();

        if (_timezone != null && _timezone.length() > 0) {
        	deviceTimezone = TimeZone.getTimeZone(_timezone);
        } else
        	deviceTimezone = null;

        deviceCharset = device.getCharset();

		String kolabWsEndpointUrl = Helper.getKolabWsUrl(kolabWsHost, kolabWsPort);
		KolabServicePortType client_ = getServiceCient(kolabWsEndpointUrl);
		SecurityHeaderType header = createHeader(username, password);
		client = new SimpleKolabServicePortType(client_, deviceId, header);
		
		long time1 = System.currentTimeMillis();
		Date kwstime = client.getCurrentTime();
		long time2 = System.currentTimeMillis();
		long kwst = kwstime.getTime();
		long dif = Math.abs(kwst << 1 - (time1 + time2)); // time difference multiplicated with 2
		if (dif > MAXIMUM_TIME_DIFFERENCE << 1) // maximum time difference exceeded ?
			;
		long l1 = getSyncTime().getTime();
		long l3 = System.currentTimeMillis();
    }
    
    protected Date getSyncTime() {
		return syncTs;
	}

	/**
     * Called after the modifications have been applied.
     *
     * @throws SyncSourceException to interrupt the process with an error
     */
    public void endSync() throws SyncSourceException {
        log.info("Ending synchronization");
    }

    /**
     * Commits the changes applied during the sync session. If the underlying
     * datastore can not commit the changes, a SyncSourceException is thrown.
     *
     * @throws SyncSourceException if the changes cannot be committed
     */
    public void commitSync() throws SyncSourceException {
        log.info("Committing synchronization");
    }
    
    /**
     * Called to get the keys of all items accordingly with the parameters
     * used in the beginSync call.
     * @return an array of all <code>SyncItemKey</code>s stored in this source.
     *         If there are no items an empty array is returned.
     *
     * @throws SyncSourceException in case of error (for instance if the
     *         underlying data store runs into problems)
     */
    public SyncItemKey[] getAllSyncItemKeys()
    throws SyncSourceException {
        log.info("getAllSyncItemKeys()");
				getFolders();
			SyncItemKeyList fids = new SyncItemKeyList();
			for (KolabType key : folders.keySet())
				for (Folder folder : folders.get(key)) {
			        log.info("get updated item ids from folder "+folder.getName());
			        List<String> ids;
					try {
		        		ids = getAllItemIDs(folder, key, getSyncTime());
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						throw new SyncSourceException("Cant retrieve all Item IDS from server.", GENERIC_ERROR);
					}
			        fids.addAll(folder.getName(), ids, key);
		        }
	        return fids.toArray();
    }
    
    
   /**
     * Called to get the keys of the items updated in the time frame sinceTs - untilTs.
     * <br><code>sinceTs</code> null means all keys of the items updated until <code>untilTs</code>.
     * <br><code>untilTs</code> null means all keys of the items updated since <code>sinceTs</code>.
     *
     * @param sinceTs consider the changes since this point in time.
     * @param untilTs consider the changes until this point in time.
     *
     * @return an array of keys containing the <code>SyncItemKey</code>'s key of the updated
     *         items in the given time frame. It MUST NOT return null for
     *         no keys, but instad an empty array.
     */
    public SyncItemKey[] getUpdatedSyncItemKeys(Timestamp sinceTs ,
                                                Timestamp untilTs )
    throws SyncSourceException {
        log.info("getUpdatedSyncItemKeys()");
			getFolders();
			SyncItemKeyList fids = new SyncItemKeyList();
			for (KolabType key : folders.keySet())
				for (Folder folder : folders.get(key)) {
			        log.info("get updated item ids from folder "+folder.getName());
		        	List<String> ids;
					try {
						ids = getUpdatedItemIDs(folder, key, sinceTs, untilTs);
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						throw new SyncSourceException("Cannot retrieve all updated item ids from server.", GENERIC_ERROR);
					}
			        fids.addAll(folder.getName(), ids, key);
		        }
	        return fids.toArray();
    }
    

    /**
     * Called to get the keys of the items deleted in the time frame sinceTs - untilTs.
     * <br><code>sinceTs</code> null means all keys of the items deleted until <code>untilTs</code>.
     * <br><code>untilTs</code> null means all keys of the items deleted since <code>sinceTs</code>.
     *
     * @param sinceTs consider the changes since this point in time.
     * @param untilTs consider the changes until this point in time.
     *
     * @return an array of keys containing the <code>SyncItemKey</code>'s key of the deleted
     *         items in the given time frame. It MUST NOT return null for
     *         no keys, but instad an empty array.
     */
    public SyncItemKey[] getDeletedSyncItemKeys(Timestamp sinceTs ,
                                                Timestamp untilTs )
    throws SyncSourceException {
        log.info("getDeletedSyncItemKeys()");
			getFolders();
			SyncItemKeyList fids = new SyncItemKeyList();
			for (KolabType key : folders.keySet())
				for (Folder folder : folders.get(key)) {
			        log.info("get deleted item ids from folder "+folder.getName());
		        	List<String> ids;
					try {
						ids = getDeletedItemIDs(folder, key, sinceTs, untilTs);
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						throw new SyncSourceException("Cannot retrieve all deleted item IDs from server.", GENERIC_ERROR);
					}
			        fids.addAll(folder.getName(), ids, key);
		        }
	        return fids.toArray();
    }
    

    /**
     * Called to get the keys of the items created in the time frame sinceTs - untilTs.
     * <br><code>sinceTs</code> null means all keys of the items created until <code>untilTs</code>.
     * <br><code>untilTs</code> null means all keys of the items created since <code>sinceTs</code>.
     *
     * @param sinceTs consider the changes since this point in time.
     * @param untilTs consider the changes until this point in time.
     *
     * @return an array of keys containing the <code>SyncItemKey</code>'s key of the created
     *         items in the given time frame. It MUST NOT return null for
     *         no keys, but instad an empty array.
     */
    public SyncItemKey[] getNewSyncItemKeys(Timestamp sinceTs ,
                                            Timestamp untilTs )
    throws SyncSourceException {
        //
        // Put here your code to retrieve the keys for the new items
        //
        log.info("getNewSyncItemKeys()");
			getFolders();
			SyncItemKeyList fids = new SyncItemKeyList();
			for (KolabType key : folders.keySet())
				for (Folder folder : folders.get(key)) {
			        log.info("get new item ids from folder "+folder.getName());
		        	List<String> ids;
					try {
						ids = getNewItemIDs(folder, key, sinceTs, untilTs);
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						throw new SyncSourceException("Cannot retrieve all new item IDs from server.", GENERIC_ERROR);
					}
			        fids.addAll(folder.getName(), ids, key);
		        }
	        return fids.toArray();
    }
    
    
    private Map<KolabType, List<Folder>> folders;
    
    private void getFolders() throws SyncSourceException {
    	if (folders == null) {
            log.info("get all folders");
			try {
				folders = getItemFoldersWT();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw new SyncSourceException("Error retrieving the item folders.", GENERIC_ERROR);
			}
    	}
    }
    
    
    private static class SyncItemKeyList {

		private List<String> fids = new LinkedList<String>();
		
		public void addAll(String folderName, List<String> ids, KolabType type) {
        	for (String id : ids)
        		fids.add(convertToFunambolId(folderName, id, type));
		}
		
    	public SyncItemKey[] toArray() {
    		return toSynItemKeys(fids);
    	}
    	
    }


    /**
     * Adds a new <code>SyncItem</code>.
     * The item is also returned giving the opportunity to the
     * source to modify its content and return the updated item (i.e. updating
     * the id to the GUID).
     *
     * @param syncInstance  the item to add
     *
     * @return the inserted item
     *
     * @throws SyncSourceException in case of error (for instance if the
     *         underlying data store runs into problems)
     */
    public SyncItem addSyncItem(SyncItem syncItem)
    throws SyncSourceException {
        //
        // Put here your code to add a new item in the data store. Remember
        // to return the item with the new global id (GUID). 
        //
        log.info("addSyncItem()");
			getFolders();

        String content = new String(syncItem.getContent());
        String type = syncItem.getType();
        Timestamp ts = syncItem.getTimestamp();
        
        KolabType ktype = getType(content, type);
		Folder folder = folders.get(ktype).get(0); // default event folder
		
        String key;
		try {
        	key = addItem(folder, ktype, content, type);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new SyncSourceException("Error adding the item", GENERIC_ERROR);
		}
        
		String fkey = convertToFunambolId(folder.getName(), key, ktype);

        SyncItemImpl newSyncItem = new SyncItemImpl(
                this                  , //syncSource
                fkey                   , //key
                null                  , //mappedKey
                SyncItemState.NEW     , //state
                content.getBytes()    , //content
                null                  , //format
                type           , //type
                ts                      //timestamp
                );

        return newSyncItem;
    }

    
    
    

    /**
     * Update a <code>SyncItem</code>.
     * The item is also returned giving the opportunity to the
     * source to modify its content and return the updated item (i.e. updating
     * the id to the GUID).
     *
     * @param syncInstance the item to replace
     *
     * @return the updated item
     *
     * @throws SyncSourceException in case of error (for instance if the
     *         underlying data store runs into problems)
     */
    public SyncItem updateSyncItem(SyncItem syncItem)
       throws SyncSourceException {
    	
        //
        // Put here your code to update an item in the data store.
        //
        log.info("updateSyncItem()");
        
        

			getFolders();

        String content = new String(syncItem.getContent());
        String type = syncItem.getType();
        Timestamp ts = syncItem.getTimestamp();
        String key = syncItem.getKey().getKeyAsString();
        

		KolabId kid = convertToKolabId(key);
		 boolean done ;
		try {
        done = updateItem(kid.getFolder(), kid.getType(), kid.getGuid(), ts, content, type);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new SyncSourceException("error updating item with sync item key " + syncItem.getKey(), GENERIC_ERROR);
		}
        SyncItemImpl newSyncItem = null;
        
        if (done)
        	newSyncItem = new SyncItemImpl(
                this                  , //syncSource
                key                   , //key
                null                  , //mappedKey
                SyncItemState.UPDATED , //state
                content.getBytes()    , //content
                null                  , //format
                type                  , //type
                null                    //timestamp
                );

        return syncItem;
    }

    

			
			
    /**
     * Removes a SyncItem given its key.
     *
     * @param itemKey the key of the item to remove
     * @param time the time of the deletion
     * @param softDelete is a soft delete ?
     *
     * @throws SyncSourceException in case of error (for instance if the
     *         underlying data store runs into problems)
     */
    public void removeSyncItem(SyncItemKey itemKey, Timestamp time, boolean softDelete)
    throws SyncSourceException {
        log.info("removeSyncItem()");
        
    	String key = (String) itemKey.getKeyValue();
    	
    	KolabId kid = convertToKolabId(key);
		boolean done = false;
    	try {
			done = removeItem(kid.getFolder(), kid.getType(), kid.getGuid(), time);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new SyncSourceException("Error removing the item with key " + itemKey.getKeyAsString(), GENERIC_ERROR);
		}
    }

    /**
     * Called to get the item with the given key.
     *
     * @return return the <code>SyncItem</code> corresponding to the given
     *         key. If no item is found, null is returned.
     *
     * @param syncItemKey the key of the SyncItem to return
     *
     * @throws SyncSourceException in case of errors (for instance if the
     *         underlying data store runs into problems)
     */
    public SyncItem getSyncItemFromId(SyncItemKey syncItemKey)
    		throws SyncSourceException {
    	String fid = syncItemKey.getKeyAsString();
    	KolabId kid = convertToKolabId(fid);
    	log.info("read item from folder "+kid.getFolder().getName()+" with id "+kid.getGuid());
//    	try {
	        String contentType = getInfo().getPreferredType().getType();
	        SyncSourceInfo info = getInfo();
	        String content;
			try {
				content = getItem(kid.getFolder(), kid.getType(), kid.getGuid(), contentType);
				if (content == null)
					return null;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw new SyncSourceException("Error retrieving the item with key " +  syncItemKey.getKeyAsString(), GENERIC_ERROR);
			}
            
			SyncItem syncItem = new SyncItemImpl(this, fid);
			syncItem.setType(contentType);
			syncItem.setContent(content.getBytes());
			return syncItem;
//		} catch (KolabServiceFault_Exception e) {
//			throw new SyncSourceException(e);
//		} catch (ConversionException e) {
//			throw new SyncSourceException(e);
//		} catch (ConverterException e) {
//			throw new SyncSourceException(e);
//		}
    }

    
    /**
     * Called to retrive the keys of the twins of the given item
     *
     * @param syncItem the twin item
     *
     * @return the keys of the twin. Each source implementation is free to
     *         interpret this as it likes (i.e.: comparing all fields).
     *
     *
     * @throws SyncSourceException in case of errors (for instance if the
     *         underlying data store runs into problems)
     */
    public SyncItemKey[] getSyncItemKeysFromTwin(SyncItem syncItem) 
    throws SyncSourceException {
        log.info("getSyncItemKeysFromTwin()");
        String content = new String(syncItem.getContent());
        return new SyncItemKey[0];
    }


    /**
     * Called by the engine to notify an operation status.
     * @param operationName the name of the operation.
     *        One between:
     *        - Add
     *        - Replace
     *        - Delete
     * @param status the status of the operation
     * @param keys the keys of the items
     */
    public void setOperationStatus(String operationName, int status, SyncItemKey[] keys) {
        log.info("setOperationStatus()");
    }

    protected String kolabWsHost;
    protected String kolabWsPort;
    
    public String getKolabWsHost() {
		return kolabWsHost;
	}

    public void setKolabWsHost(String kolabWsHost) {
		this.kolabWsHost = kolabWsHost;
	}

    public String getKolabWsPort() {
		return kolabWsPort;
	}

    public void setKolabWsPort(String kolabWsPort) {
		this.kolabWsPort = kolabWsPort;
	}

    
	protected SimpleKolabServicePortType getClient() {
    	return client;
    }
    
    protected TimeZone getTimezone() {
    	return deviceTimezone;
    }
    
    protected String getCharset() {
    	return deviceCharset;
    }
    
    abstract protected Map<KolabType, List<Folder>> getItemFoldersWT() throws Exception;

    abstract protected List<String> getAllItemIDs(Folder folder, KolabType type, Date updateTime) throws Exception;
    
    abstract protected List<String> getUpdatedItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime) throws Exception;
    
    abstract protected List<String> getDeletedItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime) throws Exception;
    
    abstract protected List<String> getNewItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime) throws Exception;
    
    abstract protected String addItem(Folder folder, KolabType type, String content, String contentType) throws Exception;
    
    abstract protected boolean updateItem(Folder folder, KolabType type, String id, Date lastUpdateTime, String content, String contentType) throws Exception; 
	
    abstract protected boolean removeItem(Folder folder, KolabType type, String id, Date lastUpdateTime) throws Exception;

    abstract protected String getItem(Folder folder, KolabType type, String id, String contentType) throws Exception;

    abstract protected KolabType getType(String content, String type);

}
