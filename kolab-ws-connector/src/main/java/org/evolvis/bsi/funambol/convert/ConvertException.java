/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;

/**
 * @author Hendrik Helwich
 *
 */
public class ConvertException extends Exception {
	
	public static final String LOG_NAME = "Converter";
	private static final FunambolLogger log = FunambolLoggerFactory.getLogger(LOG_NAME);

	public ConvertException(String message) {
		super(message);
	}

	public ConvertException(String message, Throwable cause) {
		super(message, cause);
	}

	public static void getExceptionInformation(String strExceptionMessage, Exception e) {
		// TODO Auto-generated method stub
		log.info("--------------------------------------------------------------------------------");
		log.info("Message: " + strExceptionMessage + " " + e.getMessage() +"\n");
		log.info("Localized Message: " + strExceptionMessage + e.getLocalizedMessage() + "\n");
		log.info("StackTrace to String: " + strExceptionMessage + e.getStackTrace().toString() + "\n");
		log.info("Cause: " + strExceptionMessage + e.getCause() + "\n");
		log.info("--------------------------------------------------------------------------------");
	}
}
