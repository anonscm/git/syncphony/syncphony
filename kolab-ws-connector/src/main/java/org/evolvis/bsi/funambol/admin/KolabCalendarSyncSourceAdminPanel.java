/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.admin;

import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_ICAL;
import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_SIF_EVENT;
import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_SIF_TASK;
import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_VCAL2;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_ICAL;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_SIFE;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_SIFT;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_VCAL;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import org.evolvis.bsi.funambol.source.AbstractKolabSyncSource;
import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.funambol.source.KolabCalendarSyncSource;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.engine.source.SyncSourceInfo;

public class KolabCalendarSyncSourceAdminPanel extends AbstractKolabSyncSourceAdminPanel<KolabCalendarSyncSource> {

    private JLabel           eventLabel;
    private JCheckBox        eventCheckbox;
    
    private JLabel           taskLabel;
    private JCheckBox        taskCheckbox;
    

    protected int init(int y) {
        eventLabel          = new JLabel()     ;
        eventCheckbox       = new JCheckBox() ;
        
        taskLabel          = new JLabel()     ;
        taskCheckbox       = new JCheckBox() ;
        
        
        y += dy;

        eventLabel.setText("Event: ");
        eventLabel.setFont(defaultFont);
        eventLabel.setBounds(new Rectangle(14, y, 150, 18));
        eventCheckbox.setFont(defaultFont);
        eventCheckbox.setSelected(true);
        eventCheckbox.setBounds(new Rectangle(170, y, 350, 18));
        
        y += dy;

        taskLabel.setText("Task: ");
        taskLabel.setFont(defaultFont);
        taskLabel.setBounds(new Rectangle(14, y, 150, 18));
        taskCheckbox.setFont(defaultFont);
        taskCheckbox.setSelected(true);
        taskCheckbox.setBounds(new Rectangle(170, y, 350, 18));

        this.add(eventLabel , null);
        this.add(eventCheckbox , null);
        this.add(taskLabel , null);
        this.add(taskCheckbox , null);
        
        
        return y;
	}
	
	@Override
	protected ContentType[] getContentTypes() {
        return new ContentType[] {
                new ContentType(Helper.CONTENT_TYPE_VCAL2, Helper.VERSION_VCAL),
                new ContentType(Helper.CONTENT_TYPE_ICAL, Helper.VERSION_ICAL),
                new ContentType(Helper.CONTENT_TYPE_SIF_EVENT, Helper.VERSION_SIFE),
                new ContentType(Helper.CONTENT_TYPE_SIF_TASK, Helper.VERSION_SIFT)
        };
	}


	@Override
    protected List getTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(Helper.CONTENT_TYPE_VCAL2);
        supportedTypes.add(Helper.CONTENT_TYPE_ICAL);
        supportedTypes.add(Helper.CONTENT_TYPE_SIF_EVENT);
        supportedTypes.add(Helper.CONTENT_TYPE_SIF_TASK);
        return supportedTypes;
    }

    @Override
    protected void getValues() {
		super.getValues();
		syncSource.setSyncEvent(eventCheckbox.isSelected());
		syncSource.setSyncTask(taskCheckbox.isSelected());
	}
    
    

	@Override
	public void updateForm() {
		super.updateForm();
		eventCheckbox.setSelected(syncSource.isSyncEvent());
		taskCheckbox.setSelected(syncSource.isSyncTask());
	}

	/**
     * Sets the source info of the given syncsource based on the given selectedType
     */
    public void setSyncSourceInfo(SyncSource syncSource, String selectedType) {
        AbstractKolabSyncSource pimSource = (AbstractKolabSyncSource) syncSource;
        ContentType[] contentTypes = null;
        if (CONTENT_TYPE_VCAL2.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_VCAL2, VERSION_VCAL);
        } else if (CONTENT_TYPE_ICAL.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_ICAL, VERSION_ICAL);
        } else if (CONTENT_TYPE_SIF_EVENT.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_SIF_EVENT, VERSION_SIFE);
        } else if (CONTENT_TYPE_SIF_TASK.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_SIF_TASK, VERSION_SIFT);
        }
        
        pimSource.setInfo(new SyncSourceInfo(contentTypes, 0));
    }
    
    @Override
    protected String getTypeToSelect(SyncSource syncSource) {
    	System.out.println("getTypeToSelect");
        String preferredType = null;
        if (syncSource.getInfo() != null &&
            syncSource.getInfo().getPreferredType() != null) {
        	
            preferredType = syncSource.getInfo().getPreferredType().getType();
            System.out.println(preferredType);
            if (CONTENT_TYPE_VCAL2.equals(preferredType)) {
                return CONTENT_TYPE_VCAL2;
            }
            if (CONTENT_TYPE_ICAL.equals(preferredType)) {
                return CONTENT_TYPE_ICAL;
            }
            if (CONTENT_TYPE_SIF_EVENT.equals(preferredType)) {
                return CONTENT_TYPE_SIF_EVENT;
            }
            if (CONTENT_TYPE_SIF_TASK.equals(preferredType)) {
                return CONTENT_TYPE_SIF_TASK;
            }
        }
        return null;
    }

}
