/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.admin;

import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_ICAL;
import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_SIF_TASK;
import static org.evolvis.bsi.funambol.source.Helper.CONTENT_TYPE_VCAL2;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_ICAL;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_SIFT;
import static org.evolvis.bsi.funambol.source.Helper.VERSION_VCAL;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.bsi.funambol.source.AbstractKolabSyncSource;
import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.funambol.source.KolabContactSyncSource;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.engine.source.SyncSourceInfo;
import static org.evolvis.bsi.funambol.source.Helper.*;

public class KolabContactSyncSourceAdminPanel extends AbstractKolabSyncSourceAdminPanel<KolabContactSyncSource> {

	@Override
	protected ContentType[] getContentTypes() {
		return new ContentType[] {
                new ContentType(Helper.CONTENT_TYPE_VCARD, Helper.VERSION_VCARD),
                new ContentType(Helper.CONTENT_TYPE_SIF_CONTACT, Helper.VERSION_SIFC)
        };
	}



	@Override
    protected List getTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(Helper.CONTENT_TYPE_VCARD);
        supportedTypes.add(Helper.CONTENT_TYPE_SIF_CONTACT);
        return supportedTypes;
    }

    @Override
    protected String getTypeToSelect(SyncSource syncSource) {
    	System.out.println("getTypeToSelect");
        String preferredType = null;
        if (syncSource.getInfo() != null &&
            syncSource.getInfo().getPreferredType() != null) {
        	
            preferredType = syncSource.getInfo().getPreferredType().getType();
            System.out.println(preferredType);
            if (CONTENT_TYPE_VCARD.equals(preferredType)) {
                return CONTENT_TYPE_VCARD;
            }
            if (CONTENT_TYPE_SIF_CONTACT.equals(preferredType)) {
                return CONTENT_TYPE_SIF_CONTACT;
            }
            if (CONTENT_TYPE_SIF_TASK.equals(preferredType)) {
                return CONTENT_TYPE_SIF_TASK;
            }
        }
        return null;
    }

    /**
     * Sets the source info of the given syncsource based on the given selectedType
     */
    public void setSyncSourceInfo(SyncSource syncSource, String selectedType) {
        AbstractKolabSyncSource pimSource = (AbstractKolabSyncSource) syncSource;
        ContentType[] contentTypes = null;
        if (CONTENT_TYPE_VCARD.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_VCARD, VERSION_VCARD);
        } else if (CONTENT_TYPE_SIF_CONTACT.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_SIF_CONTACT, VERSION_SIFC);
        } 

        pimSource.setInfo(new SyncSourceInfo(contentTypes, 0));
    }
}
