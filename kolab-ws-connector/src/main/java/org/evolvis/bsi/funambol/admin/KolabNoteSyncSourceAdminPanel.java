/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.admin;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.bsi.funambol.source.AbstractKolabSyncSource;
import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.funambol.source.KolabNoteSyncSource;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;

import static org.evolvis.bsi.funambol.source.Helper.*;

public class KolabNoteSyncSourceAdminPanel extends AbstractKolabSyncSourceAdminPanel<KolabNoteSyncSource> {
    protected static final String TYPE_LABEL_SIFN  = "SIF-N";
    protected static final String TYPE_LABEL_PLAIN_TEXT = "Plain Text";

    public static final FunambolLogger log = FunambolLoggerFactory.getLogger("adminpanel");
    
	@Override
	protected ContentType[] getContentTypes() {

//		KolabNoteSyncSource k = new KolabNoteSyncSource();
//		ContentType[] rc = new ContentType[] {
//                new ContentType(Helper.CONTENT_TYPE_PLAIN_TEXT, Helper.VERSION_PLAIN_TEXT),
//                new ContentType(Helper.CONTENT_TYPE_SIFN, Helper.VERSION_SIFN)
//        };
//		k.setInfo(new SyncSourceInfo(rc, 0));
//		
//		return rc;
		return null;
	}
	

    @Override
    protected String getTypeToSelect(SyncSource syncSource) {
    	System.out.println("getTypeToSelect");
        String preferredType = null;
        if (syncSource.getInfo() != null &&
            syncSource.getInfo().getPreferredType() != null) {
        	
            preferredType = syncSource.getInfo().getPreferredType().getType();
            System.out.println(preferredType);
            if (CONTENT_TYPE_PLAIN_TEXT.equals(preferredType)) {
                return CONTENT_TYPE_PLAIN_TEXT;
            }
            if (CONTENT_TYPE_SIFN.equals(preferredType)) {
                return CONTENT_TYPE_SIFN;
            }
        }
        return null;
    }

	@Override
    protected List getTypes() {
        List supportedTypes = new ArrayList();
        supportedTypes.add(Helper.CONTENT_TYPE_PLAIN_TEXT);
        supportedTypes.add(Helper.CONTENT_TYPE_SIFN);
        return supportedTypes;
    }
	
    /**
     * Sets the source info of the given syncsource based on the given selectedType
     */
    public void setSyncSourceInfo(SyncSource syncSource, String selectedType) {
        AbstractKolabSyncSource pimSource = (AbstractKolabSyncSource) syncSource;
        ContentType[] contentTypes = null;
        if (CONTENT_TYPE_SIFN.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_SIFN, VERSION_SIFN);
        } else if (CONTENT_TYPE_PLAIN_TEXT.equals(selectedType)) {
            contentTypes = new ContentType[1];
            contentTypes[0] = new ContentType(CONTENT_TYPE_PLAIN_TEXT, VERSION_PLAIN_TEXT);
        }

        pimSource.setInfo(new SyncSourceInfo(contentTypes, 0));
    }

}
