/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.admin;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang.StringUtils;
import org.evolvis.bsi.funambol.source.AbstractKolabSyncSource;

import com.funambol.admin.AdminException;
import com.funambol.admin.ui.SourceManagementPanel;
import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.logging.FunambolLogger;
import com.funambol.framework.logging.FunambolLoggerFactory;


/**
 * This class implements the configuration panel for MySyncSource
 *
 */
abstract public class AbstractKolabSyncSourceAdminPanel<T extends AbstractKolabSyncSource>
extends SourceManagementPanel
implements Serializable {

    // --------------------------------------------------------------- Constants
    public static final FunambolLogger log = FunambolLoggerFactory.getLogger("adminpanel");

    /**
     * Allowed characters for name and uri
     */
    public static final String NAME_ALLOWED_CHARS
    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_.";

    // ------------------------------------------------------------ Private data
    /** label for the panel's name */
    private JLabel panelName = new JLabel();

    /** border to evidence the title of the panel */
    private TitledBorder  titledBorder1;

    private JLabel           nameLabel          = new JLabel()     ;
    private JTextField       nameValue          = new JTextField() ;
    private JLabel           typeLabel          = new JLabel()     ;
    private JTextField       typeValue          = new JTextField() ;
    private JLabel           versionLabel       = new JLabel()     ;
    private JTextField       versionValue       = new JTextField() ;
    private JLabel           kolabWsHostLabel   = new JLabel()     ;
    private JTextField       kolabWsHostValue   = new JTextField() ;
    private JLabel           kolabWsPortLabel   = new JLabel()     ;
    private JTextField       kolabWsPortValue   = new JTextField() ;

    private JLabel           sourceUriLabel     = new JLabel()     ;
    private JTextField       sourceUriValue     = new JTextField() ;
    
    protected JComboBox typeCombo = new JComboBox();

    private JButton          confirmButton      = new JButton()    ;

    protected T     syncSource         = null             ;

    // ------------------------------------------------------------ Constructors

    /**
     * Creates a new MySyncSourceAdminPanel instance
     */
    public AbstractKolabSyncSourceAdminPanel() {
        init();
    }

    // ----------------------------------------------------------- Private methods

    protected static final int dy = 30;
    
    /**
     * Create the panel
     * @throws Exception if error occures during creation of the panel
     */
    private void init(){
        // set layout
        this.setLayout(null);

        // set properties of label, position and border
        //  referred to the title of the panel
        titledBorder1 = new TitledBorder("");

        panelName.setFont(titlePanelFont);
        panelName.setText("Edit My SyncSource");
        panelName.setBounds(new Rectangle(14, 5, 316, 28));
        panelName.setAlignmentX(SwingConstants.CENTER);
        panelName.setBorder(titledBorder1);

        int y  = 60;

        sourceUriLabel.setText("Source URI: ");
        sourceUriLabel.setFont(defaultFont);
        sourceUriLabel.setBounds(new Rectangle(14, y, 150, 18));
        sourceUriValue.setFont(defaultFont);
        sourceUriValue.setBounds(new Rectangle(170, y, 350, 18));

//        y += dy;
//
//        nameLabel.setText("Name: ");
//        nameLabel.setFont(defaultFont);
//        nameLabel.setBounds(new Rectangle(14, y, 150, 18));
//        nameValue.setFont(defaultFont);
//        nameValue.setBounds(new Rectangle(170, y, 350, 18));
//
//        y += dy;
//
//        typeLabel.setText("Supported type: ");
//        typeLabel.setFont(defaultFont);
//        typeLabel.setBounds(new Rectangle(14, y, 150, 18));
//        typeValue.setFont(defaultFont);
//        typeValue.setBounds(new Rectangle(170, y, 350, 18));
//
//        y += dy;
//
//        versionLabel.setText("Supported version: ");
//        versionLabel.setFont(defaultFont);
//        versionLabel.setBounds(new Rectangle(14, y, 150, 18));
//        versionValue.setFont(defaultFont);
//        versionValue.setBounds(new Rectangle(170, y, 350, 18));

        y += dy;

        kolabWsHostLabel.setText("Kolab WS Host: ");
        kolabWsHostLabel.setFont(defaultFont);
        kolabWsHostLabel.setBounds(new Rectangle(14, y, 150, 18));
        kolabWsHostValue.setFont(defaultFont);
        kolabWsHostValue.setBounds(new Rectangle(170, y, 350, 18));

        y += dy;

        kolabWsPortLabel.setText("Kolab WS Port: ");
        kolabWsPortLabel.setFont(defaultFont);
        kolabWsPortLabel.setBounds(new Rectangle(14, y, 150, 18));
        kolabWsPortValue.setFont(defaultFont);
        kolabWsPortValue.setBounds(new Rectangle(170, y, 350, 18));


        y = init(y);
        

        
        y += dy;
        y += dy;

        confirmButton.setFont(defaultFont);
        confirmButton.setText("Add");
        confirmButton.setBounds(170, y, 70, 25);
        
        y += dy;
        y += dy;

        final int VALUE_X = 170;

        typeCombo.setFont(defaultFont);
        typeCombo.setBounds(new Rectangle(VALUE_X, y, 350, 18));

        confirmButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event ) {
                try {
                    validateValues();
                    validateWebservice();
                    getValues();
                    if (getState() == STATE_INSERT) {
                        AbstractKolabSyncSourceAdminPanel.this.actionPerformed(
                            new ActionEvent(AbstractKolabSyncSourceAdminPanel.this,
                                            ACTION_EVENT_INSERT,
                                            event.getActionCommand()));
                    } else {
                        AbstractKolabSyncSourceAdminPanel.this.actionPerformed(
                            new ActionEvent(AbstractKolabSyncSourceAdminPanel.this,
                                            ACTION_EVENT_UPDATE,
                                            event.getActionCommand()));
                    }
                } catch (Exception e) {
                    notifyError(new AdminException(e.getMessage()));
                }
            }
        });

        // add all components to the panel
        this.add(panelName        , null);
//        this.add(nameLabel        , null);
//        this.add(nameValue        , null);
//        this.add(typeLabel        , null);
//        this.add(typeValue        , null);
//        this.add(versionLabel     , null);
//        this.add(versionValue     , null);
        this.add(sourceUriLabel   , null);
        this.add(sourceUriValue   , null);
        this.add(kolabWsHostLabel , null);
        this.add(kolabWsHostValue , null);
        this.add(kolabWsPortLabel , null);
        this.add(kolabWsPortValue , null);
        this.add(typeCombo, null);

        this.add(confirmButton    , null);

    }

    protected int init(int y) {
    	return y;
    }

	/**
     * Loads the given syncSource showing the name, uri and type in the panel's
     * fields.
     *
     * @param syncSource the SyncSource instance
     */
    public void updateForm() {
//    	try {
//    		 Field field = getClass().getField("syncSource");
//    		 ParameterizedType type = (ParameterizedType) field.getGenericType();
//    		  Type clazz = type.getActualTypeArguments()[0];
//
//    	         if (!(getSyncSource().getClass().e) { //TODO check concrete class
//    	          notifyError(
//    	              new AdminException(
//    	                  "This is not an MySyncSource! Unable to process SyncSource values."
//    	              )
//    	          );
//    	          return;
//    	        }
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchFieldException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	

        if (getState() == STATE_INSERT) {
          confirmButton.setText("Add");
        } else if (getState() == STATE_UPDATE) {
          confirmButton.setText("Save");
        }

        this.syncSource = (T) getSyncSource();

        sourceUriValue.setText(syncSource.getSourceURI() );
        nameValue.setText     (syncSource.getName()      );
        kolabWsHostValue.setText  (syncSource.getKolabWsHost());
        kolabWsPortValue.setText  (syncSource.getKolabWsPort());

        SyncSourceInfo info = syncSource.getInfo();        
        if (info != null) {
            ContentType[] types = info.getSupportedTypes();

            StringBuffer typesList = new StringBuffer(), versionsList = new StringBuffer();
            for (int i=0; ((types != null) && (i<types.length)); ++i) {
                typesList.append(types[i].getType());
                versionsList.append(types[i].getVersion());
                //
                // Also if the SyncSourceInfo contains more contentTypes, we handle
                // just the first one
                //
                break;
            }

            typeValue.setText(typesList.toString());
            versionValue.setText(versionsList.toString());
        }

        if (this.syncSource.getSourceURI() != null) {
            sourceUriValue.setEditable(false);
        }
        
        // Preparing to populate the combo box...
        typeCombo.removeAllItems();
        List types = getTypes();
        if (types == null) {
            types = new ArrayList();
        }
        for (int i = 0; i < types.size(); i++) {
            typeCombo.addItem(types.get(i));
        }
        String typeToSelect = getTypeToSelect(syncSource);
        System.out.println("getTypeToSelect returned: " + typeToSelect);
        if (typeToSelect != null) {
            typeCombo.setSelectedItem(typeToSelect);
        } else {
            typeCombo.setSelectedIndex(0);
        }


    }

    protected abstract String getTypeToSelect(SyncSource syncSource);

    protected abstract List getTypes();

	// ----------------------------------------------------------- Private methods
    /**
     * Checks if the values provided by the user are all valid. In caso of errors,
     * a IllegalArgumentException is thrown.
     *
     * @throws IllegalArgumentException if:
     *         <ul>
     *         <li>name, uri, type or directory are empty (null or zero-length)
     *         <li>the types list length does not match the versions list length
     *         </ul>
     */
    private void validateValues() throws IllegalArgumentException {
        String value = null;

//        value = nameValue.getText().trim();
//        if (StringUtils.isEmpty(value)) {
//            throw new
//            IllegalArgumentException(
//            "Field 'Name' cannot be empty. Please provide a SyncSource name.");
//        }
//
//        if (!StringUtils.containsOnly(value, NAME_ALLOWED_CHARS.toCharArray())) {
//            throw new
//            IllegalArgumentException(
//            "Only the following characters are allowed for field 'Name': \n" + NAME_ALLOWED_CHARS);
//        }
//
//        value = typeValue.getText().trim();
//        if (StringUtils.isEmpty(value)) {
//            throw new
//            IllegalArgumentException(
//            "Field 'Supported Type' cannot be empty. Please provide a type.");
//        }
//
//        value = versionValue.getText().trim();
//        if (StringUtils.isEmpty(value)) {
//            throw new
//            IllegalArgumentException(
//            "Field 'Supported Version' cannot be empty. Please provide a version.");
//        }

        value = kolabWsHostValue.getText().trim();
        if (StringUtils.isEmpty(value)) {
            throw new
            IllegalArgumentException(
            "Field 'Kolab WS Host' cannot be empty. Please provide a valid host name.");
        }

        value = kolabWsPortValue.getText().trim();
        if (StringUtils.isEmpty(value)) {
            throw new
            IllegalArgumentException(
            "Field 'Kolab WS Port' cannot be empty. Please provide a valid port.");
        }

        try {
	        int port = Integer.parseInt(value);
        } catch(NumberFormatException e) {
            throw new
            IllegalArgumentException(
            "Field 'Kolab WS Port' is not a vlid number. Please provide a valid port.");
        }

        value = sourceUriValue.getText().trim();
        if (StringUtils.isEmpty(value)) {
            throw new
            IllegalArgumentException(
            "Field 'Source URI' cannot be empty. Please provide a SyncSource URI.");
        }
    }
    
	void validateWebservice() {
//		String host = kolabWsHostValue.getText().trim();
//        String port = kolabWsPortValue.getText().trim();
//        String url = Helper.getKolabWsStatusUrl(host, port);
//
//		KolabServiceStatus ks = new KolabStatusWebService();
//        KolabServiceStatusPortType client = ks.getKolabServiceStatusPort();
//		((BindingProvider)client).getRequestContext().put(
//				BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
//		try {
//			Status status = client.getStatus();
//			if (Status.OK == status) {
//				String versions = client.getVersion();
//				try {
//					String versionc = Helper.readVersion();
//					if (!versionc.equals(versions))
//						JOptionPane.showMessageDialog(this, "Possibly incompatible components"
//								+"\nVersion of Kolab Webservice: "+versions+
//								"\nVersion of Kolab Connector: "+versionc,
//								"Warning", JOptionPane.WARNING_MESSAGE);
//				} catch (XMLStreamException e) {
//					throw new RuntimeException(e);
//				}
//			} else if (Status.NO_ACCESS_DBMS == status) {
//				JOptionPane.showMessageDialog(this, "Webservice has no access to the database",
//						"Warning", JOptionPane.WARNING_MESSAGE);
//			} else if (Status.NO_ACCESS_IMAP == status) {
//				JOptionPane.showMessageDialog(this, "Webservice has no access to the imap server",
//						"Warning", JOptionPane.WARNING_MESSAGE);
//			} else {
//				JOptionPane.showMessageDialog(this, "Webservice has status: "+status,
//						"Warning", JOptionPane.WARNING_MESSAGE);
//			}
//		} catch (KolabServiceFault_Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

    /**
     * Set syncSource properties with the values provided by the user.
     */
    protected void getValues() {
        syncSource.setSourceURI(sourceUriValue.getText().trim());
        syncSource.setName     (sourceUriValue.getText().trim());
        syncSource.setKolabWsHost(kolabWsHostValue.getText().trim());
        syncSource.setKolabWsPort(kolabWsPortValue.getText().trim());
        
//        ContentType[] contentTypes = getContentTypes();
//        final String type = (String) this.typeCombo.getSelectedItem();
//        SyncSourceInfo info = new SyncSourceInfo(contentTypes, 0);
        setSyncSourceInfo(syncSource, (String) typeCombo.getSelectedItem());

//        syncSource.setInfo(info);
    }
    
    abstract protected ContentType[] getContentTypes();
    
    public abstract void setSyncSourceInfo(SyncSource syncSource, String selectedType);

}