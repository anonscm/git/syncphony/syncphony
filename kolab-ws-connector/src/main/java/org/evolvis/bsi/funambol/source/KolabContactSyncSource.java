/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.evolvis.bsi.funambol.source.Helper.createContact;
import static org.evolvis.bsi.funambol.source.Helper.createContent;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.source.Helper.KolabType;
import org.evolvis.bsi.kolab.service.Attachment;
import org.evolvis.bsi.kolab.service.Contact;
import org.evolvis.bsi.kolab.service.Folder;
import org.jgroups.jmx.protocols.FC;

import com.funambol.common.pim.contact.Photo;

import eu.medsea.util.MimeUtil;


/**
 * @author Hendrik Helwich
 *
 */
public class KolabContactSyncSource extends KolabSingleTypeSource {

	public KolabContactSyncSource() {
		super(KolabType.CONTACT);
	}
	
	@Override
	protected List<Folder> getItemFolders() throws Exception {
		return getClient().getContactFolders();
	}

	@Override
	protected List<String> getAllItemIDs(Folder folder, KolabType type, Date updateTime) throws Exception {
		return getClient().getContactIDs(folder, updateTime);
	}

	@Override
	protected List<String> getUpdatedItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		List<String> ret = getClient().getUpdatedContactIDs(folder, lastUpdateTime, updateTime); 
		return ret;
	}

	@Override
	protected List<String> getDeletedItemIDs(Folder folder, KolabType type,
			Date lastUpdateTime, Date updateTime) throws Exception {
		return getClient().getRemovedContactIDs(folder, lastUpdateTime, updateTime);
	}

	@Override
	protected List<String> getNewItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		return getClient().getNewContactIDs(folder, lastUpdateTime, updateTime);
	}

	@Override
	protected String addItem(Folder folder, KolabType type, String content, String contentType)
			throws Exception {
		com.funambol.common.pim.contact.Contact fcontact = createContact(content, contentType, getTimezone(),
				getCharset());
		Contact contact = Converter.convertToContact(fcontact);
		String uid = getClient().addContact(folder, contact);
		final Photo photo = fcontact.getPersonalDetail().getPhotoObject();
		if (photo != null && photo.getImage() != null)
		{
			
			final Attachment attachment = new Attachment();	
			attachment.setContent(photo.getImage());
			String prop = photo.getPropertyType();
	        String mimetype = MimeUtil.getMimeType(new ByteArrayInputStream(photo.getImage()));

			// This is a photo .. so it has to be photo.attachment
			attachment.setContentType(mimetype);
			getClient().addAttachment(folder, uid, ATTACHMENT_PHOTO_ID, attachment);
		}
		return uid;
	}

	@Override
	protected boolean updateItem(Folder folder, KolabType type, String id, Date lastUpdateTime,
			String content, String contentType) throws Exception {
		com.funambol.common.pim.contact.Contact fcontact = createContact(content, contentType, getTimezone(),
				getCharset());
		Contact contact = Converter.convertToContact(fcontact);

		contact.setUid(id);
		boolean ret = getClient().updateContact(folder, contact, lastUpdateTime);
		final Photo photo = fcontact.getPersonalDetail().getPhotoObject();
		if (photo != null && photo.getImage() != null)
		{
			// update -> remove and add
			getClient().removeAttachment(folder, id, ATTACHMENT_PHOTO_ID);
			
			// add new
			final Attachment attachment = new Attachment();	
			attachment.setContent(photo.getImage());
			String prop = photo.getPropertyType();
	        String mimetype = MimeUtil.getMimeType(new ByteArrayInputStream(photo.getImage()));

			// This is a photo .. so it has to be photo.attachment
			attachment.setContentType(mimetype);
			getClient().addAttachment(folder, id, ATTACHMENT_PHOTO_ID, attachment);
		}
		else
		{
			getClient().removeAttachment(folder, id, ATTACHMENT_PHOTO_ID);
		}
		return ret;
	}

	@Override
	protected boolean removeItem(Folder folder, KolabType type, String id, Date lastUpdateTime)
			throws Exception {
		return getClient().removeContact(folder, id, lastUpdateTime);
	}

	@Override
	protected String getItem(Folder folder, KolabType type, String id, String contentType)
			throws Exception {
		Contact contact = getClient().getContact(folder, id);
		if (contact == null)
			return null;
		final String attachmentId = contact.getPictureAttachmentId();
		byte[] pic = null;
		
		// contact has a photo?
		if (attachmentId != null)
			if (!attachmentId.equals(""))
			{
				final Attachment attachment = getClient().getAttachment(folder, id, attachmentId);
				pic = attachment.getContent();
			}
		com.funambol.common.pim.contact.Contact fcontact = Converter
				.convertContact(contact, pic);
		String content = createContent(fcontact, contentType, getTimezone(),
				getCharset());

		return content;
	}

}
