/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert;

//import static org.evolvis.bsi.funambol.source.Helper.long2utc;
//import static org.evolvis.bsi.funambol.source.Helper.utc2long;
import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_DAILY;
import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_WEEKLY;
import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_MONTHLY;
import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_YEARLY;
//import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_YEAR_NTH;
import static com.funambol.common.pim.calendar.RecurrencePattern.TYPE_MONTH_NTH;
import static com.funambol.common.pim.calendar.RecurrencePattern.UNSPECIFIED;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.source.Helper;
import org.evolvis.bsi.kolab.service.Cycle;
import org.evolvis.bsi.kolab.service.Recurrence;

import com.funambol.common.pim.calendar.ExceptionToRecurrenceRule;
import com.funambol.common.pim.calendar.RecurrencePattern;
import com.funambol.common.pim.common.ConversionException;


/**
 * @author Hendrik Helwich, Patrick Apel tarent GmbH
 * 
 */
public class RecurrenceConverter {
	
	private static ConvertException oConvertException = new ConvertException("default");
	private static String strExceptionMessage = null;
	
	//Convert Kolab recurrence pattern to Funambol reccurence pattern
	@SuppressWarnings({ "static-access", "unused" })
	static RecurrencePattern convertRecurrence(Recurrence r, Date startDate) {
		if (r == null)
			return null;
		
		Date dtDate = new Date();
		RecurrencePattern rp = new RecurrencePattern(UNSPECIFIED, 0, UNSPECIFIED, UNSPECIFIED, UNSPECIFIED, UNSPECIFIED, "", "", true);
		String startDatePattern = Helper.long2localtime(startDate);
		String endDatePattern = null;
		short interval = 0;
		if(r.getInterval() != 0) {
		interval = (short) r.getInterval();
		}
		
		short monthOfYear = 0;
		if(r.getMonth() != null){
		monthOfYear = (r.getMonth().shortValue());
		}
		
		short dayOfMonth = 0;
		if(r.getMonthDay() != null){
		dayOfMonth = (r.getMonthDay().shortValue());
		}
		
		short week = 0;
		if(r.getWeek() != null){
		week = (r.getWeek().shortValue());
		}
		
		short dayOfWeekMask = 0;
		if(r.getWeekDays() != null){
		dayOfWeekMask = Converter.convertDayOfWeekMaskFromKolab(r.getWeekDays());
		}
		
		int occurrences = -1;
		boolean noEndDate = true;
		
		if (r.getRangeNumber() != null) {
			int rn = r.getRangeNumber();
			if (rn != 0) {
				occurrences = rn;
			}
		} else {
			Date rangeDate = r.getRangeDate(); // must be not null
			
			String rangeStr = Helper.long2utc(rangeDate); 
			
			endDatePattern = rangeStr; // add time of start date
		}
		
		org.evolvis.bsi.kolab.service.Cycle cycle;
		
		cycle = r.getCycle();
		
		String strLongUTC = null;
		int listSize = r.getExclusion().size();
		List<Date>dtExclusions = r.getExclusion();
		
		java.util.List<ExceptionToRecurrenceRule>dtExclusions_; 
		dtExclusions_ = new LinkedList<ExceptionToRecurrenceRule>(); 
		ExceptionToRecurrenceRule exToRe[] = new ExceptionToRecurrenceRule[listSize];

		if(listSize != 0){
		for(int i = 0; i < listSize; i++){

			try {
				strLongUTC = Helper.long2utc(dtExclusions.get(i));
				exToRe[i] = new ExceptionToRecurrenceRule(false, strLongUTC);
				
			} catch (ParseException e) {	
				strExceptionMessage = "New ExceptionToRecurrenceRule";
				oConvertException.getExceptionInformation(strExceptionMessage, e);
			}
			
			dtExclusions_.add(exToRe[i]);
		}

		}
		
		switch (cycle) {
		case DAILY:
			noEndDate = endDatePattern == null;	
			rp = new RecurrencePattern(TYPE_DAILY, interval, UNSPECIFIED, UNSPECIFIED, UNSPECIFIED, UNSPECIFIED, startDatePattern, endDatePattern, noEndDate, occurrences);
			break;
		case WEEKLY:
			noEndDate = endDatePattern == null;
			rp = new RecurrencePattern(TYPE_WEEKLY, interval, UNSPECIFIED, UNSPECIFIED, dayOfWeekMask, UNSPECIFIED, startDatePattern, endDatePattern, noEndDate, occurrences);
			break;
		case MONTHLY: 
			// 2 possibilities for year. Decision is in the pattern
			noEndDate = endDatePattern == null;
			if(dayOfMonth == UNSPECIFIED){
			rp = new RecurrencePattern(TYPE_MONTH_NTH, interval, UNSPECIFIED, UNSPECIFIED, dayOfWeekMask, week, startDatePattern, endDatePattern, noEndDate, occurrences);
			}else{
			rp = new RecurrencePattern(TYPE_MONTHLY, interval, UNSPECIFIED, dayOfMonth, UNSPECIFIED, UNSPECIFIED, startDatePattern, endDatePattern, noEndDate, occurrences);
			}
			break;
		case YEARLY:
			// 2 possibilities for year. Decision is in the pattern
			// anniversary is not available in the recurrence pattern
			noEndDate = endDatePattern == null;	
			if(dayOfWeekMask == UNSPECIFIED){ 
			rp = new RecurrencePattern(TYPE_YEARLY, interval, monthOfYear, dayOfMonth, UNSPECIFIED, UNSPECIFIED, startDatePattern, endDatePattern, noEndDate, occurrences);
			}else{
			/* Workaround for Windows Mobile 6: You can set the recurrence of an appointment that this appointment will happen every year. Also you can 
			 * set the recurrence to every two years.
			 * This is not possible in Windows Mobile 6. In Windows Mobile you can set the recurrence to every 12 month instead, but not to a recurrence of every 24 months.
			 * This behavior will take place, if you want to synchronize a appointment that takes place every year in the same month and at the same weekday.
			 */
			//Before the Workaround: rp = new RecurrencePattern(TYPE_YEAR_NTH, interval, monthOfYear, UNSPECIFIED, dayOfWeekMask, week, startDatePattern, endDatePattern, noEndDate, occurrences);
			//The same information of the appointment just in an other pattern. After:
			rp = new RecurrencePattern(TYPE_MONTH_NTH, 12, UNSPECIFIED, UNSPECIFIED, dayOfWeekMask, week, startDatePattern, endDatePattern, noEndDate, occurrences);
			}
			break; 
		default:
			throw new RuntimeException("recurrence has invalid cycle "+r.getCycle());
		}
		
		rp.setExceptions(dtExclusions_);

		return rp;
	}
	
	
	//Convert Funambol recurrence pattern to Kolab reccurence pattern
	@SuppressWarnings("unused")
	static Recurrence convertRecurrencePattern(RecurrencePattern rp, TimeZone deviceZone) throws ConversionException {
		if (rp == null)
			return null;
        // get the client TimeZone
        String tz = rp.getTimeZone();
        if (tz != null)
        	deviceZone = TimeZone.getTimeZone(tz);
        
		String sdp = rp.getStartDatePattern(); //not utc but local time!!! Example: 20091015T060000
		String edp = rp.getEndDatePattern();   //not utc but local time!!! Example: 20100103T060000
		// if tz ="Europe/Berlin" the format is for example: 20091015T060000Z in sdp and edp
		// convert to utc time
		
		Date sd, ed = null;
		if (sdp != null && deviceZone != null)
			sd = Helper.localtime2long(sdp, deviceZone);
		if (edp != null && deviceZone != null)
			ed = Helper.localtime2long(edp, deviceZone);

		Recurrence r = new Recurrence();
		r.setInterval(rp.getInterval());
        
		String strDate = null;
		Date dtDate = null;
		
		if(rp.getExceptions().size() != 0){
		for(int i = 0; i < rp.getExceptions().size(); i++){
			strDate = rp.getExceptions().get(i).getDate();
			dtDate = Helper.eliminateTime(strDate, TimeZone.getTimeZone("UTC"));
			r.getExclusion().add(dtDate);
		}
		}
		
		int oc = rp.getOccurrences();
		if (ed != null)
			// for Bug [#43] Nokia S60: Serientermin wird nicht im Webinterface angelegt
			// https://bsi-evolvis.tarent.de/tracker/index.php?func=detail&aid=43&group_id=7&atid=105
			// Error was in DateTypeAdapter.java: "time is present in date type". (setRangeDate) 
			r.setRangeDate(Helper.eliminateTime(Helper.dateToString(ed), TimeZone.getTimeZone("UTC")));
		else if (oc != -1) {
			if (oc < 1)
				throw new RuntimeException("illegal occurrence: "+oc);
			r.setRangeNumber(oc);
		} else 
			r.setRangeNumber(0);
		switch (rp.getTypeId()) {
		case RecurrencePattern.TYPE_DAILY:
			r.setCycle(Cycle.DAILY);
			break;
		case RecurrencePattern.TYPE_WEEKLY:
			r.setCycle(Cycle.WEEKLY);
			r.setWeekDays(Converter.convertDayOfWeekMaskToKolab(rp.getDayOfWeekMask()));
			break;
		case RecurrencePattern.TYPE_MONTHLY:
			r.setCycle(Cycle.MONTHLY);
			r.setMonthDay((int) rp.getDayOfMonth());
			break;
		case RecurrencePattern.TYPE_MONTH_NTH:
			r.setCycle(Cycle.MONTHLY);
			r.setWeek((int) rp.getInstance()); //TODO verify that no conversion is needed here
			r.setWeekDays(Converter.convertDayOfWeekMaskToKolab(rp.getDayOfWeekMask()));
			break;
		case RecurrencePattern.TYPE_YEARLY:
			r.setCycle(Cycle.YEARLY);
			r.setMonth((int) rp.getMonthOfYear()); //TODO maybe decrease by one?
			r.setMonthDay((int) rp.getDayOfMonth());
			break;
		case RecurrencePattern.TYPE_YEAR_NTH:
			r.setCycle(Cycle.YEARLY);
			r.setMonth((int) rp.getMonthOfYear()); //TODO maybe decrease by one?
			r.setWeek((int) rp.getInstance()); //TODO verify that no conversion is needed here
			r.setWeekDays(Converter.convertDayOfWeekMaskToKolab(rp.getDayOfWeekMask()));
			break;
		default:
			throw new RuntimeException("recurrence has invalid type "+rp.getTypeId());
		}
		return r;
	}
}
