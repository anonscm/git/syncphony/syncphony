/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;

import static org.evolvis.bsi.funambol.source.Helper.*;
import static org.evolvis.bsi.funambol.source.Helper.KolabType.EVENT;
import static org.evolvis.bsi.funambol.source.Helper.KolabType.TASK;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.evolvis.bsi.funambol.convert.Converter;
import org.evolvis.bsi.funambol.source.Helper.KolabType;
import org.evolvis.bsi.kolab.service.Event;
import org.evolvis.bsi.kolab.service.Folder;
import org.evolvis.bsi.kolab.service.Task;

import com.funambol.common.pim.calendar.Calendar;

/**
 * @author Hendrik Helwich
 *
 */
public class KolabCalendarSyncSource extends AbstractKolabSyncSource {

	private boolean syncTask = true;
	private boolean syncEvent = true;

	protected Map<KolabType, List<Folder>> getItemFoldersWT() throws Exception {
		Map<KolabType, List<Folder>> folders = new HashMap<KolabType, List<Folder>>();
		if (isSyncEvent())
			folders.put(EVENT, getClient().getEventFolders());
		if (isSyncTask())
			folders.put(TASK, getClient().getTaskSyncFolders());
		return folders;
	}

	@Override
	protected List<String> getAllItemIDs(Folder folder, KolabType type, Date updateTime) throws Exception {
		if (type == EVENT)
			return getClient().getEventIDs(folder, updateTime);
		if (type == TASK)
			return getClient().getTaskIDs(folder, updateTime);
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected List<String> getUpdatedItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		if (type == EVENT)
			return getClient().getUpdatedEventIDs(folder, lastUpdateTime, updateTime);
		if (type == TASK)
			return getClient().getUpdatedTaskIDs(folder, lastUpdateTime, updateTime);
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected List<String> getDeletedItemIDs(Folder folder, KolabType type,
			Date lastUpdateTime, Date updateTime) throws Exception {
		if (type == EVENT)
			return getClient().getRemovedEventIDs(folder, lastUpdateTime, updateTime);
		if (type == TASK)
			return getClient().getRemovedTaskIDs(folder, lastUpdateTime, updateTime);
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected List<String> getNewItemIDs(Folder folder, KolabType type, Date lastUpdateTime, Date updateTime)
			throws Exception {
		if (type == EVENT)
			return getClient().getNewEventIDs(folder, lastUpdateTime, updateTime);
		if (type == TASK)
			return getClient().getNewTaskIDs(folder, lastUpdateTime, updateTime);
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected String addItem(Folder folder, KolabType type, String content, String contentType)
			throws Exception {
		if (type == EVENT) {
			Calendar calendar = createCalendar(content, contentType, getTimezone(),
					getCharset());
			Event event = Converter.convertToEvent(calendar, getTimezone(), calendar.getCalendarContent().getDtStart().getTimeZone(), contentType); //getTimezone());
			removeFolderInfo(event);
			return getClient().addEvent(folder, event);
		}
		if (type == TASK) {
			com.funambol.common.pim.calendar.Task ftask = createTask(content, contentType, getTimezone(),
					getCharset());
			Task task = Converter.convertToTask(ftask, getTimezone(), ftask.getDAlarm().getTimeZone(), contentType);
			return getClient().addTask(folder, task);
		}
		throw new RuntimeException("illegal folder type "+type);
	}
	
	@Override
	protected boolean updateItem(Folder folder, KolabType type, String id, Date lastUpdateTime,
			String content, String contentType) throws Exception {
		if (type == EVENT) {
			Calendar calendar = createCalendar(content, contentType, getTimezone(),
					getCharset());
			Event event = Converter.convertToEvent(calendar,  getTimezone(), calendar.getCalendarContent().getDtStart().getTimeZone(),  contentType);
			removeFolderInfo(event);
			event.setUid(id);
			return getClient().updateEvent(folder, event, lastUpdateTime);
		}
		if (type == TASK) {
			com.funambol.common.pim.calendar.Task fcontact = createTask(content, contentType, getTimezone(),
					getCharset());
			Task task = Converter.convertToTask(fcontact, getTimezone(), fcontact.getDAlarm().getTimeZone(), contentType);
			task.setUid(id);
			return getClient().updateTask(folder, task, lastUpdateTime);
		}
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected boolean removeItem(Folder folder, KolabType type, String id, Date lastUpdateTime)
			throws Exception {
		if (type == EVENT) {
			return getClient().removeEvent(folder, id, lastUpdateTime);
		}
		if (type == TASK) {
			return getClient().removeTask(folder, id, lastUpdateTime);
		}
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected String getItem(Folder folder, KolabType type, String id, String contentType)
			throws Exception {
		if (type == EVENT) {
			Event event = getClient().getEvent(folder, id);
			if (event == null)
				return null;
			addFolderInfo(folder.getName(), event);
			Calendar calendar = Converter.convertEvent(event);
			String content = createContent(calendar, contentType, getTimezone(),
					getCharset());
			return content;
		}
		if (type == TASK) {
			Task task = getClient().getTask(folder, id);
			if (task == null)
				return null;
			com.funambol.common.pim.calendar.Task ftask = Converter
					.convertTask(task);
			String content = createContent(ftask, contentType, getTimezone(),
					getCharset());
			return content;
		}
		throw new RuntimeException("illegal folder type "+type);
	}

	@Override
	protected KolabType getType(String content, String type) {
		if (CONTENT_TYPE_VCAL2.equals(type) || CONTENT_TYPE_ICAL.equals(type)) {
			if (content.contains("BEGIN:VEVENT"))
				return EVENT;
			if (content.contains("BEGIN:VTODO"))
				return TASK;
			throw new RuntimeException("unexpected vcard content");
		} else if (CONTENT_TYPE_SIF_EVENT.equals(type)) {
			return EVENT;
		} else if (CONTENT_TYPE_SIF_TASK.equals(type)) {
			return TASK;
		} else
			throw new RuntimeException("unknown content type "+type);
	}

	public boolean isSyncTask() {
		return syncTask;
	}

	public void setSyncTask(boolean syncTask) {
		if (!syncTask && !syncEvent)
			throw new IllegalArgumentException("calendar syncsource must have at least one type");
		this.syncTask = syncTask;
	}

	public boolean isSyncEvent() {
		return syncEvent;
	}

	public void setSyncEvent(boolean syncEvent) {
		if (!syncTask && !syncEvent)
			throw new IllegalArgumentException("calendar syncsource must have at least one type");
		this.syncEvent = syncEvent;
	}

	static final void addFolderInfo(String folderName, Event event) {
		if (event == null)
			return;
		int idx1 = folderName.lastIndexOf('/');
		if (idx1 != -1 && folderName.length() == idx1+1) { // slash is last char
			addFolderInfo(folderName.substring(0, folderName.length()-1), event);
			return;
		} 
		String parentName = folderName.substring(idx1+1);
		if (parentName.length() > 0) {
			parentName = parentName.replace(']', ' ');
			parentName = '['+parentName+']';
			String summary = event.getSummary();
			if (summary != null)
				summary = summary.trim();
			if (summary != null && summary.length() > 0)
				event.setSummary(parentName + ' ' + summary);
			else
				event.setSummary(parentName);
		}
	}

	static final void removeFolderInfo(Event event) {
		if (event == null)
			return;
		String summary = event.getSummary();
		if (summary != null) {
			Pattern fnp = Pattern.compile("\\s*\\[[^\\]]+\\](.*)");
			Matcher matcher = fnp.matcher(summary);
			if (matcher.matches()) {
				summary = matcher.group(1).trim();
				event.setSummary(summary);
			}
		}
	}
	
}
