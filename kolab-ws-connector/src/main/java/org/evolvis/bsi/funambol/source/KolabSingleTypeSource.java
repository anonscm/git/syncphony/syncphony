/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.bsi.funambol.source.Helper.KolabType;
import org.evolvis.bsi.kolab.service.Folder;




/**
 * @author Hendrik Helwich
 *
 */
abstract public class KolabSingleTypeSource extends AbstractKolabSyncSource {
	
	private KolabType type;
	
	public KolabSingleTypeSource(KolabType type) {
		this.type = type;
	}

	@Override
	protected Map<KolabType, List<Folder>> getItemFoldersWT() throws Exception {
		return addType(getItemFolders(), type);
	}


	@Override
	protected KolabType getType(String content, String type) {
		return this.type;
	}

	private static Map<KolabType, List<Folder>> addType(List<Folder> folders, KolabType type) {
		Map<KolabType, List<Folder>> map = new HashMap<KolabType, List<Folder>>();
		map.put(type, folders);
		return map;
	}
	
    abstract protected List<Folder> getItemFolders() throws Exception;


}
