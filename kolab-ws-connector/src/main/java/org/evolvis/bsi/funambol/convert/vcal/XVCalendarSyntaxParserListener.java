/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.convert.vcal;

import java.util.TimeZone;
import com.funambol.common.pim.ParserProperty;
import com.funambol.common.pim.calendar.Calendar;
import com.funambol.common.pim.model.VCalendar;
import com.funambol.common.pim.xvcalendar.XVCalendarParser;
import java.io.InputStream;

import org.evolvis.bsi.funambol.convert.vcal.XVCalendarSyntaxParserListenerImpl;

public class XVCalendarSyntaxParserListener {

	private final XVCalendarSyntaxParserListenerImpl delegate;

	private final VCalendar vcalendar;
	
	public XVCalendarSyntaxParserListener(VCalendar vcalendar) {
		
		delegate = new XVCalendarSyntaxParserListenerImpl(vcalendar);
		this.vcalendar = vcalendar;
	}

	public void start() {
		delegate.start();
	}


	public void end() {
		delegate.end();
	}


	public void addProperty(ParserProperty property) {
		delegate.addProperty(property);
	}


	public void startEvent() {
		delegate.startEvent();
	}


	public void addEventProperty(ParserProperty property) {
		delegate.addEventProperty(property);
	}


	public void endEvent() {
		delegate.endEvent();
	}


	public void startToDo() {
		delegate.startToDo();
	}


	public void addToDoProperty(ParserProperty property) {
		delegate.addToDoProperty(property);
	}


	public void endToDo() {
		delegate.endToDo();
	}
	
}
