/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.funambol.source.bypass;

import com.funambol.common.pim.converter.TimeZoneTransition;
import com.funambol.common.pim.utility.TimeUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * This class is just instrumental to the output of time-zone information in the
 * iCalendar format.
 */
public class ICalendarTimeZoneTransition extends TimeZoneTransition {

    private int year;
    private int month;
    private int dayOfWeek;
    private int instance;
    private boolean lastInstance;
    private String timeISO1861;

    public ICalendarTimeZoneTransition(TimeZoneTransition transition,
                                       int previousOffset           ) {
        
        super(transition.getOffset(), 
              transition.getTime()  , 
              transition.getName()  );
        
        TimeZone fixed = TimeZone.getTimeZone("UTC");
        fixed.setRawOffset(previousOffset);
        Calendar finder = new GregorianCalendar(fixed);
        
        finder.setTimeInMillis(transition.getTime());
        year = finder.get(Calendar.YEAR);
        month = finder.get(Calendar.MONTH);
        dayOfWeek = finder.get(Calendar.DAY_OF_WEEK);
        instance = finder.get(Calendar.DAY_OF_WEEK_IN_MONTH);
        lastInstance =
                (finder.getActualMaximum(Calendar.DAY_OF_WEEK_IN_MONTH)
                 ==
                 instance);
        DateFormat localTimeFormat = 
                new SimpleDateFormat(TimeUtils.PATTERN_UTC_WOZ);
        localTimeFormat.setTimeZone(fixed);
        timeISO1861 = localTimeFormat.format(finder.getTime());
    }
    
    public ICalendarTimeZoneTransition(String name, int offset) {
        super(offset, 
              0     , // age start
              name  );
        year         = 1970;
        month        = 1;
        dayOfWeek    = 5; // It was a Thursday
        instance     = 1;
        lastInstance = false;
        timeISO1861  = "19700101T000000";
    }
    
    public boolean matchesRecurrence(ICalendarTimeZoneTransition other      ,
                                     boolean                     lastInMonth) {
        if (getMonth() != other.getMonth()) {
            return false;
        }
        if (getDayOfWeek() != other.getDayOfWeek()) {
            return false;
        }
        if (lastInMonth) {
            if (!lastInstance || !(other.lastInstance)) {
               return false;
            }         
            return true;
        }
        return (getInstance() == other.getInstance());
    }
    
    public String getTimeISO1861() {
        return timeISO1861;
    }

    public int getMonth() {
        return month;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getInstance() {
        return instance;
    }

    public int getYear() {
        return year;
    }
}
