/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.kolab.service;

import static org.evolvis.bsi.funambol.source.Helper.*;

import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.SecurityHeaderType;

public class KolabServiceClient {

	public static void main(String[] args) {
		try {
			SecurityHeaderType header = createHeader("papel", "test");
			KolabServicePortType type = getServiceCient("http://localhost:8877/kolab-ws/KolabServicePortTypeImpl");
			System.out.println(type.getCurrentTime(new GetCurrentTime(), header).getReturn());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}