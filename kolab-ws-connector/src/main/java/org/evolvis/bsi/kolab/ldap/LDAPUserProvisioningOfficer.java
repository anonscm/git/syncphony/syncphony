/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.kolab.ldap;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.funambol.framework.core.Authentication;
import com.funambol.framework.core.Cred;
import com.funambol.framework.security.*;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.framework.server.store.NotFoundException;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.framework.tools.Base64;
import com.funambol.framework.tools.beans.LazyInitBean;

import com.funambol.server.admin.AdminException;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;
import com.funambol.server.security.DBOfficer;



/**
 * This is an implementation of the <i>Officer</i> interface 
 * If an user can authenticate on an LDAP server 
 * defined in LDAPUserProvisioningOfficer.xml it will
 *  be added to the database.
 * It requires basic authentication
 *
 * @author  <a href='mailto:r.polli _@ babel.it'>Roberto Polli</a>
 *
 * @version $Id: LDAPUserProvisioningOfficer.java,v 1.00 2007/11/26 10:40:27 rpolli@babel.it Exp $
 * from UserProvisioningOfficer.java
 * 
 * 
 * @author of modification Patrick Apel tarent GmbH
 * 
 * @version $id modification of the LDAPUserProvisioningOfficer.java 2009/11/23
 * 
 */
public class LDAPUserProvisioningOfficer
extends DBOfficer
implements LazyInitBean {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String EMAIL_PATTERN = "^[a-zA-Z]([\\w\\.-]*[a-zA-Z0-9])*@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
	private boolean boolUserIsInRole = false; 

	// set ldap url if given
	protected String ldapUrl = "ldap://localhost:389/";
	public void setLdapUrl(String s) {
		ldapUrl = s; 
	}
	public String getLdapUrl() {
		return ldapUrl;
	}	
	
	// set ldap server properties: ldapServer, ldapPort, baseDn, isSSL, userSearch
	protected String ldapServer = "localhost";
	public void setLdapServer(String s) {
		ldapServer = s;
	}
	public String getLdapServer() {
		return ldapServer;
	}

	protected int ldapPort = 389;
	public void setLdapPort (int p) {
		ldapPort = p;
	}
	public void setLdapPort (String p) {
		Integer port = Integer.parseInt(p);
		
		ldapPort =  (port > 0) ? port : 389 ;
	}
	public Integer getLdapPort () {
		return ldapPort;
		
	}

	protected boolean isSSL = false;
	public void setIsSSL (boolean b) {
		isSSL = b;
	}
	public boolean getIsSSL () {
		return isSSL;
	}
	
	protected String bindDn = null;
	public void setBindDn(String s) {
		bindDn = s;
	}
	
	public String getBindDn() {
		return bindDn;
	}
	
	protected String bindCredential= null;
	public void setBindCredential(String s) {
		bindCredential = s;
	}
	public String getBindCredential() {
		return bindCredential;
	}
	
	protected String baseDn =null;
	public void setBaseDn(String s) {
		baseDn = s;
	}
	public String getBaseDn() {
		return baseDn;
	}
	
	protected String roleDn =null;
	public void setRoleDn(String s) {
		roleDn = s;
	}
	public String getRoleDn() {
		return roleDn;
	}
	
	protected String userRole = null;
	public void setUserRole(String s) {
		userRole = s;
	}
	public String getUserRole() {
		return userRole;
	}
	
	protected String baseUserFilter = null; 
	public void setBaseUserFilter(String s) {
		baseUserFilter = s;
	}
	public String getBaseUserFilter() {
		return baseUserFilter;
	}
	
	protected String roleFilter = null; 
	public void setRoleFilter(String s) {
		roleFilter = s;
	}
	public String getRoleFilter() {
		return roleFilter;
	}
	
	protected String roleAttributeId = null;
	public void setRoleAttributeId(String s) {
		roleAttributeId = s;
	}
	public String getRoleAttributeId() {
		return roleAttributeId;
	}
	
	protected String searchFilter = null;
	public void setSearchFilter(String s) {
		searchFilter = s;
	}
	public String getSearchFilter() {
		return searchFilter;
	}

	// ------------------------------------------------------------ Constructors
    public LDAPUserProvisioningOfficer() {
        super();
    }

    // ---------------------------------------------------------- Public methods

    public void init() {
        super.init();
    }

    /**
     * Authenticates a credential.
     *
     * @param credential the credential to be authenticated
     *
     * @return the Sync4jUser if the credential is autenticated, null otherwise
     */
    public Sync4jUser authenticateUser(Cred credential) {

        Configuration config = Configuration.getConfiguration();
        ps = config.getStore();

        userManager = (UserManager)config.getUserManager();

        String type = credential.getType();

        if ((Cred.AUTH_TYPE_BASIC).equals(type)) {
            return authenticateBasicCredential(credential);
        }

        return null;
    }
 
    /**
     * Gets the supported authentication type
     *
     * @return the basic authentication type
     */
    public String getClientAuth() {
        return Cred.AUTH_TYPE_BASIC;
    }

    // ------------------------------------------------------- Protected Methods
    /**
     * Checks the given credential thru LDAP bind. 
     * If the user or the principal isn't found, *but the user can bind*,
     * they are created.
     *
     * @param credential the credential to check
     *
     * @return the Sync4jUser if the credential is autenticated, null otherwise
     */
    protected Sync4jUser authenticateBasicCredential(Cred credential) {
        String username = null, password = null;
        
        Authentication auth = credential.getAuthentication();
        String deviceId = auth.getDeviceId();

        String userpwd = new String(Base64.decode(auth.getData()));

        int p = userpwd.indexOf(':');

        if (p == -1) {
            username = userpwd;
            password = "";
        } else {
            username = (p > 0) ? userpwd.substring(0, p) : "";
            password = (p == (userpwd.length() - 1)) ? "" :
                       userpwd.substring(p + 1);
        }

        if (log.isTraceEnabled()) {
            log.trace("User to check: " + username);
        }

        // if username  is an email substitute %u e %d in baseDn:  
		if ( username.matches(EMAIL_PATTERN)) {
			String tmp[] = username.split("@");
			String myUsername = tmp[0];
			String myDomain   = (tmp.length >1) ? tmp[1] : "example.com"; // if domain is not set, it shouldn't be used in the search
			log.info("username is [" +username+"," + myUsername +", " + myDomain + "]"); 
			
			// expand %u and %d in ldapUrl and baseDn
			// this enables elastic funambol support:D
			baseDn = baseDn.replaceAll("%u",myUsername);
			baseDn = baseDn.replaceAll("%d",myDomain);
			ldapUrl = ldapUrl.replaceAll("%u",myUsername);
			ldapUrl = ldapUrl.replaceAll("%d",myDomain);
			
			log.info("connecting to "+ ldapUrl + "/" + baseDn);
		} 
        
		// try the binding
        if (!ldapBind(username, password) ) {
        	// "puppa";
			  return null;
        }  
        
        //
        // Gets the user without checking the password
        //
        Sync4jUser user = getUser(username, null);

        if (user == null) {

            if (log.isTraceEnabled()) {
                log.trace("User '" +
                           username +
                          "' not found. A new user will be created");
            }
            // TODO try to bind to ldap server:
            // TODO set myUserSearch to retrieve the user by mail (eg. search '(mail=rpolli@babel.it)' )

            // TODO set the baseDn to retrieve the user by domain (eg. rpolli@babel.it -> rpolli+babel.it)
            
            try {
                try {
					user = insertUser(username, password);
				} catch (AdminException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                if (log.isTraceEnabled()) {
                    log.trace("User '" + username + "' created");
                }
            } catch (PersistentStoreException e) {
                log.error("Error inserting a new user", e);
                return null;
            }

        } else {
            if (log.isTraceEnabled()) {
                log.trace("User '" + username + "' found");
            }
            //
            // Check the password
            //
            String storedPassword = user.getPassword();
            if (!password.equals(storedPassword)) {
                //
                // The user isn't authenticate
                //
               /* if (log.isTraceEnabled()) {
                    log.trace( "The sent password is different from the stored "
                             + "one. User not authenticate");
                } */
            	
				user.setPassword(password);	
				auth.setPassword(password);
				
				try {
					userManager.setUser(user);
				} catch (PersistentStoreException e) {
					e.printStackTrace();
				} catch (AdminException e) {
					e.printStackTrace();
				}
			   //////////////////////////////////////////	
                
				  boolean isASyncUser = isASyncUser(user);

	                if (isASyncUser) {
	                    //
	                    // User authenticate
	                    //
	                    if (log.isTraceEnabled()) {
	                        log.trace("User authenticate");
	                    }
	                } else {
	                    //
	                    // User not authenticate
	                    //
	                    if (log.isTraceEnabled()) {
	                        log.trace("The user is not a '" + ROLE_USER + "'");
	                    }
	                    return null;
	                }
				/////////////////////////////////////////////////
				
               // return null;
            } else {
                //
                // Check the roles
                //
                boolean isASyncUser = isASyncUser(user);

                if (isASyncUser) {
                    //
                    // User authenticate
                    //
                    if (log.isTraceEnabled()) {
                        log.trace("User authenticate");
                    }
                } else {
                    //
                    // User not authenticate
                    //
                    if (log.isTraceEnabled()) {
                        log.trace("The user is not a '" + ROLE_USER + "'");
                    }
                    return null;
                }
            }
        }

        //
        // Verify that the principal for the specify deviceId and username exists
        // Otherwise a new principal will be created
        //
        try {
            handlePrincipal(username, deviceId);
        } catch (PersistentStoreException e) {
            log.error("Error handling the principal", e);
            return null;
        }

        return user;
    }

    /**
     * Insert a new user with the given username and password
     *
     * @param userName the username
     * @param password the password
     *
     * @return the new user
     *
     * @throws PersistentStoreException if an error occurs
     * @throws AdminException 
     */
    protected Sync4jUser insertUser(String userName, String password)
    throws PersistentStoreException, AdminException {

        Sync4jUser user = new Sync4jUser();
        user.setUsername(userName);
        user.setPassword(password);
        user.setRoles(new String[] {ROLE_USER});
        userManager.insertUser(user);
        return user;
    }

    /**
     * Returns the principal with the given username and deviceId.
     * <code>null</code> if not found
     * @param userName the username
     * @param deviceId the device id
     * @return the principal found or null.
     */
    protected Sync4jPrincipal getPrincipal(String userName, String deviceId)
    throws PersistentStoreException {

        Sync4jPrincipal principal = null;

        //
        // Verify that exist the principal for the specify deviceId and username
        //
        principal = Sync4jPrincipal.createPrincipal(userName, deviceId);

        try {
            ps.read(principal);
        } catch (NotFoundException ex) {
            return null;
        }

        return principal;
    }

    /**
     * Inserts a new principal with the given userName and deviceId
     * @param userName the username
     * @param deviceId the device id
     * @return the principal created
     * @throws PersistentStoreException if an error occurs creating the principal
     */
    protected Sync4jPrincipal insertPrincipal(String userName, String deviceId)
    throws PersistentStoreException {

        //
        // We must create a new principal
        //
        Sync4jPrincipal principal =
            Sync4jPrincipal.createPrincipal(userName, deviceId);

        ps.store(principal);

        return principal;
    }


    /**
     * Searches if there is a principal with the given username and device id.
     * if no principal is found, a new one is created.
     * @param userName the user name
     * @param deviceId the device id
     * @return the found principal or the new one
     */
    protected Sync4jPrincipal handlePrincipal(String username, String deviceId)
    throws PersistentStoreException {

        Sync4jPrincipal principal = null;

        //
        // Verify if the principal for the specify deviceId and username exists
        //

        principal = getPrincipal(username, deviceId);

        if (log.isTraceEnabled()) {
            log.trace("Principal '" + username +
                       "/" +
                       deviceId + "' " +
                       ((principal != null) ?
                        "found"             :
                        "not found. A new principal will be created")
                       );
        }

        if (principal == null) {
            principal = insertPrincipal(username, deviceId);
            if (log.isTraceEnabled()) {
                log.trace("Principal '" + username +
                           "/" +
                           deviceId + "' created");
            }
        }

        return principal;
    }
    
    /**
     * return false if user or password is wrong
     * 	if username ~= EMAIL_PATTERN then baseDN is already nice
	  * 	if defined userSearch, retrieve user's DN  and try to bind with it
     * @param username
     * @param password
     * @return
     */
    
    @SuppressWarnings("unchecked")
	public boolean ldapBind(String username, String password) {    	
    	
    	String[] strUserCN = new String[] {baseUserFilter};
    	String strUserCnDn = null;
    	String strUserRealCnDn = null;
    	
    	if (! "none".equals(userRole)) {
	    	// customize the field used to search the user.

    		Hashtable<String, Object> notAnonimousEnv = new Hashtable<String, Object>(11);
	    	notAnonimousEnv.put(Context.INITIAL_CONTEXT_FACTORY, 
	    	    "com.sun.jndi.ldap.LdapCtxFactory");
	    	notAnonimousEnv.put(Context.PROVIDER_URL, getLdapProviderUrl() );
	    	
	    	notAnonimousEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
	    	notAnonimousEnv.put(Context.SECURITY_PRINCIPAL, getBindDn()); 
	    	notAnonimousEnv.put(Context.SECURITY_CREDENTIALS, getBindCredential());
	    	
	    	try {
	    	
				DirContext anonCtx = new InitialDirContext(notAnonimousEnv);

				Attributes matchAttrs_ = new BasicAttributes(true);
				matchAttrs_.put(new BasicAttribute(searchFilter, username)); //mail
				
				NamingEnumeration answer_ = anonCtx.search(baseDn,matchAttrs_,strUserCN);
				strUserCnDn = answer_.next().toString();				
				String[] strUserCnSplit = strUserCnDn.split(":");
				strUserRealCnDn = strUserCnSplit[0] + "," + baseDn;
				
		        Attributes matchAttrs = new BasicAttributes(true); 
		        matchAttrs.put(new BasicAttribute(roleAttributeId, userRole)); 
		        matchAttrs.put(new BasicAttribute(roleFilter, strUserRealCnDn));

		        NamingEnumeration answer = anonCtx.search(roleDn, matchAttrs);        
		        
		       if (! answer.hasMore()) {
		        	//log.info("attribute "+ strUserRealCnDn +" not found for any user"); //No necessary. Could make the server slow 
					return false;
		        } 
		    /*   else
		       {
		    	   SearchResult sr = (SearchResult)answer.next();
		            log.info("Search result: " + sr.getName()); 
		            log.info(sr.getAttributes());
		       } */

				anonCtx.close();
			} catch (NamingException e) {
				return false;
			}
			catch (NullPointerException e){
				return false;
			}
			catch (Exception e) {
				return false;
			}
    	}
    	
    	// Set up environment for creating initial context
    	Hashtable<String, String> env = new Hashtable<String, String>(11);
    	env.put(Context.INITIAL_CONTEXT_FACTORY, 
    	    "com.sun.jndi.ldap.LdapCtxFactory");
    	env.put(Context.PROVIDER_URL, getLdapProviderUrl() );

    	env.put(Context.SECURITY_AUTHENTICATION, "simple");
    	env.put(Context.SECURITY_PRINCIPAL, strUserRealCnDn);
    	env.put(Context.SECURITY_CREDENTIALS, password);

    	try {
    	    DirContext ctx = new InitialDirContext(env);
    	    //FIXME remove the println, user for DEBUG
    	    ctx.close(); 
    	} catch (NamingException e) {
    	    e.printStackTrace();
    	    return false;
    	}
    	return true;
    }
    
    private String getLdapProviderUrl() {
    	if ( !"none".equals(getLdapUrl())) {
    		return getLdapUrl();
    	} else {
    	    return "ldap" + (isSSL? "s":"")
				+ "://"+getLdapServer()+":"+ getLdapPort() +
				"/";
    	}
    }
}