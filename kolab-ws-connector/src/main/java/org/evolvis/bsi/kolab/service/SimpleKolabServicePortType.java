/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
package org.evolvis.bsi.kolab.service;

import java.util.Date;
import java.util.List;

import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.SecurityHeaderType;

/**
 * Usage of the security header makes generated java code circuitous. This class
 * can be used to enhance code readability.
 * 
 * @author Hendrik Helwich
 *
 */
public class SimpleKolabServicePortType {
	
	private final KolabServicePortType delegate;
	private final SecurityHeaderType header;
	private final String deviceId;
	
	public SimpleKolabServicePortType(KolabServicePortType delegate, String deviceId, SecurityHeaderType header) {
		this.delegate = delegate;
		this.deviceId = deviceId;
		this.header = header;
	}

    public Event getEvent(Folder folder, String id) throws KolabServiceFault_Exception {
    	GetEvent parameters = new GetEvent();
    	parameters.setFolder(folder);
    	parameters.setId(id);
    	return delegate.getEvent(parameters, header).getReturn();
    }

    public Contact getContact(Folder folder, String id) throws KolabServiceFault_Exception {
    	GetContact parameters = new GetContact();
    	parameters.setFolder(folder);
    	parameters.setId(id);
    	return delegate.getContact(parameters, header).getReturn();
    }

    public Note getNote(Folder folder, String id) throws KolabServiceFault_Exception {
    	GetNote parameters = new GetNote();
    	parameters.setFolder(folder);
    	parameters.setId(id);
    	return delegate.getNote(parameters, header).getReturn();
    }

    public Task getTask(Folder folder, String id) throws KolabServiceFault_Exception {
    	GetTask parameters = new GetTask();
    	parameters.setFolder(folder);
    	parameters.setId(id);
    	return delegate.getTask(parameters, header).getReturn();
    }
    
    public boolean removeEvent(Folder folder, String id, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	RemoveEvent parameters = new RemoveEvent();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setId(id);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.removeEvent(parameters, header).isReturn();
    }
    
    public boolean removeContact(Folder folder, String id, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	RemoveContact parameters = new RemoveContact();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setId(id);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.removeContact(parameters, header).isReturn();
    }
    
    public boolean removeNote(Folder folder, String id, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	RemoveNote parameters = new RemoveNote();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setId(id);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.removeNote(parameters, header).isReturn();
    }
    
    public boolean removeTask(Folder folder, String id, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	RemoveTask parameters = new RemoveTask();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setId(id);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.removeTask(parameters, header).isReturn();
    }
    
    public boolean updateEvent(Folder folder, Event event, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	UpdateEvent parameters = new UpdateEvent();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setEvent(event);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.updateEvent(parameters, header).isReturn();
    }
    
    public boolean updateContact(Folder folder, Contact contact, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	UpdateContact parameters = new UpdateContact();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setContact(contact);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.updateContact(parameters, header).isReturn();
    }
    
    public boolean updateNote(Folder folder, Note note, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	UpdateNote parameters = new UpdateNote();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setNote(note);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.updateNote(parameters, header).isReturn();
    }
    
    public boolean updateTask(Folder folder, Task task, Date lastUpdateTime) throws KolabServiceFault_Exception {
    	UpdateTask parameters = new UpdateTask();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setTask(task);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	return delegate.updateTask(parameters, header).isReturn();
    }
    
    public String addEvent(Folder folder, Event event) throws KolabServiceFault_Exception {
    	AddEvent parameters = new AddEvent();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setEvent(event);
    	return delegate.addEvent(parameters, header).getReturn();
    }
    
    public String addContact(Folder folder, Contact contact) throws KolabServiceFault_Exception {
    	AddContact parameters = new AddContact();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setContact(contact);
    	return delegate.addContact(parameters, header).getReturn();
    }
    
    public String addNote(Folder folder, Note note) throws KolabServiceFault_Exception {
    	AddNote parameters = new AddNote();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setNote(note);
    	return delegate.addNote(parameters, header).getReturn();
    }
    
    public String addTask(Folder folder, Task task) throws KolabServiceFault_Exception {
    	AddTask parameters = new AddTask();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setTask(task);
    	return delegate.addTask(parameters, header).getReturn();
    }
    
    public List<String> getNewEventIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetNewEventIDs parameters = new GetNewEventIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getNewEventIDs(parameters, header).getReturn();
    }
    
    public List<String> getNewContactIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetNewContactIDs parameters = new GetNewContactIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getNewContactIDs(parameters, header).getReturn();
    }
    
    public List<String> getNewNoteIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetNewNoteIDs parameters = new GetNewNoteIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getNewNoteIDs(parameters, header).getReturn();
    }
    
    public List<String> getNewTaskIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetNewTaskIDs parameters = new GetNewTaskIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getNewTaskIDs(parameters, header).getReturn();
    }
    
    public List<String> getRemovedEventIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetRemovedEventIDs parameters = new GetRemovedEventIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getRemovedEventIDs(parameters, header).getReturn();
    }
    
    public List<String> getRemovedContactIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetRemovedContactIDs parameters = new GetRemovedContactIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getRemovedContactIDs(parameters, header).getReturn();
    }
    
    public List<String> getRemovedNoteIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetRemovedNoteIDs parameters = new GetRemovedNoteIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getRemovedNoteIDs(parameters, header).getReturn();
    }
    
    public List<String> getRemovedTaskIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetRemovedTaskIDs parameters = new GetRemovedTaskIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getRemovedTaskIDs(parameters, header).getReturn();
    }
    
    public List<String> getUpdatedEventIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetUpdatedEventIDs parameters = new GetUpdatedEventIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getUpdatedEventIDs(parameters, header).getReturn();
    }
    
    public List<String> getUpdatedContactIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetUpdatedContactIDs parameters = new GetUpdatedContactIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getUpdatedContactIDs(parameters, header).getReturn();
    }
    
    public List<String> getUpdatedNoteIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetUpdatedNoteIDs parameters = new GetUpdatedNoteIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getUpdatedNoteIDs(parameters, header).getReturn();
    }
    
    public List<String> getUpdatedTaskIDs(Folder folder, Date lastUpdateTime, Date updateTime) throws KolabServiceFault_Exception {
    	GetUpdatedTaskIDs parameters = new GetUpdatedTaskIDs();
    	parameters.setFolder(folder);
    	parameters.setDeviceId(deviceId);
    	parameters.setLastUpdateTime(lastUpdateTime);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getUpdatedTaskIDs(parameters, header).getReturn();
    }

	public List<Folder> getEventFolders() throws KolabServiceFault_Exception {
		return delegate.getEventFolders(new GetEventFolders(), header).getReturn();
	}

	public List<Folder> getContactFolders() throws KolabServiceFault_Exception {
		return delegate.getContactFolders(new GetContactFolders(), header).getReturn();
	}

	public List<Folder> getNoteFolders() throws KolabServiceFault_Exception {
		return delegate.getNoteFolders(new GetNoteFolders(), header).getReturn();
	}

	public List<Folder> getTaskSyncFolders() throws KolabServiceFault_Exception {
		return delegate.getTaskFolders(new GetTaskFolders(), header).getReturn();
	}

	public List<String> getEventIDs(Folder folder, Date updateTime) throws KolabServiceFault_Exception {
    	GetEventIDs parameters = new GetEventIDs();
    	parameters.setFolder(folder);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getEventIDs(parameters, header).getReturn();
	}

	public List<String> getContactIDs(Folder folder, Date updateTime) throws KolabServiceFault_Exception {
    	GetContactIDs parameters = new GetContactIDs();
    	parameters.setFolder(folder);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getContactIDs(parameters, header).getReturn();
	}

	public List<String> getNoteIDs(Folder folder, Date updateTime) throws KolabServiceFault_Exception {
    	GetNoteIDs parameters = new GetNoteIDs();
    	parameters.setFolder(folder);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getNoteIDs(parameters, header).getReturn();
	}

	public List<String> getTaskIDs(Folder folder, Date updateTime) throws KolabServiceFault_Exception {
    	GetTaskIDs parameters = new GetTaskIDs();
    	parameters.setFolder(folder);
    	parameters.setUpdateTime(updateTime);
    	return delegate.getTaskIDs(parameters, header).getReturn();
	}
	
	public Date getCurrentTime() {
    	return delegate.getCurrentTime(new GetCurrentTime(), header).getReturn();
	}
	
	public Attachment getAttachment(Folder folder, String id, String attachmentId) throws KolabServiceFault_Exception
	{
		GetAttachment parameters = new GetAttachment();
		parameters.setFolder(folder);
		parameters.setId(id);
		parameters.setAttachmentId(attachmentId);
		return delegate.getAttachment(parameters, header).getReturn();
	}
	
	public boolean addAttachment(Folder folder, String id, String attachmentId, Attachment attachment) throws KolabServiceFault_Exception
	{
		AddAttachment parameters = new AddAttachment();
		parameters.setAttachmentData(attachment);
		parameters.setAttachmentId(attachmentId);
		parameters.setFolder(folder);
		parameters.setId(id);
    	parameters.setDeviceId(deviceId);
		return delegate.addAttachment(parameters, header).isReturn();
	}
	
	public boolean removeAttachment(Folder folder, String id, String attachmentId) throws KolabServiceFault_Exception
	{
		RemoveAttachment parameters = new RemoveAttachment();
		parameters.setAttachmentId(attachmentId);
		parameters.setFolder(folder);
		parameters.setId(id);
    	parameters.setDeviceId(deviceId);
		return delegate.removeAttachment(parameters, header).isReturn();
	}
    
    
	//TODO delegate all other operations
	
}
