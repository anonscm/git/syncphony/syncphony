/* 
 * syncphony, an extension of the kolab groupware.
 * Copyright (C) 2010 tarent GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'syncphony'
 * Signature of Elmar Geese, 21.01.2010
 * Elmar Geese, CEO tarent GmbH
 */
// This file is a copy from the project: kolab-ws
package org.evolvis.xml.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.evolvis.bsi.funambol.convert.ConvertException;
import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.evolvis.bsi.funambol.source.Helper;

import com.funambol.common.pim.common.ConversionException;

/**
 * A Converter class which converts between an xsd date or xsd dateTime and a
 * {@link Date} instance. When parsing a string which holds no time zone,
 * UTC time zone is used. When parsing an xsd date string, time zone is ignored.
 * When creating an xml string, UTC time zone is always added.
 * Due to this approach The conversions by this class are independent from the
 * systems time zone.
 * Previously the class {@link org.apache.cxf.tools.common.DataTypeAdapter} was
 * used, but it is not independent of the system time zone (default time zone).
 * 
 * @see org.apache.cxf.tools.common.DataTypeAdapter
 * 
 * @author Hendrik Helwich
 *
 */
public class DateTypeAdapter {

	private DateTypeAdapter() {}
	
    private static final DatatypeFactory DATATYPE_FACTORY;
    private static final DateFormat DATE_FORMAT;
    //private static final DateFormat DATE_FORMAT_;
    private static final TimeZone TIME_ZONE_UTC;
    
    static {
        try {
            DATATYPE_FACTORY = DatatypeFactory.newInstance();
            TIME_ZONE_UTC = TimeZone.getTimeZone("UTC");
            DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'Z'");
           // DATE_FORMAT_ = new SimpleDateFormat("yyyy-MM-dd'Z'");
            DATE_FORMAT.setTimeZone(TIME_ZONE_UTC);
        } catch (DatatypeConfigurationException e) {
            throw new Error(e);
        }
    }

	public static Date parseDate(String s) {
		if (s == null)
			return null;
		XMLGregorianCalendar gcal = DATATYPE_FACTORY.newXMLGregorianCalendar(s);
    	gcal.setTimezone(0);
		GregorianCalendar gc = gcal.toGregorianCalendar();
		return checkDate(gc.getTime());
	}

	public static String printDate(Date dt) {
		if (dt == null)
			return null;
		checkDate(dt);
		return DATE_FORMAT.format(dt);
	}

	public static Date parseDateTime(String s) {
		if (s == null)
			return null;
        s = s.trim();
        XMLGregorianCalendar gcal =  DATATYPE_FACTORY.newXMLGregorianCalendar(s);
        if (DatatypeConstants.FIELD_UNDEFINED == gcal.getTimezone())
        	gcal.setTimezone(0);
		GregorianCalendar gc = gcal.toGregorianCalendar();
		return gc.getTime();
	}

	public static String printDateTime(Date dt) {
		if (dt == null)
			return null;
		Calendar c = Calendar.getInstance(TIME_ZONE_UTC);
		c.setTime(dt);
		return DatatypeConverter.printDateTime(c);
	}

	public static final Date checkDate(Date date) {
		if (date.getTime() % (1000L*60*60*24) != 0)
			throw new RuntimeException("time is present in date type: "+date.getTime());
		return date;
	}
}
	
