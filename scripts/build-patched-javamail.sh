#!/bin/bash

wget http://kenai.com/projects/javamail/downloads/download/javamail-1.4.2-src.zip && \
unzip javamail-1.4.2-src.zip -d javamail && \
cd javamail && \
echo SVN Username && \
read SVNUSER && \
svn export svn+ssh://$SVNUSER@scm.evolvis.org/svnroot/syncphony/trunk/kolab-ws/javamail-1.4.2/patches && \
quilt push -a && \
mvn clean install && \
cd .. && \
echo ALL OK
