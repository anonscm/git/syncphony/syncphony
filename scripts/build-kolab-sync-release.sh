#!/bin/bash

SVNURL=scm.evolvis.org/svnroot/syncphony
VERSION_WEBADMIN=1.0
FILENAME_WEBADMIN=kolab-webadmin-extension-tarent-$VERSION_WEBADMIN.tar.gz

echo -n "SVN Username        : "  && \
read -e SVNUSER && \
echo -n "Release Version     : "  && \
read -e VERSION && \
if [ -z "$VERSION" ]; then
  echo -n "Tag Version         : "
  read -e TAG_VERSION
  BUILD_VERSION=$TAG_VERSION
else
  BUILD_VERSION=$VERSION
fi && \
if [ -z "$VERSION" ]; then  # build from tag
svn export svn+ssh://$SVNUSER@$SVNURL/tags/RELEASE_${TAG_VERSION}_kolab-ws kolab-ws
svn export svn+ssh://$SVNUSER@$SVNURL/tags/RELEASE_${TAG_VERSION}_kolab-ws-connector kolab-ws-connector
else                  # build head and tag
svn co -N svn+ssh://$SVNUSER@$SVNURL/tags && \
svn co svn+ssh://$SVNUSER@$SVNURL/trunk/kolab-ws && \
svn co svn+ssh://$SVNUSER@$SVNURL/trunk/kolab-ws-connector && \
svn co svn+ssh://$SVNUSER@$SVNURL/trunk/kolab-ws-wsdl-creator && \
svn copy kolab-ws tags/RELEASE_${VERSION}_kolab-ws && \
svn copy kolab-ws-connector tags/RELEASE_${VERSION}_kolab-ws-connector && \
svn copy kolab-ws-wsdl-creator tags/RELEASE_${VERSION}_kolab-ws-wsdl-creator && \
rm -rf kolab-ws-wsdl-creator
fi && \
mkdir kolab-sync-$BUILD_VERSION && \
svn export svn+ssh://$SVNUSER@$SVNURL/trunk/kolab-webadmin-extension/build/$FILENAME_WEBADMIN kolab-sync-$BUILD_VERSION/$FILENAME_WEBADMIN && \
cp kolab-ws/jboss/kolabws-ds-mysql.xml kolab-sync-$BUILD_VERSION/kolabws-ds.xml && \
cp kolab-ws/jboss/login-config.xml kolab-sync-$BUILD_VERSION && \
cp kolab-ws/db/schema-MySQL.sql kolab-sync-$BUILD_VERSION && \
cp kolab-ws/src/main/resources/org/evolvis/bsi/kolab/kolab-ws.properties kolab-sync-$BUILD_VERSION && \
cd kolab-ws && \
mvn clean package && \
cd .. \ &&
cp kolab-ws/target/kolab-ws-$BUILD_VERSION-jar-with-dependencies-packed.jar kolab-sync-$BUILD_VERSION/kolab-ws-$BUILD_VERSION.jar && \
rm -rf kolab-ws && \
cd kolab-ws-connector && \
mvn clean package && \
cd .. \ &&
cp kolab-ws-connector/target/kolab-ws-connector-$BUILD_VERSION.s4j kolab-sync-$BUILD_VERSION && \
rm -rf kolab-ws-connector && \
if [ -n "$VERSION" ]; then
  svn commit tags/*
fi && \
rm -rf tags && \
cp $HOME/.m2/repository/javax/mail/mail/1.4.2-patched-tarent/mail-1.4.2-patched-tarent.jar kolab-sync-$BUILD_VERSION && \
wget http://mapmap.googlecode.com/files/mysql-connector-java-5.0.8-bin.jar && \
mv mysql-connector-java-5.0.8-bin.jar kolab-sync-$BUILD_VERSION && \
zip -r kolab-sync-$BUILD_VERSION.zip kolab-sync-$BUILD_VERSION && \
rm -r kolab-sync-$BUILD_VERSION && \
echo ALL OK
